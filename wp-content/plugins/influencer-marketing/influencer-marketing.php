<?php

/*
  Plugin Name: Influencer Marketing
  Plugin URL: http://SwiftMarketing.com/IMPRESS
  Description: Influencer Marketing
  Version: 2.2
  Author: Roger Vaughn, Tejas Hapani
  Author URI: https://swiftcloud.io/
  Text Domain: impress-pr
 */

// Make sure we don't expose any info if called directly
if (!function_exists('add_action')) {
    _e('Hi there!  I\'m just a plugin, not much I can do when called directly.', 'impress-pr');
    exit;
}

define('IMKT_VERSION', '2.2');
define('IMKT__MINIMUM_WP_VERSION', '4.5');
define('IMKT__PLUGIN_URL', plugin_dir_url(__FILE__));
define('IMKT__PLUGIN_DIR', plugin_dir_path(__FILE__));
define('IMKT_PLUGIN_PREFIX', 'imkt_');

register_activation_hook(__FILE__, 'imkt_install');

function imkt_install() {
    if (version_compare($GLOBALS['wp_version'], IMKT__MINIMUM_WP_VERSION, '<')) {
        add_action('admin_notices', create_function('', "echo '<div class=\"error\"><p>" . sprintf(esc_html__('Influencer Marketing %s requires WordPress %s or higher.', 'impress-pr'), IMKT_VERSION, IMKT__MINIMUM_WP_VERSION) . "</p></div>'; "));

        add_action('admin_init', 'imkt_deactivate_self');

        function imkt_deactivate_self() {
            if (isset($_GET["activate"]))
                unset($_GET["activate"]);
            deactivate_plugins(plugin_basename(__FILE__));
        }

        return;
    }
    update_option('influencer_marketing_version', IMKT_VERSION);
    imkt_preload_options();

    flush_rewrite_rules();
}

//Load admin modules
require_once('admin/influencer-marketing-admin.php');
require_once('imkt-pagetemplater.php');
require_once('public/section/imkt-preload-data.php');

/**
 *       plugin load
 * */
add_action('plugins_loaded', 'imkt_update_check_callback');
if (!function_exists('imkt_update_check_callback')) {

    function imkt_update_check_callback() {
        if (get_option("influencer_marketing_version") != IMKT_VERSION) {
            imkt_install();
        }
    }

}

add_action('upgrader_process_complete', 'imkt_update_process');
if (!function_exists('imkt_update_process')) {

    function imkt_update_process($upgrader_object, $options = '') {
        $current_plugin_path_name = plugin_basename(__FILE__);

        if (isset($options) && !empty($options) && $options['action'] == 'update' && $options['type'] == 'plugin' && $options['bulk'] == false && $options['bulk'] == false) {
            foreach ($options['packages'] as $each_plugin) {
                if ($each_plugin == $current_plugin_path_name) {
                    imkt_install();
                    imkt_initial_data();
                }
            }
        }
    }

}

/**
 *      Deactive plugin
 */
register_deactivation_hook(__FILE__, 'imkt_deactive_plugin');
if (!function_exists('imkt_deactive_plugin')) {

    function imkt_deactive_plugin() {
        flush_rewrite_rules();
    }

}

/**
 *      Uninstall plugin
 */
register_uninstall_hook(__FILE__, 'imkt_uninstall_callback');
if (!function_exists('imkt_uninstall_callback')) {

    function imkt_uninstall_callback() {
        global $wpdb;

        delete_option("influencer_marketing_version");
        delete_option("imkt_notice");

        // delete pages
        $pages = get_option('imkt_pages');
        if ($pages) {
            $pages = explode(",", $pages);
            foreach ($pages as $pid) {
                wp_delete_post($pid, true);
            }
        }
        delete_option("imkt_pages");

        /* Drop tables */
        $table_influencers = $wpdb->prefix . 'imkt_influencers';
        $wpdb->query($wpdb->prepare("DROP TABLE IF EXISTS %s", $table_influencers));

        /**
         * Delete CPT press release and terms
         */
        // Delete Taxonomy
        foreach (array('press_release_category') as $taxonomy) {
            $wpdb->delete(
                    $wpdb->term_taxonomy, array('taxonomy' => $taxonomy)
            );
        }

        // Delete CPT posts
        $wpdb->query("DELETE FROM $wpdb->posts WHERE post_type IN ('press_release')");
        $wpdb->query("DELETE meta FROM $wpdb->postmeta meta LEFT JOIN $wpdb->posts posts ON posts.ID = meta.post_id WHERE posts.ID IS NULL");

        // Deregister CPT
        if (function_exists('unregister_post_type')) {
            unregister_post_type('press_release');
        }
    }

}

/**
 *      Register Location Navigations.
 */
add_action('init', 'imkt_register_menu');
if (!function_exists('imkt_register_menu')) {

    function imkt_register_menu() {
        register_nav_menus(array(
            'menu-press-media-kit' => __('Press Media Kit', 'influencer-marketing'),
        ));
    }

}
/**
 *         Add sidebar
 */
add_action('widgets_init', 'imkt_widget_init');
if (!function_exists('imkt_widget_init')) {

    function imkt_widget_init() {
        register_sidebar(array(
            'name' => __('Press Room Sidebar', 'impress-pr'),
            'id' => 'imkt-pr-sidebar',
            'description' => __('Add widgets here to appear in press release sidebar', 'impress-pr'),
            'before_widget' => '<div class="pr-widget-border pr-m-b-15">',
            'after_widget' => '</div>',
            'before_title' => '<h3 class="pr-widget-title">',
            'after_title' => '</h3>',
        ));
    }

}
/**
 *      Enqueue scripts and styles.
 */
add_action('wp_enqueue_scripts', 'imkt_enqueue_scripts_styles');
if (!function_exists('imkt_enqueue_scripts_styles')) {

    function imkt_enqueue_scripts_styles() {
        wp_enqueue_style('imkt-custom', plugins_url('public/css/imkt-style.css', __FILE__), '', '', '');
        wp_enqueue_style('swiftcloud-tooltip', plugins_url('public/css/swiftcloud-tooltip.css', __FILE__), '', '', '');
        wp_enqueue_style('swiftcloud-fontawesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css', '', '', '');

        wp_enqueue_script('imkt-custom-script', plugins_url('public/js/imkt-custom-script.js', __FILE__), array('jquery'), '', true);
        //ajax obj
        //wp_localize_script('imkt-custom-script', 'imkt_ajax_object', array('ajax_url' => admin_url('admin-ajax.php'), 'file_path' => plugins_url('/section/swift-reviews-callback.php', __FILE__)));
    }

}

include "public/section/imkt-function.php";
include "public/section/imkt-shortcodes.php";
include "public/section/press-release-print.php";

/**
 *      RSS Feeds
 */
add_action('init', 'imkt_rss_feed_init');
if (!function_exists('imkt_rss_feed_init')) {

    function imkt_rss_feed_init() {
        add_feed('press-feed', 'imkt_rss_feed_callback');
    }

}

if (!function_exists('imkt_rss_feed_callback')) {

    function imkt_rss_feed_callback() {
        get_template_part('rss', 'press-feed');
    }

}

// Add press custom post type to feed
function pressfeed_request($qv) {
    if (isset($qv['feed']) && !isset($qv['post_type']))
        $qv['post_type'] = array('post', 'swift_reviews', 'press_release', 'event_marketing', 'swift_jobs', 'vhcard');
    return $qv;
}

add_filter('request', 'pressfeed_request');
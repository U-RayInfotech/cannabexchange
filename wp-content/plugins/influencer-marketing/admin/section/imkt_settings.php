<?php
/*
 * Settings page
 */
if (!function_exists('imkt_settings_callback')) {

    function imkt_settings_callback() {
        
        global $wpdb;
        ?>
        <div class="wrap">
            <h2>Settings</h2><hr/>
            <?php
            if (isset($_GET['update']) && !empty($_GET['update'])) {
                if ($_GET['update'] == 1) {
                    ?>
                    <div id="message" class="notice notice-success is-dismissible below-h2">
                        <p>Setting updated successfully.</p>
                    </div>
                    <?php
                } else if ($_GET['update'] == 2) {
                    ?>
                    <div id="message" class="notice notice-success is-dismissible below-h2">
                        <p>Influencers / Media contact added successfully.</p>
                    </div>
                    <?php
                } else if ($_GET['update'] == 3) {
                    ?>
                    <div id="message" class="notice notice-success is-dismissible below-h2">
                        <p>Influencers / Media contact deleted successfully.</p>
                    </div>
                    <?php
                }
            }
            ?>
            <div class="inner_content">
                <h2 class="nav-tab-wrapper" id="imkt-setting-tabs">
                    <a class="nav-tab custom-tab <?php echo (!isset($_GET['tab']) || $_GET['tab'] == "imkt-general-settings") ? 'nav-tab-active' : ''; ?>" id="imkt-general-settings-tab" href="#imkt-general-settings">General Settings</a>
                    <a class="nav-tab custom-tab <?php echo ($_GET['tab'] == "imkt-influencers-media-contacts-settings") ? 'nav-tab-active' : ''; ?>" id="imkt-influencers-media-contacts-tab" href="#imkt-influencers-media-contacts-settings">Influencers / Media Contacts</a>
                    <a class="nav-tab custom-tab <?php echo ($_GET['tab'] == "imkt-help-setup-settings") ? 'nav-tab-active' : ''; ?>" id="imkt-help-setup-tab" href="#imkt-help-setup-settings">Help & Setup</a>
                    <a class="nav-tab custom-tab <?php echo ($_GET['tab'] == "imkt-roles-permissions") ? 'nav-tab-active' : ''; ?>" id="imkt-roles-permissions-tab" href="#imkt-roles-permissions">Roles & Permissions</a>
                </h2>

                <div class="tabwrapper">
                    <div id="imkt-general-settings" class="panel <?php echo (!isset($_GET['tab']) || $_GET['tab'] == "imkt-general-settings") ? 'active' : ''; ?>">
                        <?php include 'imkt-general-settings.php'; ?>
                    </div>

                    <div id="imkt-influencers-media-contacts-settings" class="panel <?php echo ($_GET['tab'] == "imkt-influencers-media-contacts-settings") ? 'active' : ''; ?>">
                        <?php include 'imkt-influencers-media-contact-settings.php'; ?>
                    </div>

                    <div id="imkt-help-setup-settings" class="panel <?php echo ($_GET['tab'] == "imkt-help-setup-settings") ? 'active' : ''; ?>">
                        <?php include 'imkt-help-setup-settings.php'; ?>
                    </div>

                    <div id="imkt-roles-permissions" class="panel <?php echo ($_GET['tab'] == "imkt-roles-permissions") ? 'active' : ''; ?>">
                        <?php include 'imkt-role-permissions-settings.php'; ?>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }

}
?>
=== B2i Investor Tools ===
Contributors:      b2ijoe
Tags:              SEC, press, news, stock, chart, email, governance, CRM
Requires at least: 4.4
Tested up to:      4.9.6
Stable tag:        0.7.7
License:           GPLv2
License URI:       http://www.gnu.org/licenses/gpl-2.0.html

Automated Investor Relations tools: SEC Filings, Press Releases, Stock Quote / Chart, Email Updates, Financials, Institutional Holders, Insider Holdings. Products that allow you to easily and automatically keep your website data up to date.

== Description ==

= About =
SEC Website compliance and other investor relations tasks made easy, automated, and affordable. **Stop manually posting** to your website, we keep your information up-to-date automatically, securely (https), and display it in mobile friendly compatible displays.  **Keep investors and analyst informed** without them having to visit your website for updates.  Our automation tools can automatically send your investors emails with new press releases, SEC filings, calendar event reminders and end of day stock quotes.  It's that easy!


<a href="http://www.b2itech.com/about_plugins" target="_blank">Click here</a> for our live demonstration site.

* Save time by having your SEC filings and press releases automatically post to your website using our  plugins.
* Press releases can be automatically created as Wordpress posts to leverage theme's and plugin's existing styling and functionality.
* Add our Email alerts plugin to your website and give investors and analysts the ablity to opt-in/subscribe to receive email updates.
* Once you submit your press releases to major wire services and SEC filings to Edgar, our application can automatically send email notifications to subscribers.

B2i Managed VIP Services are ready to support you and can perform edits and updates to content that is not automatically updated and take more website maintenance off your plate to allow you to spend time on other key tasks. Your investor presentation, annual reports, conference calls, and any other documents and information can be posted to your website and email notifications sent to subscribers.

B2i offers additional integrated solutions, which are full featured and cloud based to handle your website, CRM, email marketing needs, conference calls, webcast and whistleblower hotline/web form solutions.

B2i (Business to Investor) Technologies, Inc. has been providing Investor Relations products and services since 2000 including these same plugins that have now been made available through Wordpress. Our product offering has continuously grown to meet industry demand with valued input from our clients. Our business philosophy is centered on providing excellent personalized customer service while offering cost effective products and services. We challenge you to find a more customizable and better priced offering.


= Getting started =
We will provide you with a Business ID and API key. Next, visit the settings/B2i options area to see available shortcodes and parameters for shortcodes.


= Plugins include =

* SEC filings
* Press releases
* Stock quote / chart / historical data
* Financials / Fundamentals
* Institutional Holders
* Insider Holdings
* Event Calendar
* Directors / Management
* Committee composition
* Charters / Documents list
* Email alert sign-up
* Contact us



Other services include: Conference call / Webcast / Whistle blower hotline. <a href="http://www.b2itech.com/Contact_Us" target="_blank">Contact us</a> for more details.

More information about each plugin and our live demonstration site: <a href="http://www.b2itech.com/About_Plugins" target="_blank">http://www.b2itech.com/About_Plugins</a>

== Frequently Asked Questions ==

= Plugin licensing =
B2i Investor Tools is a plugin based on B2i's software as a service hosted application. We offer our products and services for a monthly fee, determined by the features bundled together. Our up-front price guarantee offers the lowest cost or we will beat competing products cost by 10%.


= Trial and testing =
<a href="http://www.b2itech.com/Contact_Us" target="_blank">Contact us</a> for a temporary Business ID and Key for testing and trials.


= Plugin support =
***  B2i offers email and phone support with direct access to the developers. We encourage feedback and release feature-sets based upon input and request. ***



== Installation ==

1. Install b2i Investor Tools automatically or by uploading the entire '/b2i' directory to the '/wp-content/plugins/' directory.
2. Activate the 'b2i Investor Tools' plugin through the 'Plugins' menu in WordPress.
3. Once installed and activated, go to Settings / b2i options to register or enter your company information
5. We will then contact you and validate all information and discuss your project and pricing.
Pricing is very straight-forward, but we do offer a-la-carte or build your bundle and want to make sure all of your needs are met.


== Changelog ==

= 0.7.7 =
* Add Date format parameter for Press Release plugin: df="1" Currently accepts 1,2,3
 df="1" May 3, 2018
 df="2" 05/03/2018
 df="3" 03/05/2018

= 0.7.6 =
* Fix for chart custom period selected on mobile devices

= 0.7.5 =
* added parameter for calendar to control year as parameter
* added mode m="0" for calendar to return all events (past and future)


= 0.7.4 =
* added parameter for chart to control colors with parameters
 c   - HTML color for line and fill: default 666666 (no # needed)
 cc  - HTML color for chart cursor: default ff0000 (no # needed)
 lc  - HTML color for top label color: default 666666 (no # needed)<br>
 lc2 - HTML color for other label color: default 666666 (no # needed)


= 0.7.3 =
* made https default on all plugins
* added parameter for chart to make default display period: p="" - acceptable values: 10d, 1m, 3m, 6m, 1y, max


= 0.7.2 =
* Press Releases Update: Restrict headline character count displayed.
* Email Alert Update: Multi-lingual handling.
* Plugin Update: versioning on JavaScript reference to prevent cache when updating plugin.


= 0.7.1 =
* Feature Update: B2i now automatically posts press releases into your WordPress post system. This allows you to leverage themes that format your post and use other plugins that work with your WordPress post.

= 0.7.0 =
* Chart updates, Press release expand funtionality

= 0.6.9 =
* Add historical quote search

= 0.6.8 =
* New Stock plugin with multiple format display options
* Event Calendar update to retrieve Past events

= 0.6.7 =
* Quarterly Financials: Cash Flow, Income Statement, Balance Sheet

= 0.6.6 =
* Event Calendar, Institutional Holders, Insider Holdings

= 0.6.5 =
* Parameters for Document lists

= 0.6.4 =
* Added Event Calendar

= 0.6.3 =
* Fix for Detailed Quote parameters being passed with default values

= 0.6.2 =
* Added Directors Committee composition chart

= 0.6.1 =
* Added custom control over SEC div to load data in

= 0.6.0 =
* Added paging feature for Documents list/Showcase

= 0.5.7 =
* Added search feature for Press releases and drop down/select option for tags

= 0.5.6 =
* Added ability for Press releases to use a link instead of floating div

= 0.5.5 =
* Added Interactive Chart and shortcode

= 0.5.4 =
* updated readme file for clarity on setup process
* Added new parameters for controlling https for all plugins, parameters for controlling content height / width.

= 0.5.3 =
* <span style="color:red">Initial public release</span>

<div class="wrap">
	<br><hr><br>
    <p><h3>Trials and Testing </h3></p>
	<p>Please <a href="http://www.b2itech.com/Contact_Us" target="_blank">contact us</a> for a temporary Business ID and Key for testing and trials.</p>
    <br><hr><br>
    <p><h3>Shortcodes</h3></p>
    <p>Examples: (only functional once we activate your data or you request a trial Business ID and key)</p>
    <p><code>[b2i_sec]</code></p>
    <p><code>[b2i_press_releases group="65" c="5"]</code></p>
    <p><code>[b2i_stock s="your symbol"]</code></p>
    <p><code>[b2i_chart s="your symbol"]</code></p>
    <table class="b2i-instructions widefat fixed">
        <thead>
            <tr>
                <th width="200">B2i Plugin</th>
                <th width="300">Shortcode</th>
                <th>Attributes</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>SEC Filings</td>
                <td>[b2i_sec]</td>
                <td>
					<strong>sdiv</strong> - Div to write and load content, default is sdiv="SecDiv" if using multiple instances on same page, must use unique sdiv for each<br>
                    <strong>y</strong> - Year, ex. y=2017 to show only filings for this year<br>
                    <strong>t</strong> - Type, ex. t=10k to show only 10k filings<br>
                    <strong>o</strong> - Owner: 'default' = all filings, '1' = owner only filings, '2' = exclude owner filings<br>
                    <strong>c</strong> - Count of filings per page, default 20<br>
                    <strong>n</strong> - Set to n=0 to hide page naviation<br>
                    <strong>sf</strong> - Set to sf=0 to hide search/filter form<br>
                    <strong>sh</strong> - Set to sh=0 to hide header row with column titles<br>
                    <strong>lo</strong> - Layout: default 1 for table, 2 for Div output<br>
		            <strong>isw</strong> - Content div story width - reduced<br>
		            <strong>ish</strong> - Content div story height - reduced<br>
		            <strong>ismh</strong> - Content div story max height expanded - in pixels - ex. ismh="700"<br>
		            <strong>ismw</strong> - Content div story max width expanded - in pixels - ex. ismw="900"<br>
		            <strong>ilo</strong> - Content div left offset<br>
		            <strong>ito</strong> - Content div top offset<br><br>
                </td>
            </tr>
            <tr>
                <td>Press Releases <br /><br />Now press releases can also be automatically posted to your WordPress site as a WordPress post with minimal setup.</td>
                <td>[b2i_press_releases group="ID"]</td>
                <td>
                    <strong>group</strong> - Library ID - we will provide - multiple list are possible<br>
                    <strong>n</strong> - Set to 0 to hide the navigation<br>
                    <strong>c</strong> - Count of items to show, default is 10<br>
                    <strong>df</strong> - Date Format: 1-3 default is df="1" 1=[mmm dd, yyyy] 2=[mm/dd/yyyy] 3=[dd/mm/yyyy]<br>
                    <strong>se</strong> - Show expand set to "1"- allows story to display below headline<br>
                    <strong>o</strong> - Output, default 0 for table, 1 for unordered list<br>
		            <strong>ilo</strong> - Set number of pixels for content div left offset<br>
		            <strong>ito</strong> - Set number of pixels for content div top offset<br>
		            <strong>a</strong> - Set to "1" to use A link to new window instead of floating DIV<br>
		            <strong>tl</strong> - Set to "1" to show tools - Year select and search box<br>
                    <strong>css</strong> - Use css="0" to turn off default css<br>
                    <strong>ln</strong> - Set a limited amount of characters to return for the headline
					<strong>ismh</strong> - Content div story max height expanded - in pixels - ex. ismh="700"<br>
		            <strong>ismw</strong> - Content div story max width expanded - in pixels - ex. ismw="900"<br>
		            <strong>isw</strong> - Set number of pixels for content div story width when reduced<br>
		            <strong>ish</strong> - Set number of pixels for content div story height when reduced<br>
					<!-- 
                    <strong>h</strong> - Default is "https"<br>
					<strong>t</strong> - Tag ID<br>
                    <strong>f</strong> - Filter ID<br>
					-->
					<br>
					<br>
                </td>
            </tr>
			
			
            <tr>
                <td>Stock Detail Quote</td>
                <td>[b2i_stock s="symbol"]<br><a href="http://www.b2itech.com/QuoteApi" target="_blank">View formats</a></td>
                <td>
                    <strong>sdiv</strong> - When using quote multiple times on same page, set unique div names.<br>
                    <strong>s</strong> - Stock symbol to look up<br>
                    <strong>f</strong> - Format for stock data output. Current usage is 1-5<br>
                    <strong>dl</strong> - Decimal places, Default is 2, dl="4" would show 4 places right of decimal<br>
                    <strong>d</strong> - Show dollar sign, set to d="1", Default is 0 / off<br>
                    <strong>css</strong> - Use css="0" to turn off default css<br>
					<br>
                </td>
            </tr>
			
			
            <tr>
                <td>Stock Historical Quote</td>
                <td>[b2i_HistoricalQuote s="symbol"] </td>
                <td>
                    <strong>s</strong> - Stock symbol to look up<br>
                    <strong>dl</strong> - Decimal places, Default is 2, dl="4" would show 4 places right of decimal<br>
                    <strong>d</strong> - Show dollar sign, set to d="1", Default is 0 / off<br>
                    <strong>css</strong> - Use css="0" to turn off default css<br>
					<br>
                </td>
            </tr>
			
            <tr>
                <td>Stock Chart</td>
                <td>[b2i_chart s="symbol"]</td>
                <td>
                    <strong>s</strong> - Stock symbol to look up<br>
					<strong>p</strong> - Selected time period - values are 10d, 1m, 3m, 6m, 1y, max<br>
                    <strong>e</strong> - Stock exchange to display on chart<br>
                    <strong>c</strong> - HTML color for line and fill: default 666666 (no # needed)<br>
                    <strong>cc</strong> - HTML color for chart cursor: default ff0000 (no # needed)<br>
                    <strong>lc</strong> - HTML color for top label color: default 666666 (no # needed)<br>
                    <strong>lc2</strong> - HTML color for other label color: default 666666 (no # needed)<br>
                    <strong>bgc</strong> - HTML color for chart background: default f1f1f1 (no # needed)<br>
                    <strong>height</strong> - Default is 600px<br>
                    <strong>width</strong> - Default is 96%<br><br>
                </td>
            </tr>
			
			
            <tr>
                <td>Quote Detail</td>
                <td>[b2i_quote s="symbol"]<br>Becoming obsolete except for dataonly usage</td>
                <td>
                    <strong>dataonly</strong> - set to "true" to return data only JSON object<br>
                    <strong>o</strong> - data output type, o="1" for JSON object, o="0" for javascript variables<br>
                    <strong>s</strong> - Stock symbol to look up<br>
                    <strong>dl</strong> - Decimal places, Default is 2, dl="4" would show 4 places right of decimal<br>
                    <strong>d</strong> - Show dollar sign, set to d="1", Default is 0 / off<br><br>
                </td>
            </tr>
			
            <tr>
                <td>Committee Composition / Charters<br><br><br></td>
                <td>[b2i_committee]</td>
                <td>
                    <!--  <strong>h</strong> - Default is "https"<br>  -->
                </td>
            </tr>
			
            <tr>
                <td>Directors<br><br><br></td>
                <td>[b2i_directors]</td>
                <td>
                    <!-- <strong>h</strong> - Default is "https"<br> -->
                </td>
            </tr>
			
            <tr>
                <td>Management<br><br><br></td>
                <td>[b2i_management]</td>
                <td>
                    <!-- <strong>h</strong> - Default is "https"<br> -->
                </td>
            </tr>
			
            <tr>
                <td>Financials / Fundamentals<br><br><br></td>
                <td>[b2i_financials s="symbol"]</td>
                <td>
                    <strong>s</strong> - Stock symbol for Financials<br>
                    <strong>m</strong> - Mode for financials type, default is m="1" for Cash Flow<br>
					1=Cash Flow, 2=Income Statement, 3=Balance Sheet.<br>
					
                    <!-- <strong>c</strong> - Count of Quarters to show, default is 5<br> -->
                </td>
            </tr>
			
            <tr>
                <td>Events Calendar<br><br><br></td>
                <td>[b2i_calendar]<br>Multiple can be used for segregating data by past/future or year</td>
                <td>
                    <strong>c</strong> - Count of items to show, default is 5<br>
                    <strong>m</strong> - Default m="", Future events, Mode set m="1" Past events, m="0" All events<br>
                    <strong>y</strong> - Year to filter events for Past events ex. y="2017"<br>
					<!-- <strong>se</strong> - Show expand - set to "1" - creates a div below each item - for new downloads coming -->
					<br>
                </td>
            </tr>
			
            <tr>
                <td>Institutional Holders<br><br><br></td>
                <td>[b2i_institutional s="symbol"]</td>
                <td>
                    <strong>s</strong> - Stock symbol for inside holdings<br>
                    <strong>c</strong> - Count of items to show, default is 5<br>
                </td>
            </tr>
			
            <tr>
                <td>Insider Holders<br><br><br></td>
                <td>[b2i_insiders s="symbol"]</td>
                <td>
                    <strong>s</strong> - Stock symbol for inside holdings<br>
                    <strong>c</strong> - Count of items to show, default is 5<br>
                </td>
            </tr>
			
            <tr>
                <td>Documents</td>
                <td>[b2i_showcase id="ID"]</td>
                <td>
                    <strong>id</strong> - Document list id - we will provide - multiple list are possible<br>
                    <strong>c</strong> - Count of items to show, default is 5<br>
                    <strong>n</strong> - Navigation for paging, default is 0 (Off), set to 1 for On<br>
                    <strong>lo</strong> - Layout: default 0 for table, 1 for unordered list, 2 for Div output<br>
					<strong>si</strong> - Show Image: 0:no, 1:left, 2:right<br>
					<strong>sd</strong> - Show Date: 0:no 1:yes, 2:Long format<br>
		            <strong>ismh</strong> - Content div story max height expanded - in pixels - ex. ismh="700"<br>
		            <strong>ismw</strong> - Content div story max width expanded - in pixels - ex. ismw="900"<br>
		            <strong>isw</strong> - Content div story width - reduced<br>
		            <strong>ish</strong> - Content div story height - reduced<br>
		            <strong>ilo</strong> - Content div left offset<br>
		            <strong>ito</strong> - Content div top offset<br><br>
                </td>
            </tr>
			
            <tr>
                <td>Contact-us Form</td>
                <td>[b2i_contactus]</td>
                <td>
		            <strong>ish</strong> - Content area height - in pixels - ex. ish="900"<br>
		            <strong>isw</strong> - Content area width - in pixels - ex. isw="900"<br>
				</td>
            </tr>
			
            <tr>
                <td>Email Opt-In</td>
                <td>[b2i_email_optin]</td>
                <td>
		            <strong>ish</strong> - Content area height - in pixels - ex. ish="900"<br>
		            <strong>isw</strong> - Content area width - in pixels - ex. isw="900"<br>
		            <strong>lang</strong> - Language ID - Default 1 for English. Requires additional languages to be added to B2i account.<br>
					<br>
				</td>
            </tr>
			
            <tr>
                <td>Request Information</td>
                <td>[b2i_request_optin]</td>
                <td>
		            <strong>ish</strong> - Content area height - in pixels - ex. ish="900"<br>
		            <strong>isw</strong> - Content area width - in pixels - ex. isw="900"<br>
		            <strong>lang</strong> - Language ID - Default 1 for English. Requires additional languages to be added to B2i account.<br>
					<br>
				</td>
            </tr>
			
            <tr>
                <td>Investor Profile</td>
                <td>[b2i_myprofile]</td>
                <td>
		            <strong>ish</strong> - Content area height - in pixels - ex. ish="900"<br>
		            <strong>isw</strong> - Content area width - in pixels - ex. isw="900"<br>
		            <strong>lang</strong> - Language ID - Default 1 for English. Requires additional languages to be added to B2i account.<br>
					<br>
				</td>
            </tr>
			
        </tbody>
    </table>
</div>
<?php
/**
 * B2i Shortcode
 * @since 0.1.0
 * @package b2i
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * B2i Shortcode.
 * @since 0.1.0
 */
class B2i_Shortcode {
	
	protected $version = '0.7.7';
	
	/**
	 * Parent plugin class
	 * @var   class
	 * @since 0.1.0
	 */
	protected $plugin = null;

	/**
	 * Business ID option
	 * @var   string
	 * @since 0.1.0
	 */
	protected $business_id = '';

	/**
	 * Key option
	 * @var   string
	 * @since 0.1.0
	 */
	protected $key = '';
	protected $postkey = '';

	/**
	 ***************************************************************************************************************
	 * Base URL
	 * @var   string
	 * @since 0.1.0
	 ***************************************************************************************************************
	 */
	protected $base_http = 'https';
	protected $base_url = 'https://www.b2i.us/b2i/';
	protected $base_url2 = '://www.b2i.us/b2i/';
	protected $base_url3 = '://www.b2i.us/';
	protected $base_libdivname = 'LibDiv';
	protected $base_showdivname = 'ShowDiv';
	protected $base_findivname = 'LibDiv';
	protected $base_stockdivname = 'QuoteDiv';
		
	/**
	*********************************************************************************
	 * Constructor
	 *
	 * @since  0.1.0
	 * @param  object $plugin Main plugin object.
	 * @return void
	*********************************************************************************
	 */
	public function __construct( $plugin ) {
		$this->plugin = $plugin;

		add_action( 'cmb2_init', array( $this, 'setup_vars' ) );
		add_action( 'cmb2_init', array( $this, 'shortcodes' ) );
		
		wp_enqueue_script( 'whistleblower', plugins_url( 'b2i-investor-tools/js/wb_script.js'), array( 'jquery'), $this->version, false );
		
		wp_enqueue_script( 'amcharts', plugins_url( 'b2i-investor-tools/js/amcharts.js'), array( 'jquery'), $this->version, false );
		wp_enqueue_script( 'serial', plugins_url( 'b2i-investor-tools/js/serial.js'), array( 'jquery'), $this->version, false );
		wp_enqueue_script( 'amstock', plugins_url( 'b2i-investor-tools/js/amstock.js'), array( 'jquery'), $this->version, false );
		wp_enqueue_script( 'export', plugins_url( 'b2i-investor-tools/js/export.js'), array( 'jquery'), $this->version, false );
		
		wp_enqueue_style('chartstyle', plugins_url('b2i-investor-tools/css/style.css'), array(), $this->version, false);
		wp_enqueue_style('export', plugins_url('b2i-investor-tools/css/export.css'), array(), $this->version, false);
	}

	
	/**
	*********************************************************************************
	 * Setup business id and api key variables
	 * @since  0.1.0
	 * @return void
	*********************************************************************************
	*/
	public function setup_vars() {
		$this->business_id = cmb2_get_option( 'b2i_options', 'business_id' );
		$this->key = cmb2_get_option( 'b2i_options', 'key' );
		$this->postkey = cmb2_get_option( 'b2i_options', 'postkey' );
	}

	
	/**
	 *******************************************************************************
	 * Initiate shortcodes
	 * @since  0.1.0
	 * @return void
	 ********************************************************************************
	 */
	public function shortcodes() {
		add_shortcode( 'b2i_library_latest_item', array( $this, 'library_latest_item_shortcode' ) );
		add_shortcode( 'b2i_library_headline', array( $this, 'library_headline_shortcode' ) );
		add_shortcode( 'b2i_press_releases', array( $this, 'press_releases_shortcode' ) );
		add_shortcode( 'b2i_sec', array( $this, 'sec_shortcode' ) );
		add_shortcode( 'b2i_quote', array( $this, 'quote_shortcode' ) );
		add_shortcode( 'b2i_stock', array( $this, 'stock_shortcode' ) );
		add_shortcode( 'b2i_showcase', array( $this, 'showcase_shortcode' ) );
		add_shortcode( 'b2i_email_optin', array( $this, 'email_optin_shortcode' ) );
		add_shortcode( 'b2i_request_optin', array( $this, 'request_optin_shortcode' ) );
		add_shortcode( 'b2i_myprofile', array( $this, 'myprofile_shortcode' ) );
		add_shortcode( 'b2i_contactus', array( $this, 'email_contactus_shortcode' ) );
		add_shortcode( 'b2i_chart', array( $this, 'chart_shortcode' ) );
		add_shortcode( 'b2i_committee', array( $this, 'comm_shortcode' ) );
		add_shortcode( 'b2i_directors', array( $this, 'directors_shortcode' ) );
		add_shortcode( 'b2i_management', array( $this, 'management_shortcode' ) );
		add_shortcode( 'b2i_calendar', array( $this, 'cal_shortcode' ) );
		add_shortcode( 'b2i_institutional', array( $this, 'institutional_shortcode' ) );
		add_shortcode( 'b2i_insiders', array( $this, 'insiders_shortcode' ) );
		add_shortcode( 'b2i_financials', array( $this, 'financials_shortcode' ) );
		add_shortcode( 'b2i_HistoricalQuote', array( $this, 'HistoricalQuote_shortcode' ) );
	}


	/**
	 *********************************************************************************
	 * Define the Contact us plugin shortcode
	 * @since  0.1.0
	 * @return string
	 *********************************************************************************
	 */
	public function email_contactus_shortcode( $atts ) {
		$html = '';
		$atts = shortcode_atts(
			array(
				'h' => 'https',
				'n' => '',
				'isw' => '',
				'ish' => ''
			),
			$atts
		);

		$html .= '<div id="ContactDiv"></div>' . "\n";
		$html .= '<script language="JavaScript" src="' . esc_url(esc_attr( $atts['h'] ) . $this->base_url2 . 'ContactUsApi.js?v=' . $this->version ) . '" type="text/javascript"></script>' . "\n";
		$html .= '<script language="JavaScript" type="text/javascript">' . "\n";
		$html .= 'oContact.BizID="' . $this->business_id . '";' . "\n";
		$html .= ( $atts['h'] !== '' ) ? 'oContact.sHttp="' . esc_attr( $atts['h'] ) . '";' . "\n" : '';
		$html .= ( $atts['n'] !== '' ) ? 'oContact.sNav="' . esc_attr( $atts['n'] ) . '";' . "\n" : '';
		$html .= ( $atts['isw'] !== '' ) ? 'oContact.iStoryWidth="' . esc_attr( $atts['isw'] ) . '";' . "\n" : '';
		$html .= ( $atts['ish'] !== '' ) ? 'oContact.iStoryHeight="' . esc_attr( $atts['ish'] ) . '";' . "\n" : '';
		$html .= 'getContactData();' . "\n";
		$html .= '</script>' . "\n";
		return $html;
	}


	/**
	 **********************************************************************************
	 * Define the Email Opt-in plugin shortcode
	 * @since  0.1.0
	 * @return string
	 **********************************************************************************
	 */
	public function email_optin_shortcode( $atts ) {
		$html = '';
		$atts = shortcode_atts(
			array(
				'h' => 'https',
				'g' => '',
				'lang' => '',
				'isw' => '',
				'ish' => ''
			),
			$atts
		);

		$html .= '<div id="EmailDiv"></div>' . "\n";
		$html .= '<script language="JavaScript" src="' . esc_url(esc_attr( $atts['h'] ) . $this->base_url2 . 'EmailApi.js?v=' . $this->version ) . '" type="text/javascript"></script>' . "\n";
		$html .= '<script language="JavaScript" type="text/javascript">' . "\n";
		$html .= 'oEmail.BizID="' . $this->business_id . '";' . "\n";
		$html .= ( $atts['h'] !== '' ) ? 'oEmail.sHttp="' . esc_attr( $atts['h'] ) . '";' . "\n" : '';
		$html .= ( $atts['g'] !== '' ) ? 'oEmail.Group="' . esc_attr( $atts['g'] ) . '";' . "\n" : '';
		$html .= ( $atts['lang'] !== '' ) ? 'oEmail.LangID="' . esc_attr( $atts['lang'] ) . '";' . "\n" : '';
		$html .= ( $atts['isw'] !== '' ) ? 'oEmail.iStoryWidth="' . esc_attr( $atts['isw'] ) . '";' . "\n" : '';
		$html .= ( $atts['ish'] !== '' ) ? 'oEmail.iStoryHeight="' . esc_attr( $atts['ish'] ) . '";' . "\n" : '';
		$html .= 'getEmailData();' . "\n";
		$html .= '</script>' . "\n";
		return $html;
	}


	/**
	 **********************************************************************************
	 * Define the Request info Opt-in plugin shortcode
	 * @since  0.7.2
	 * @return string
	 **********************************************************************************
	 */
	public function request_optin_shortcode( $atts ) {
		$html = '';
		$atts = shortcode_atts(
			array(
				'h' => 'https',
				'g' => '',
				'lang' => '',
				'isw' => '',
				'ish' => ''
			),
			$atts
		);

		$html .= '<div id="RequestDiv"></div>' . "\n";
		$html .= '<script language="JavaScript" src="' . esc_url(esc_attr( $atts['h'] ) . $this->base_url2 . 'RequestApi.js?v=' . $this->version ) . '" type="text/javascript"></script>' . "\n";
		$html .= '<script language="JavaScript" type="text/javascript">' . "\n";
		$html .= 'oRequest.BizID="' . $this->business_id . '";' . "\n";
		$html .= ( $atts['h'] !== '' ) ? 'oRequest.sHttp="' . esc_attr( $atts['h'] ) . '";' . "\n" : '';
		$html .= ( $atts['g'] !== '' ) ? 'oRequest.Group="' . esc_attr( $atts['g'] ) . '";' . "\n" : '';
		$html .= ( $atts['lang'] !== '' ) ? 'oRequest.LangID="' . esc_attr( $atts['lang'] ) . '";' . "\n" : '';
		$html .= ( $atts['isw'] !== '' ) ? 'oRequest.iStoryWidth="' . esc_attr( $atts['isw'] ) . '";' . "\n" : '';
		$html .= ( $atts['ish'] !== '' ) ? 'oRequest.iStoryHeight="' . esc_attr( $atts['ish'] ) . '";' . "\n" : '';
		$html .= 'getRequestData();' . "\n";
		$html .= '</script>' . "\n";
		return $html;
	}


	/**
	 **********************************************************************************
	 * Define the Request info Opt-in plugin shortcode
	 * @since  0.7.2
	 * @return string
	 **********************************************************************************
	 */
	public function myprofile_shortcode( $atts ) {
		$html = '';
		$atts = shortcode_atts(
			array(
				'h' => 'https',
				'lang' => '',
				'isw' => '',
				'ish' => ''
			),
			$atts
		);

		$html .= '<div id="ProfileDiv"></div>' . "\n";
		$html .= '<script language="JavaScript" src="' . esc_url(esc_attr( $atts['h'] ) . $this->base_url2 . 'ProfileApi.js?v=' . $this->version ) . '" type="text/javascript"></script>' . "\n";
		$html .= '<script language="JavaScript" type="text/javascript">' . "\n";
		$html .= 'oProfile.BizID="' . $this->business_id . '";' . "\n";
		$html .= ( $atts['h'] !== '' ) ? 'oProfile.sHttp="' . esc_attr( $atts['h'] ) . '";' . "\n" : '';
		$html .= ( $atts['lang'] !== '' ) ? 'oProfile.LangID="' . esc_attr( $atts['lang'] ) . '";' . "\n" : '';
		$html .= ( $atts['isw'] !== '' ) ? 'oProfile.iStoryWidth="' . esc_attr( $atts['isw'] ) . '";' . "\n" : '';
		$html .= ( $atts['ish'] !== '' ) ? 'oProfile.iStoryHeight="' . esc_attr( $atts['ish'] ) . '";' . "\n" : '';
		$html .= 'getProfileData();' . "\n";
		$html .= '</script>' . "\n";
		return $html;
	}
	
	
	
	
	
	/**
	 **********************************************************************************
	 * Define the Library Standalone plugin shortcode
	 * @since  0.1.0
	 * @return string
	 **********************************************************************************
	 */
	public function press_releases_shortcode( $atts ) {
		$html = '';
		$libdivname = $this->base_libdivname;
		$atts = shortcode_atts(
			array(
				'group' => '',
				'h' => 'https',
				'dn' => '',    // list div name, set when using multiple on same page
				'sdn' => '',   // story div name, set to the list div name
				'n' => '',     // show navigation/paging
				'c' => '',     // count
				'df' => '',    // dateformat
				'se' => '',    // Show expand set to 1, allows story to show in div under the headline
				'o' => '',     // output type table div
				't' => '',     // tag id
				'f' => '',     // filter id
				'isw' => '',   // floating div story width
				'ish' => '',   // floating div story height
				'ismw' => '',  // floating div story MAX width
				'ismh' => '',  // floating div story MAX height
				'ilo' => '',   // floating div story left offset
				'ito' => '',   // floating div story top offset
				'a' => '', 	   // set to 1 to use a link instead of target div
				'tl' => '',    // show tools - tags ddl and search box
				'pd' => '1',   // use popout div
				'ucn' => '',   // use custom div name
				'css' => '',    // set to 0 to turn off default css
				'ln' => '' 		// set Headline length
			),
			$atts
		);
		// $base_libdivname
		$libdivname .= esc_attr( $atts['group'] ) ;
		if ( $atts['dn']!='' ) $libdivname = esc_attr( $atts['dn'] );
		if ( $atts['pd']=='0' ) $html .= '<a name="Lib' . esc_attr( $atts['group'] ) . '"></a><span id="DisplayListDiv1" style="display:none;"><a href="Javascript:document.location=\'#Lib65\'; getData();">Display list</a></span>' . "\n";
		$html .= '<div id="' . $libdivname . '"></div>' . "\n";
		$html .= '<script language="JavaScript" src="' . esc_url(esc_attr( $atts['h'] ) . $this->base_url2 . 'LibraryApi.js?v=' . $this->version ) . '" type="text/javascript"></script>' . "\n";
		$html .= '<script language="JavaScript" type="text/javascript">' . "\n";
		$html .= 'oLib.BizID="' . $this->business_id . '";' . "\n";
		$html .= 'oLib.Group="' . esc_attr( $atts['group'] ) . '";' . "\n";
		$html .= 'oLib.sDiv="' . $libdivname . '";' . "\n";
		$html .= ( $atts['sdn'] !== '' ) ? 'oLib.sStoryDiv="' . esc_attr( $atts['sdn'] ) . '";' . "\n" : '';
		$html .= ( $atts['h'] !== '' ) ? 'oLib.sHttp="' . esc_attr( $atts['h'] ) . '";' . "\n" : '';
		$html .= ( $atts['c'] !== '' ) ? 'oLib.Count="' . esc_attr( $atts['c'] ) . '";' . "\n" : '';
		$html .= ( $atts['df'] !== '' ) ? 'oLib.iDateFormat="' . esc_attr( $atts['df'] ) . '";' . "\n" : '';
		$html .= ( $atts['se'] !== '' ) ? 'oLib.ShowExpand="' . esc_attr( $atts['se'] ) . '";' . "\n" : '';
		$html .= ( $atts['n'] !== '' ) ? 'oLib.sNav="' . esc_attr( $atts['n'] ) . '";' . "\n" : '';
		$html .= ( $atts['o'] !== '' ) ? 'oLib.Output="' . esc_attr( $atts['o'] ) . '";' . "\n" : '';
		$html .= ( $atts['t'] !== '' ) ? 'oLib.Tag="' . esc_attr( $atts['t'] ) . '";' . "\n" : '';
		$html .= ( $atts['f'] !== '' ) ? 'oLib.Filter="' . esc_attr( $atts['f'] ) . '";' . "\n" : '';
		$html .= ( $atts['isw'] !== '' ) ? 'oLib.iStoryWidth="' . esc_attr( $atts['isw'] ) . '";' . "\n" : '';
		$html .= ( $atts['ish'] !== '' ) ? 'oLib.iStoryHeight="' . esc_attr( $atts['ish'] ) . '";' . "\n" : '';
		$html .= ( $atts['ismw'] !== '' ) ? 'oLib.iStoryMaxWidth="' . esc_attr( $atts['ismw'] ) . '";' . "\n" : '';
		$html .= ( $atts['ismh'] !== '' ) ? 'oLib.iStoryMaxHeight="' . esc_attr( $atts['ismh'] ) . '";' . "\n" : '';
		$html .= ( $atts['ilo'] !== '' ) ? 'oLib.iLeftOffset="' . esc_attr( $atts['ilo'] ) . '";' . "\n" : '';
		$html .= ( $atts['ito'] !== '' ) ? 'oLib.iTopOffset="' . esc_attr( $atts['ito'] ) . '";' . "\n" : '';
		$html .= ( $atts['a'] !== '' ) ? 'oLib.Alink="' . esc_attr( $atts['a'] ) . '";' . "\n" : '';
		$html .= ( $atts['tl'] !== '' ) ? 'oLib.sTools="' . esc_attr( $atts['tl'] ) . '";' . "\n" : '';
		$html .= ( $atts['css'] !== '' ) ? 'oLib.CSS="' . esc_attr( $atts['css'] ) . '";' . "\n" : '';
		$html .= ( $atts['ln'] !== '' ) ? 'oLib.HeadlineLen="' . esc_attr( $atts['ln'] ) . '";' . "\n" : '';
		/* $html .= ( $atts['pd'] !== '' ) ? 'oLib.iPopoutDiv="' . esc_attr( $atts['pd'] ) . '";' . "\n" : ''; */
		$html .= 'getData();' . "\n";
		$html .= '</script>' . "\n";
		/*
		if ( $atts['pd']==0 ) $html .= '<span id="DisplayListDiv2" style="display:none;"><a href="Javascript:document.location=\'#Lib65\'; getData();">Display list</a></span>' . "\n"; 
		*/
		return $html;
	}

	
	/**
	*********************************************************************************
	 * Define the SEC Standalone plugin shortcode
	 * @since  0.1.0
	 * @return string
	*********************************************************************************
	 */
	public function sec_shortcode( $atts ) {
		$html = '';
		$atts = shortcode_atts(
			array(
				'y' => '',
				't' => '',
				'sdiv' => 'SECdiv',
				'o' => 'default',
				'c' => '',
				'n' => '',
				'h' => 'https',
				'sf' => '',
				'sh' => '',
				'lo' => '1',
				'isw' => '',
				'ish' => '',
				'ismw' => '',
				'ismh' => '',
				'ilo' => '',
				'ito' => '',
				'v' => ''
			),
			$atts
		);
		
		$html .= '<div id="' . esc_attr( $atts['sdiv'] ) . '"></div>' . "\n";
		$html .= '<script language="JavaScript" src="' . esc_url( esc_attr( $atts['h'] ) . $this->base_url2 . 'SECapi.js?v=' . $this->version ) . '" type="text/javascript"></script>' . "\n";
		$html .= '<script language="JavaScript" type="text/javascript">' . "\n";
		$html .= 'oSEC.BizID="' . $this->business_id . '";' . "\n";
		$html .= 'oSEC.sKey="' . $this->key . '";' . "\n";
		
		$html .= ( $atts['sdiv'] !== '' ) ? 'oSEC.sDiv="' . esc_attr( $atts['sdiv'] ) . '";' . "\n" : '';
		$html .= ( $atts['y'] !== '' ) ? 'oSEC.y="' . esc_attr( $atts['y'] ) . '";' . "\n" : '';
		$html .= ( $atts['t'] !== '' ) ? 'oSEC.t="' . esc_attr( $atts['t'] ) . '";' . "\n" : '';
		$html .= ( $atts['o'] !== '' ) ? 'oSEC.o="' . esc_attr( $atts['o'] ) . '";' . "\n" : '';
		$html .= ( $atts['c'] !== '' ) ? 'oSEC.c="' . esc_attr( $atts['c'] ) . '";' . "\n" : '';
		$html .= ( $atts['n'] !== '' ) ? 'oSEC.n="' . esc_attr( $atts['n'] ) . '";' . "\n" : '';
		$html .= ( $atts['h'] !== '' ) ? 'oSEC.sHttp="' . esc_attr( $atts['h'] ) . '";' . "\n" : '';
		$html .= ( $atts['sf'] !== '' ) ? 'oSEC.sf="' . esc_attr( $atts['sf'] ) . '";' . "\n" : '';
		$html .= ( $atts['sh'] !== '' ) ? 'oSEC.sh="' . esc_attr( $atts['sh'] ) . '";' . "\n" : '';
		$html .= ( $atts['lo'] !== '' ) ? 'oSEC.lo="' . esc_attr( $atts['lo'] ) . '";' . "\n" : '';
		$html .= ( $atts['isw'] !== '' ) ? 'oSEC.iStoryWidth="' . esc_attr( $atts['isw'] ) . '";' . "\n" : '';
		$html .= ( $atts['ish'] !== '' ) ? 'oSEC.iStoryHeight="' . esc_attr( $atts['ish'] ) . '";' . "\n" : '';
		$html .= ( $atts['ismw'] !== '' ) ? 'oSEC.iStoryMaxWidth="' . esc_attr( $atts['ismw'] ) . '";' . "\n" : '';
		$html .= ( $atts['ismh'] !== '' ) ? 'oSEC.iStoryMaxHeight="' . esc_attr( $atts['ismh'] ) . '";' . "\n" : '';
		$html .= ( $atts['ilo'] !== '' ) ? 'oSEC.iLeftOffset="' . esc_attr( $atts['ilo'] ) . '";' . "\n" : '';
		$html .= ( $atts['ito'] !== '' ) ? 'oSEC.iTopOffset="' . esc_attr( $atts['ito'] ) . '";' . "\n" : '';
		$html .= ( $atts['v'] !== '' ) ? 'oSEC.SecVer="' . esc_attr( $atts['v'] ) . '";' . "\n" : '';
		$html .= 'getSecData();' . "\n";
		$html .= '</script>' . "\n";
		return $html;
	}

	
	/**
	*********************************************************************************
	 * Define the Custom Showcase plugin shortcode
	 * @since  0.1.0
	 * @return string
	*********************************************************************************
	 */
	public function showcase_shortcode( $atts ) {
		$html = '';
		$showdivname = $this->base_showdivname;
		$atts = shortcode_atts(
			array(
				'id' => '',
				'h' => 'https',
				'c' => '5',
				'n' => '0',
				'si' => '1',
				'sd' => '1',
				'lo' => '',
				'isw' => '',
				'ish' => '',
				'ismw' => '',
				'ismh' => '',
				'ilo' => '',
				'ito' => ''
			),
			$atts
		);
		$showdivname .= esc_attr( $atts['id'] );
		$html .= '<div id="' . $showdivname . '"></div>' . "\n";
		$html .= '<script language="JavaScript" src="' . esc_attr( $atts['h'] ) . $this->base_url2 . 'Showapi.asp?v=' . $this->version . '&id=' . esc_attr( $atts['id'] ) . '" type="text/javascript"></script>' . "\n";
		$html .= '<script language="JavaScript" type="text/javascript">' . "\n";
		$html .= 'oShow'. esc_attr( $atts['id'] ) .'.BizID="' . $this->business_id . '";' . "\n";
		$html .= 'oShow'. esc_attr( $atts['id'] ) .'.ID="' . esc_attr( $atts['id'] ) . '";' . "\n";
		$html .= ( $atts['h'] !== '' ) ? 'oShow'. esc_attr( $atts['id'] ) .'.sHttp="' . esc_attr( $atts['h'] ) . '";' . "\n" : '';
		$html .= ( $atts['c'] !== '' ) ? 'oShow'. esc_attr( $atts['id'] ) .'.c="' . esc_attr( $atts['c'] ) . '";' . "\n" : '';
		$html .= ( $atts['n'] !== '' ) ? 'oShow'. esc_attr( $atts['id'] ) .'.n="' . esc_attr( $atts['n'] ) . '";' . "\n" : '';
		$html .= ( $atts['lo'] !== '' ) ? 'oShow'. esc_attr( $atts['id'] ) .'.lo="' . esc_attr( $atts['lo'] ) . '";' . "\n" : '';
		$html .= ( $atts['si'] !== '' ) ? 'oShow'. esc_attr( $atts['id'] ) .'.si="' . esc_attr( $atts['si'] ) . '";' . "\n" : '';
		$html .= ( $atts['sd'] !== '' ) ? 'oShow'. esc_attr( $atts['id'] ) .'.sd="' . esc_attr( $atts['sd'] ) . '";' . "\n" : '';
		$html .= ( $atts['isw'] !== '' ) ? 'oShow'. esc_attr( $atts['id'] ) .'.iStoryWidth="' . esc_attr( $atts['isw'] ) . '";' . "\n" : '';
		$html .= ( $atts['ish'] !== '' ) ? 'oShow'. esc_attr( $atts['id'] ) .'.iStoryHeight="' . esc_attr( $atts['ish'] ) . '";' . "\n" : '';
		$html .= ( $atts['ismw'] !== '' ) ? 'oShow'. esc_attr( $atts['id'] ) .'.iStoryMaxWidth="' . esc_attr( $atts['ismw'] ) . '";' . "\n" : '';
		$html .= ( $atts['ismh'] !== '' ) ? 'oShow'. esc_attr( $atts['id'] ) .'.iStoryMaxHeight="' . esc_attr( $atts['ismh'] ) . '";' . "\n" : '';
		$html .= ( $atts['ilo'] !== '' ) ? 'oShow'. esc_attr( $atts['id'] ) .'.iLeftOffset="' . esc_attr( $atts['ilo'] ) . '";' . "\n" : '';
		$html .= ( $atts['ito'] !== '' ) ? 'oShow'. esc_attr( $atts['id'] ) .'.iTopOffset="' . esc_attr( $atts['ito'] ) . '";' . "\n" : '';
		$html .= 'getShowData();' . "\n";
		$html .= '</script>' . "\n";
		return $html;
	}

	
	/**
	*********************************************************************************
	 * Define the Quote plugin shortcode
	 * @since  0.1.0
	 * @return string
	*********************************************************************************
	 */
	public function quote_shortcode( $atts ) {
		$html = '';
		$atts = shortcode_atts(
			array(
				'dataonly' => 'false',
				'sdiv' => 'qdiv',
				'h' => 'https',
				'f' => '1',
				'd' => '0',
				'dl' => '2',
				'o' => '',
				's' => ''
			),
			$atts
		);

		$endpoint = $atts['dataonly'] === 'true' ? 'quote3.asp' : 'QuotePlug.asp';

		$url_args = array(
			'b' => $this->business_id,
			'sdiv' => $atts['sdiv'],
			'h' => $atts['h'],
			'f' => $atts['f'],
			's' => $atts['s'],
			'd' => $atts['d'],
			'dl' => $atts['dl'],
			'o' => $atts['o']
		);
		
		$url = add_query_arg( $url_args, esc_attr( $atts['h'] ) . $this->base_url2 . $endpoint );
		if ($atts['dataonly'] == 'false') $html .= '<div id="qdiv"></div>' . "\n";
		$html .= '<script language="JavaScript" src="' . esc_url( $url ) . '" type="text/javascript"></script>' . "\n";
		return $html;
	}
	
	
	/**
	*********************************************************************************
	 * Define the stock plugin shortcode
	 * @since  0.6.8
	 * @return string
	*********************************************************************************
	 */
	public function stock_shortcode( $atts ) {
		$html = '';
		$stockdivname = $this->base_stockdivname;
		$atts = shortcode_atts(
			array(
				'sdiv' => '',
				'h' => 'https',
				'f' => '1',
				'd' => '1',
				'dl' => '2',
				's' => '',
				'css' => ''
			),
			$atts
		);

		$url_args = array(
			'b' => $this->business_id,
			'sdiv' => $atts['sdiv'],
			'h' => $atts['h'],
			'f' => $atts['f'],
			'd' => $atts['d'],
			'dl' => $atts['dl'],
			's' => $atts['s'],
			'css' => $atts['css']
		);
		
		$stockdivname = 'QuoteDiv' . esc_attr( $atts['f'] );
		
		$html .= '<div id="' . $stockdivname . '"></div>' . "\n";
		$html .= '<script language="JavaScript" src="' . esc_url( esc_attr( $atts['h'] ) . $this->base_url2 . 'QuoteApi.asp?v=' . $this->version ) . '" type="text/javascript"></script>' . "\n";
		$html .= '<script language="JavaScript" type="text/javascript">' . "\n";
		$html .= 'oQuote.BizID="' . $this->business_id . '";' . "\n";
		$html .= 'oQuote.sKey="' . $this->key . '";' . "\n";
		$html .= 'oQuote.sDiv="' . $stockdivname . '";' . "\n";
		$html .= ( $atts['h'] !== '' ) ? 'oQuote.sHttp="' . esc_attr( $atts['h'] ) . '";' . "\n" : '';
		$html .= ( $atts['f'] !== '' ) ? 'oQuote.Format="' . esc_attr( $atts['f'] ) . '";' . "\n" : '';
		$html .= ( $atts['d'] !== '' ) ? 'oQuote.Dollar="' . esc_attr( $atts['d'] ) . '";' . "\n" : '';
		$html .= ( $atts['dl'] !== '' ) ? 'oQuote.DecLen="' . esc_attr( $atts['dl'] ) . '";' . "\n" : '';
		$html .= ( $atts['s'] !== '' ) ? 'oQuote.Symbol="' . esc_attr( $atts['s'] ) . '";' . "\n" : '';
		$html .= ( $atts['css'] !== '' ) ? 'oQuote.CSS="' . esc_attr( $atts['css'] ) . '";' . "\n" : '';
		$html .= 'getQuoteData();' . "\n";
		$html .= '</script>' . "\n";
		return $html;
	}
	
	
	/**
	*********************************************************************************
	 * Define the stock plugin shortcode
	 * @since  0.6.9
	 * @return string
	*********************************************************************************
	 */
	public function HistoricalQuote_shortcode( $atts ) {
		$html = '';
		$atts = shortcode_atts(
			array(
				's' => '',
				'h' => 'https',
				'd' => '1',
				'dl' => '2',
				'css' => ''
			),
			$atts
		);

		$url_args = array(
			's' => $atts['s'],
			'h' => $atts['h'],
			'd' => $atts['d'],
			'dl' => $atts['dl'],
			'css' => $atts['css']
		);
				
		$html .= '<div id="HistoricalDiv"></div>' . "\n";
		$html .= '<script language="JavaScript" src="' . esc_url( esc_attr( $atts['h'] ) . $this->base_url2 . 'HistoricalQuoteApi.asp?v=' . $this->version ) . '" type="text/javascript"></script>' . "\n";
		$html .= '<script language="JavaScript" type="text/javascript">' . "\n";
		$html .= 'oHistoric.BizID="' . $this->business_id . '";' . "\n";
		$html .= 'oHistoric.sKey="' . $this->key . '";' . "\n";
		$html .= ( $atts['s'] !== '' ) ? 'oHistoric.Symbol="' . esc_attr( $atts['s'] ) . '";' . "\n" : '';
		$html .= ( $atts['h'] !== '' ) ? 'oHistoric.sHttp="' . esc_attr( $atts['h'] ) . '";' . "\n" : '';
		$html .= ( $atts['d'] !== '' ) ? 'oHistoric.Dollar="' . esc_attr( $atts['d'] ) . '";' . "\n" : '';
		$html .= ( $atts['dl'] !== '' ) ? 'oHistoric.DecLen="' . esc_attr( $atts['dl'] ) . '";' . "\n" : '';
		$html .= ( $atts['css'] !== '' ) ? 'oHistoric.CSS="' . esc_attr( $atts['css'] ) . '";' . "\n" : '';
		$html .= 'getHistoricalData();' . "\n";
		$html .= '</script>' . "\n";
		return $html;
	}
	
	
	/**
	*********************************************************************************
	 * Define the Committee plugin shortcode
	 * @since  0.6.3
	 * @return string
	*********************************************************************************
	 */
	public function comm_shortcode( $atts ) {
		$html = '';
		$atts = shortcode_atts(
			array(
				'h' => 'https'
			),
			$atts
		);

		$url_args = array(
			'h' => $atts['h']
		);

		$url = add_query_arg( $url_args, esc_attr( $atts['h'] ) . $this->base_url2 . $endpoint );
		
		$html .= '<div id="CommDiv"></div>' . "\n";
		$html .= '<script language="JavaScript" src="' . esc_url( esc_attr( $atts['h'] ) . $this->base_url2 . 'Commapi.asp?v=' . $this->version ) . '" type="text/javascript"></script>' . "\n";
		$html .= '<script language="JavaScript" type="text/javascript">' . "\n";
		$html .= 'oComm.BizID="' . $this->business_id . '";' . "\n";
		$html .= 'oComm.sKey="' . $this->key . '";' . "\n";
		$html .= ( $atts['h'] !== '' ) ? 'oComm.sHttp="' . esc_attr( $atts['h'] ) . '";' . "\n" : '';
		$html .= 'getCommData();' . "\n";
		$html .= '</script>' . "\n";
		return $html;
	}

	
	/**
	*********************************************************************************
	 * Define the Director plugin shortcode
	 * @since  0.6.4
	 * @return string
	*********************************************************************************
	 */
	public function directors_shortcode( $atts ) {
		$html = '';
		$atts = shortcode_atts(
			array(
				'h' => 'https'
			),
			$atts
		);

		$url_args = array(
			'h' => $atts['h']
		);
		$url = add_query_arg( $url_args, esc_attr( $atts['h'] ) . $this->base_url2 . $endpoint );
		
		$html .= '<div id="DirDiv"></div>' . "\n";
		$html .= '<script language="JavaScript" src="' . esc_url( esc_attr( $atts['h'] ) . $this->base_url2 . 'DirectorApi.asp?v=' . $this->version ) . '" type="text/javascript"></script>' . "\n";
		$html .= '<script language="JavaScript" type="text/javascript">' . "\n";
		$html .= 'oDir.BizID="' . $this->business_id . '";' . "\n";
		$html .= 'oDir.sKey="' . $this->key . '";' . "\n";
		$html .= 'getDirData();' . "\n";
		$html .= '</script>' . "\n";
		return $html;
	}

	
	/**
	*********************************************************************************
	 * Define the Management plugin shortcode
	 * @since  0.6.4
	 * @return string
	*********************************************************************************
	 */
	public function management_shortcode( $atts ) {
		$html = '';
		$atts = shortcode_atts(
			array(
				'h' => 'https'
			),
			$atts
		);

		$url_args = array(
			'h' => $atts['h']
		);
		$url = add_query_arg( $url_args, esc_attr( $atts['h'] ) . $this->base_url2 . $endpoint );
		
		$html .= '<div id="MgmtDiv"></div>' . "\n";
		$html .= '<script language="JavaScript" src="' . esc_url( esc_attr( $atts['h'] ) . $this->base_url2 . 'MgmtApi.asp?v=' . $this->version ) . '" type="text/javascript"></script>' . "\n";
		$html .= '<script language="JavaScript" type="text/javascript">' . "\n";
		$html .= 'oMgmt.BizID="' . $this->business_id . '";' . "\n";
		$html .= 'oMgmt.sKey="' . $this->key . '";' . "\n";
		$html .= 'getMgmtData();' . "\n";
		$html .= '</script>' . "\n";
		return $html;
	}

	
	/**
	*********************************************************************************
	 * Define the Event Calendar plugin shortcode
	 * @since  0.6.6
	 * @return string
	*********************************************************************************
	 */
	public function cal_shortcode( $atts ) {
		$html = '';
		$atts = shortcode_atts(
			array(
				'sdiv' => '',
				'h' => 'https',
				'c' => '',
				'y' => '',
				'se' => '',
				'm' => '',
				'a' => ''
			),
			$atts
		);

		$url_args = array(
			'sdiv' => $atts['sdiv'],
			'h' => $atts['h'],
			'c' => $atts['c'],
			'y' => $atts['y'],
			'm' => $atts['m']
		);
		
		$caldivname = 'CalDiv' . esc_attr( $atts['m'] ) . esc_attr( $atts['y'] );
		
		$html .= '<div id="' . $caldivname . '"></div>' . "\n";
		$html .= '<script language="JavaScript" src="' . esc_url( esc_attr( $atts['h'] ) . $this->base_url2 . 'Calapi.asp?v=' . $this->version ) . '" type="text/javascript"></script>' . "\n";
		$html .= '<script language="JavaScript" type="text/javascript">' . "\n";
		$html .= 'oCal.BizID="' . $this->business_id . '";' . "\n";
		$html .= 'oCal.sKey="' . $this->key . '";' . "\n";
		$html .= 'oCal.sDiv="' . $caldivname . '";' . "\n";
		$html .= ( $atts['h'] !== '' ) ? 'oCal.sHttp="' . esc_attr( $atts['h'] ) . '";' . "\n" : '';
		$html .= ( $atts['c'] !== '' ) ? 'oCal.Count="' . esc_attr( $atts['c'] ) . '";' . "\n" : '';
		$html .= ( $atts['y'] !== '' ) ? 'oCal.Year="' . esc_attr( $atts['y'] ) . '";' . "\n" : '';'';
		$html .= ( $atts['m'] !== '' ) ? 'oCal.Mode="' . esc_attr( $atts['m'] ) . '";' . "\n" : '';
		$html .= 'getCalData();' . "\n";
		$html .= '</script>' . "\n";
		return $html;
	}

	
	/**
	*********************************************************************************
	 * Define the Institutional Holdings plugin shortcode
	 * @since  0.6.6
	 * @return string
	*********************************************************************************
	 */
	public function institutional_shortcode( $atts ) {
		$html = '';
		$atts = shortcode_atts(
			array(
				'h' => 'https',
				's' => '',
				'c' => ''
			),
			$atts
		);

		$url_args = array(
			'h' => $atts['h'],
			's' => $atts['s'],
			'c' => $atts['c']
		);

		$url = add_query_arg( $url_args, esc_attr( $atts['h'] ) . $this->base_url2 . $endpoint );
		
		$html .= '<div id="InstiDiv"></div>' . "\n";
		$html .= '<script language="JavaScript" src="' . esc_url( esc_attr( $atts['h'] ) . $this->base_url2 . 'InstitutionalAPI.asp?v=' . $this->version ) . '" type="text/javascript"></script>' . "\n";
		$html .= '<script language="JavaScript" type="text/javascript">' . "\n";
		$html .= 'oInsti.BizID="' . $this->business_id . '";' . "\n";
		$html .= 'oInsti.sKey="' . $this->key . '";' . "\n";
		$html .= ( $atts['h'] !== '' ) ? 'oInsti.sHttp="' . esc_attr( $atts['h'] ) . '";' . "\n" : '';
		$html .= ( $atts['s'] !== '' ) ? 'oInsti.Symbol="' . esc_attr( $atts['s'] ) . '";' . "\n" : '';
		$html .= ( $atts['c'] !== '' ) ? 'oInsti.Count=' . esc_attr( $atts['c'] ) . ';' . "\n" : '';
		$html .= 'getInstiData();' . "\n";
		$html .= '</script>' . "\n";
		return $html;
	}

	
	/**
	*********************************************************************************
	 * Define the Insider Holdings plugin shortcode
	 * @since  0.6.6
	 * @return string
	*********************************************************************************
	 */
	public function insiders_shortcode( $atts ) {
		$html = '';
		$atts = shortcode_atts(
			array(
				'h' => 'https',
				's' => '',
				'c' => ''
			),
			$atts
		);

		$url_args = array(
			'h' => $atts['h'],
			's' => $atts['s'],
			'c' => $atts['c']
		);

		$url = add_query_arg( $url_args, esc_attr( $atts['h'] ) . $this->base_url2 . $endpoint );
		
		$html .= '<div id="InsidersDiv"></div>' . "\n";
		$html .= '<script language="JavaScript" src="' . esc_url( esc_attr( $atts['h'] ) . $this->base_url2 . 'InsidersApi.asp?v=' . $this->version ) . '" type="text/javascript"></script>' . "\n";
		$html .= '<script language="JavaScript" type="text/javascript">' . "\n";
		$html .= 'oInsiders.BizID="' . $this->business_id . '";' . "\n";
		$html .= 'oInsiders.sKey="' . $this->key . '";' . "\n";
		$html .= ( $atts['h'] !== '' ) ? 'oInsiders.sHttp="' . esc_attr( $atts['h'] ) . '";' . "\n" : '';
		$html .= ( $atts['s'] !== '' ) ? 'oInsiders.Symbol="' . esc_attr( $atts['s'] ) . '";' . "\n" : '';
		$html .= ( $atts['c'] !== '' ) ? 'oInsiders.Count=' . esc_attr( $atts['c'] ) . ';' . "\n" : '';
		$html .= 'getInsidersData();' . "\n";
		$html .= '</script>' . "\n";
		return $html;
	}

	
	/**
	*********************************************************************************
	 * Define the Financials plugin shortcode
	 * @since  0.6.7
	 * @return string
	*********************************************************************************
	 */
	public function financials_shortcode( $atts ) {
		$findivname = $this->base_findivname;
		$html = '';
		$atts = shortcode_atts(
			array(
				'h' => 'https',
				's' => '',
				'm' => '',
				'dn' => '',
				'c' => ''
			),
			$atts
		);

		$url_args = array(
			'h' => $atts['h'],
			's' => $atts['s'],
			'm' => $atts['m'],
			'dn' => $atts['dn'],
			'c' => $atts['c']
		);

		$url = add_query_arg( $url_args, esc_attr( $atts['h'] ) . $this->base_url2 . $endpoint );
		
		switch ($atts['m']) {
			case '1':
				$findivname='Cash';
				break;
			case '2':
				$findivname='Income';
				break;
			case '3':
				$findivname='Balance';
				break;
		}
		
		$html .= '<div id="' . $findivname . '"></div>' . "\n";
		$html .= '<script language="JavaScript" src="' . esc_url( esc_attr( $atts['h'] ) . $this->base_url2 . 'FundamentalApi.asp?v=' . $this->version ) . '" type="text/javascript"></script>' . "\n";
		$html .= '<script language="JavaScript" type="text/javascript">' . "\n";
		$html .= 'oFinancials.BizID="' . $this->business_id . '";' . "\n";
		$html .= 'oFinancials.sKey="' . $this->key . '";' . "\n";
		$html .= ( $atts['h'] !== '' ) ? 'oFinancials.sHttp="' . esc_attr( $atts['h'] ) . '";' . "\n" : '';
		$html .= ( $atts['s'] !== '' ) ? 'oFinancials.Symbol="' . esc_attr( $atts['s'] ) . '";' . "\n" : '';
		$html .= ( $atts['m'] !== '' ) ? 'oFinancials.Mode="' . esc_attr( $atts['m'] ) . '";' . "\n" : '';
		$html .= ( $atts['c'] !== '' ) ? 'oFinancials.Count=' . esc_attr( $atts['c'] ) . ';' . "\n" : '';
		$html .= 'oFinancials.sDiv="' . $findivname . '";' . "\n";
		$html .= 'getFinancialsData();' . "\n";
		$html .= '</script>' . "\n";
		return $html;
	}

	
	/**
	*********************************************************************************
	 * Define the Library single plugin shortcode
	 * @since  0.1.0
	 * @return string
	*********************************************************************************
	 */
	public function library_latest_item_shortcode( $atts ) {
		$atts = shortcode_atts(
			array(
				'g' => '',
				't' => '',
				'f' => '',
				'i' => '5',
				'off' => '',
				'out' => '',
				'ln' => '',
				'n' => '',
			),
			$atts
		);

		$url_args = array(
			'b' => $this->business_id,
			'n' => '1',
			's' => '0',
			'l' => '1',
			'i' => $atts['i'],
			'g' => $atts['g'],
			't' => $atts['t'],
			'f' => $atts['f'],
			'off' => $atts['off'],
			'out' => $atts['out'],
			'ln' => $atts['ln'],
			'n' => $atts['n']
		);

		$url_args = array_filter( $url_args, array( $this, 'remove_empty_url_args' ) );
		$url = add_query_arg( $url_args, $this->base_url . 'SingleHeadlinePluginData.asp?v=' . $this->version );
		$html = '<script language="JavaScript" src="' . esc_url( $url ) . '" type="text/javascript"></script>';
		return $html;
	}

	
	/**
	*********************************************************************************
	 * Define the Library plugin shortcode
	 * @since  0.1.0
	 * @return string
	*********************************************************************************
	 */
	public function library_headline_shortcode( $atts ) {
		$atts = shortcode_atts(
			array(
				'g' => '',
				't' => '',
				'f' => '',
				'i' => '5'
			),
			$atts
		);

		$url_args = array(
			'b' => $this->business_id,
			'n' => '1',
			's' => '0',
			'l' => '1',
			'i' => $atts['i'],
			'g' => $atts['g'],
			't' => $atts['t'],
			'f' => $atts['f'],
		);
		
		$url_args = array_filter( $url_args, array( $this, 'remove_empty_url_args' ) );
		$url = add_query_arg( $url_args, $this->base_url . 'HeadlinePlugin.asp?v=' . $this->version );
		$html = '<script language="JavaScript" src="' . esc_url( $url ) . '" type="text/javascript"></script>';
		return $html;
	}
	
	
	/**
	*********************************************************************************
	 * Define the Chart Opt-in plugin shortcode
	 * @since  0.1.0
	 * @return string
	*********************************************************************************
	 */
	public function Chart_shortcode( $atts ) {
		$html = '';
		$atts = shortcode_atts(
			array(
				'h' => 'https',
				's' => '',
				'e' => '',
				'c' => '666666',
				'lc' => '666666',
				'lc2' => '666666',
				'cc' => 'ff0000',
				'bgc' => 'f1f1f1',
				'p' => '10d',
				'height' => '600px',
				'width' => '96%'
			),
			$atts
		);

		$html .= '<div id="chartdiv" style="height: ' . esc_attr( $atts['height'] ) . '; width:' . esc_attr( $atts['width'] ) . ';"></div>' . "\n";
		
		$html .= '<script language="JavaScript" src="' . esc_url(esc_attr( $atts['h'] ) . $this->base_url3 . 'profiles/inc/amstockgetJsData.asp?b=' ) . $this->business_id . '&s=' . esc_attr( $atts['s'] ) . '" type="text/javascript"></script>' . "\n";

$html .= '<script type="text/javascript">' . "\n";
$html .= 'var chartData = [];' . "\n";
$html .= 'loadData();' . "\n";
$html .= 'var chart = AmCharts.makeChart("chartdiv", {type: "stock", pathToImages: "' . esc_url(esc_attr( $atts['h'] ) . $this->base_url3 . 'profiles/investor/ChartJS/images/') .'", dataSets: [{color: "#' . $atts['c'] . '",' . "\n";
$html .= 'title:"' . $atts['e'] . ' ' . $atts['s'] . '",' . "\n";
$html .= 'fieldMappings: [{' . "\n";
$html .= 'fromField: "price",' . "\n";
$html .= 'toField: "price"' . "\n";
$html .= '}, {' . "\n";
$html .= 'fromField: "volume",' . "\n";
$html .= 'toField: "volume"' . "\n";
$html .= '}],' . "\n";
$html .= 'dataProvider: chartData, categoryField: "date"' . "\n";
$html .= '}],' . "\n";
$html .= 'panels: [{"titles":[{"text":"' . $atts['e'] . ' ' . $atts['s'] . '","size":18,"alpha":0.5}],' . "\n";
$html .= 'title: "Price",' . "\n";
$html .= 'percentHeight: 70,' . "\n";
$html .= 'stockGraphs: [{' . "\n";
$html .= 'id: "g1",' . "\n";
$html .= 'lineThickness: 2,' . "\n";
$html .= 'valueField: "price",' . "\n";
$html .= 'fillAlphas: 0.2,' . "\n";
$html .= 'balloonText:"[[title]]: <b>$[[value]]</b>"' . "\n";
$html .= '}],' . "\n";
$html .= 'stockLegend: {' . "\n";
$html .= 'valueTextRegular: " $[[value]]",' . "\n";
$html .= 'markerType: "none"' . "\n";
$html .= '}' . "\n";
$html .= '},{' . "\n";
$html .= 'title: "Volume",' . "\n";
$html .= 'percentHeight: 30,' . "\n";
$html .= 'stockGraphs: [{' . "\n";
$html .= 'valueField: "volume",' . "\n";
$html .= 'type: "column",' . "\n";
$html .= 'cornerRadiusTop: 1,' . "\n";
$html .= 'fillAlphas: 1' . "\n";
$html .= '}],' . "\n";
$html .= 'stockLegend: {' . "\n";
$html .= 'valueTextRegular: " [[value]]",' . "\n";
$html .= 'markerType: "none"' . "\n";
$html .= '}' . "\n";
$html .= '}' . "\n";
$html .= '],' . "\n";
$html .= 'chartCursorSettings: {' . "\n";
$html .= 'valueBalloonsEnabled: true,' . "\n";
$html .= 'fullWidth: true,' . "\n";
$html .= 'cursorAlpha: 0.1,' . "\n";
$html .= 'cursorColor: "#' . $atts['cc'] . '",' . "\n";
$html .= 'graphBulletSize: 1,' . "\n";
$html .= 'valueLineBalloonEnabled:true,' . "\n";
$html .= 'valueLineEnabled:true,' . "\n";
$html .= 'valueLineAlpha: 0.7,' . "\n";
$html .= '},' . "\n";
$html .= 'legendSettings: {' . "\n";
$html .= 'color: "#' . $atts['lc'] . '"' . "\n";
$html .= '},' . "\n";
$html .= 'balloon: {' . "\n";
$html .= 'maxWidth:300,' . "\n";
$html .= 'horizontalPadding: 5,' . "\n";
$html .= 'verticalPadding: 3,' . "\n";
$html .= 'textAlign:"left",' . "\n";
$html .= 'borderThickness:1,' . "\n";
$html .= 'borderColor: "#BBBBBB",' . "\n";
$html .= 'borderAlpha:0.2,' . "\n";
$html .= 'fillColor: "#FFFFFF",' . "\n";
$html .= 'cornerRadius: 4,' . "\n";
$html .= 'fillAlpha:1,' . "\n";
$html .= 'offsetY: 10' . "\n";
$html .= '},' . "\n";
$html .= 'periodSelector: {' . "\n";
$html .= 'dateFormat: "MM-DD-YYYY",' . "\n";
$html .= 'periods: [{' . "\n";
$html .= 'period: "DD",' . "\n";
	if ( esc_attr( $atts['p'] )=='10d' ) $html .= 'selected: true,' . "\n";
$html .= 'count: 10,' . "\n";
$html .= 'label: "10 days"' . "\n";
$html .= '}, {' . "\n";
$html .= 'period: "MM",' . "\n";
	if ( esc_attr( $atts['p'] )=='1m' ) $html .= 'selected: true,' . "\n";
$html .= 'count: 1,' . "\n";
$html .= 'label: "1 m"' . "\n";
$html .= '}, {' . "\n";
$html .= 'period: "MM",' . "\n";
	if ( esc_attr( $atts['p'] )=='3m' ) $html .= 'selected: true,' . "\n";
$html .= 'count: 3,' . "\n";
$html .= 'label: "3 m"' . "\n";
$html .= '}, {' . "\n";
$html .= 'period: "MM",' . "\n";
	if ( esc_attr( $atts['p'] )=='6m' ) $html .= 'selected: true,' . "\n";
$html .= 'count: 6,' . "\n";
$html .= 'label: "6 m"' . "\n";
$html .= '}, {' . "\n";
$html .= 'period: "YYYY",' . "\n";
	if ( esc_attr( $atts['p'] )=='1y' ) $html .= 'selected: true,' . "\n";
$html .= 'count: 1,' . "\n";
$html .= 'label: "1 year"' . "\n";
$html .= '}, {' . "\n";
$html .= 'period: "MAX",' . "\n";
	if ( esc_attr( $atts['p'] )=='max' ) $html .= 'selected: true,' . "\n";
$html .= 'label: "MAX"' . "\n";
$html .= '}]' . "\n";
$html .= '},' . "\n";
$html .= 'panelsSettings: {' . "\n";
$html .= 'usePrefixes: true,' . "\n";
$html .= 'color: "#' . $atts['lc2'] . '",' . "\n";
$html .= 'plotAreaFillColors: "#' . $atts['bgc'] . '",' . "\n";
$html .= 'plotAreaFillAlphas: 1.0,' . "\n";
$html .= 'plotAreaBorderColor: "#999",' . "\n";
$html .= 'plotAreaBorderAlpha: 0.5,' . "\n";
$html .= 'marginLeft: 60,' . "\n";
$html .= 'marginTop: 5,' . "\n";
$html .= 'marginBottom: 5,' . "\n";
$html .= 'creditsPosition: "top-left"' . "\n";
$html .= '},' . "\n";
$html .= 'valueAxesSettings: {' . "\n";
$html .= 'inside: false' . "\n";
$html .= '},' . "\n";
$html .= 'chartScrollbarSettings: {' . "\n";
$html .= 'graph: "g1"' . "\n";
$html .= '},' . "\n";
$html .= 'categoryAxesSettings: {' . "\n";
$html .= 'equalSpacing: true' . "\n";
$html .= '}' . "\n";
$html .= '});' . "\n";
$html .= '</script>' . "\n";
	return $html;
}
	
	
	/**
	*********************************************************************************
	 */
	protected function remove_empty_url_args( $arg ) {
		return ( $arg !== '' );
	}
}

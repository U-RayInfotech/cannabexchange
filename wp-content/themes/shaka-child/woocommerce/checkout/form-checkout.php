<?php
/**
 * Checkout Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-checkout.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.0
 */

if (!defined('ABSPATH')) {
	exit;
}

wc_print_notices();

do_action('woocommerce_before_checkout_form', $checkout);

// If checkout registration is disabled and not logged in, the user cannot checkout
if (!$checkout->is_registration_enabled() && $checkout->is_registration_required() && !is_user_logged_in()) {
	echo apply_filters('woocommerce_checkout_must_be_logged_in_message', __('You must be logged in to checkout.', 'woocommerce'));
	return;
}

?>
<form name="checkout" id="checkout" method="post" class="checkout woocommerce-checkout" action="<?php echo esc_url(wc_get_checkout_url()); ?>" enctype="multipart/form-data">

	<?php if ($checkout->get_checkout_fields()): ?>

		<?php //do_action('woocommerce_checkout_before_customer_details');?>
		<fieldset id="step1" class="active">
			<div class="col2-set" id="customer_details">
				<div class="col-1">
					<?php do_action('woocommerce_checkout_billing');?>
				</div>

				<div class="col-2">
					<?php do_action('woocommerce_checkout_shipping');?>
				</div>
			</div>

		<?php do_action('woocommerce_checkout_after_customer_details');?>
		</fieldset>
	<?php endif;?>
		<fieldset id="step2" class="hidden">
			<h1>Press Release From</h1>
			<div class="col2-set">
				<div class="col-1">

					<p class="form-row form-row-wide" >
						<label for="press_title" class="">Title&nbsp;<span title="required" class="required">*</span></label>
						<span class="woocommerce-input-wrapper">
							<input type="text" class="input-text required" name="press_title" id="press_title" placeholder="" value="" autocomplete="organization">
						</span>
					</p>
					<p class="form-row form-row-wide" >
						<label for="press_subtitle" class="">Sub Title&nbsp;<span title="required" class="required">*</span></label>
						<span class="woocommerce-input-wrapper">
							<input type="text" class="input-text required" name="press_subtitle" id="press_subtitle" placeholder="" value="" autocomplete="organization">
						</span>
					</p>
					<p class="form-row form-row-wide" >
						<label for="press_content" class="">Content&nbsp;<span title="required" class="required">*</span></label>
						<!-- <textarea id="press_content" name="press_content"></textarea> -->

						<?php
$settings = array(
	'textarea_name' => 'press_content',
	'tinymce' => true,
);
wp_editor('', 'press_content', $settings);?>
					</p>
					<p class="form-row form-row-wide" >
						<label for="press_location" class="">Location&nbsp;<span title="required" class="required">*</span></label>
						<span class="woocommerce-input-wrapper">
							<input type="text" class="input-text required" name="press_location" id="press_location" placeholder="" value="" autocomplete="organization">
						</span>
					</p>
					<?php
$products = array();
foreach (WC()->cart->get_cart() as $cart_item) {
	$product = $cart_item['data'];
	if (!empty($product)) {
		$products[] = $product->id;
	}
}
if (in_array("8", $products)) {$cate_limit = 2;}
?>
					<p class="form-row form-row-wide" >
						<label for="press_category" class="">Category&nbsp;<span title="required" class="required">*</span> <small>(You can select <?php echo $cate_limit; ?> categories as per your package)</small></label>
						<span class="woocommerce-input-wrapper">
							<!-- <input type="text" class="input-text " name="press_subtitle" id="press_subtitle" placeholder="" value="" autocomplete="organization"> -->

							<select class="required input-text" data-limit="<?php echo $cate_limit; ?>" multiple="" name="press_category[]" id="press_category">
								<option value="">Select Category</option>
								<?php
$cats = get_terms('press_release_category', 'hide_empty=0');
foreach ($cats as $cat) {
	?>
								<option value="<?php echo $cat->term_id; ?>"><?php echo $cat->name; ?></option>
								<?php }?>
							</select>
						</span>
					</p>

				</div>
			</div>
		</fieldset>
		<fieldset id="step3" class="hidden">
			<h1>All details</h1>

		</fieldset>
		<fieldset id="step4" class="hidden">
			<h3 id="order_review_heading"><?php _e('Your order', 'woocommerce');?></h3>

			<?php do_action('woocommerce_checkout_before_order_review');?>

			<div id="order_review" class="woocommerce-checkout-review-order">
				<?php do_action('woocommerce_checkout_order_review');?>
			</div>

			<?php // do_action('woocommerce_checkout_after_order_review');?>
		</fieldset>
		<button type="button" data-id="1" id="previous" class="hidden btn btn-primary">Previous</button>
		<button type="button" data-id="1" id="next" class="btn btn-primary">Next</button>
</form>

<?php do_action('woocommerce_after_checkout_form', $checkout);?>

<?php
/**
 * Footer
 *
 * @package shaka-pt
 */
$shaka_footer_widgets_layout = ShakaHelpers::footer_widgets_layout_array();
$shaka_footer_allowed_html = array(
	'a'      => array(
		'class'   => array(),
		'href'   => array(),
		'target' => array(),
		'title'  => array(),
	),
	'em'     => array(),
	'strong' => array(),
	'img'    => array(
		'src'    => array(),
		'alt'    => array(),
		'width'  => array(),
		'height' => array(),
	),
	'span'   => array(
		'class'  => array(),
		'style'  => array(),
	),
	'i'      => array(
		'class'  => array(),
	),
);

?>

	<footer class="footer">
		<!-- Footer Top -->
		<?php if ( ! empty( $shaka_footer_widgets_layout ) && is_active_sidebar( 'footer-widgets' ) ) : ?>
		<div class="footer-top">
			<div class="container">
				<div class="row">
					<?php
					if ( is_active_sidebar( 'footer-widgets' ) ) {
						dynamic_sidebar( 'footer-widgets' );
					}
					?>
				</div>
			</div>
		</div>
		<?php endif; ?>
		<!-- Footer Bottom -->
		<div class="footer-bottom">
			<div class="container">
				<div class="row">
					<div class="col-xs-12  col-lg-4">
						<div class="footer-bottom__left">
							<?php echo wp_kses( do_shortcode( apply_filters( 'shaka_footer_bottom_left_txt', get_theme_mod( 'footer_bottom_left_txt', '<a href="https://www.proteusthemes.com/wordpress-themes/shaka/">Shaka Theme</a> - Made by ProteusThemes.' ) ) ), $shaka_footer_allowed_html ); ?>
						</div>
					</div>
					<div class="col-xs-12  col-lg-4">
						<div class="footer-bottom__center">
							<?php echo wp_kses( do_shortcode( apply_filters( 'shaka_footer_bottom_center_txt', get_theme_mod( 'footer_bottom_center_txt', '[fa icon="fa-facebook" href="https://www.facebook.com/ProteusThemes/"] [fa icon="fa-youtube" href="https://www.youtube.com/user/ProteusNetCompany"] [fa icon="fa-twitter" href="https://twitter.com/ProteusThemes"]' ) ) ), $shaka_footer_allowed_html ); ?>
						</div>
					</div>
					<div class="col-xs-12  col-lg-4">
						<div class="footer-bottom__right">
							<?php echo wp_kses( do_shortcode( apply_filters( 'shaka_footer_bottom_right_txt', get_theme_mod( 'footer_bottom_right_txt', '&copy; ' . date( 'Y' ) . ' Shaka. All rights reserved.' ) ) ), $shaka_footer_allowed_html ); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>
	</div><!-- end of .boxed-container -->

	<?php wp_footer(); ?>

	<?php if (is_page('checkout')) {?>
		<script type="text/javascript">
			jQuery(window).load(function($){
				jQuery(document.getElementById('press_content_ifr').contentWindow.document).keydown(function() {
					var data = tinymce.activeEditor.getContent();
						jQuery('#press_content').val(data);
						if(data != '' && data != undefined){
							jQuery('#press_content-error').fadeOut();
						}
				});
				jQuery('.woocommerce-form-login-toggle .woocommerce-info a.showlogin').attr('href','/login');
				jQuery('.woocommerce-form-login-toggle .woocommerce-info a').removeClass('showlogin');
			});

			jQuery(document).ready(function($){
				$("#billing_first_name, #billing_last_name, #billing_address_1, #billing_city, #billing_postcode, #billing_country, #billing_state, #billing_email, #billing_phone, #press_content").addClass("required");
				$(document).keydown(function(event){
					var keycode = (event.keyCode ? event.keyCode : event.which);
					if(keycode == '13'){
						$('#next').trigger('click');
					}

				});

				if($('#order_review').length > 0){
					$('#place_order').attr("disabled", true);
				}
				$('#next').click(function(event){
					event.preventDefault();
					var id = $(this).attr('data-id');
					var nextId = parseInt(id)+1;
					if(id == 1){
						var res = billing_validate();
						if(res == false){
							return false;
						}

					} else if(id == 2){
					var res = press_validate();
						if(res == false){
							return false;
						}
						$("#step3").html('');
						$("#step3").append('<h1>All details</h1>');

						$('.input-text').each(function(){
							var id = $(this).attr('id');
							var label = $(this).parent().parent().find('label').html();
							var newhtml = '';
							if(id != 'username' && id != 'password') {
								if(id == "press_category"){
									var val = $('#'+id).find(":selected").text()+'</br>';
									val = $("#"+id+" option:selected").map(function () {
								        return $(this).text();
								    }).get().join(', ');
									newhtml = "<p id='"+id+"_data'><label>"+label+"</label>: "+val+"</p>";
									newhtml = newhtml.replace('<span title="required" class="required">*</span>', "");
									newhtml = newhtml.replace('<small>(You can select 2 categories as per your package)</small>', "");
									localStorage.setItem(id, $(this).val());
								} else if(id == "billing_address_2"){
									$('#billing_address_1_data').append(', '+$(this).val());

								} else if($(this).val()){
									newhtml = "<p id='"+id+"_data'><label>"+label+"</label>: "+$(this).val()+"</p>";
									//newht	ml.replaceWith('');
									newhtml = newhtml.replace('<abbr class="required" title="required">*</abbr>', "");
									newhtml = newhtml.replace('<span title="required" class="required">*</span>', "");
									localStorage.setItem(id, $(this).val());

								}
							$("#step3").append(newhtml);
							label = "",id = "", newhtml="";
							}
						});

						$('.select2-hidden-accessible').each(function(){
							if($(this).val()){
								var id = $(this).attr('name');
								var label = $(this).parent().parent().find('label').html();
								var val = $('#'+id).find(":selected").text();
								var newhtml = "<p id='"+id+"_data'><label>"+label+"</label>: "+val+"</p>";
								//newhtml.replaceWith('');
								newhtml = newhtml.replace('<abbr class="required" title="required">*</abbr>', "");
								localStorage.setItem(id, $(this).val());
								//alert("select");

								//$("#step3").append(newhtml);
								$( newhtml ).insertAfter( "#billing_city_data" );
							}
							label = "",id = "", newhtml="";
						});

						var newhtml = "<p id='press_content_data'><label>Content&nbsp</label>: "+$("#press_content").val()+"</p>";
						$( newhtml ).insertAfter( "#press_subtitle_data" );
						localStorage.setItem("press_content", $("#press_content").val());
						$('<h3>Personal Details</h3>').insertBefore( "#billing_first_name_data" );
						$('<h3>Press Release Details</h3>').insertAfter( "#billing_email_data" );
					}

					$('#step'+id).hide().removeClass('active');
					$('#step'+nextId).removeClass('hidden').addClass('active');
					$('#step'+nextId).fadeIn();
					if(nextId > 1){
						$('#previous').fadeIn().removeClass('hidden');
					} else{
						$('#previous').fadeout().addClass('hidden');
					}
					$(this).attr('data-id', nextId);
					$("#previous").attr('data-id', nextId);
					if(nextId == 4){
						$(this).addClass('hidden');
						$("#previous").insertBefore("#place_order");
					}

					if(id == 3){
						$('#place_order').removeAttr('disabled');
					}
				});

				$('#previous').click(function(){
					var id = $(this).attr('data-id');
					var currentId = parseInt(id)-1;
					if(id == 4){
						$('#place_order').attr('disabled', true);
					}

					$('#step'+id).hide().removeClass('active');
					$('#step'+currentId).removeClass('hidden').addClass('active');
					$('#step'+currentId).fadeIn();

					$("#next").attr('data-id', currentId);
					$(this).attr('data-id', currentId);
					if(currentId == 3){
						$("#previous").insertBefore("#next");
					}

					if(currentId != 4){
						$("#next").removeClass('hidden');
					}
					if(currentId == 1){
						$(this).addClass('hidden');
					}
				});

				function billing_validate(){
					var validator = $( "#checkout" ).validate();
					var billing_first_name = validator.element( "#billing_first_name" ),
					billing_last_name = validator.element( "#billing_last_name" ),
					billing_address_1 = validator.element( "#billing_address_1" ),
					billing_postcode = validator.element( "#billing_postcode" ),
					billing_country = validator.element( "#billing_country" ),
					billing_state = validator.element( "#billing_state" ),
					billing_email = validator.element( "#billing_email" ),
					billing_phone = validator.element( "#billing_phone" ),
					billing_city = validator.element( "#billing_city" );
					//alert(first);
					if(billing_first_name == false ||
						billing_postcode == false ||
						billing_last_name == false ||
						billing_address_1 == false ||
						billing_country == false ||
						billing_state == false ||
						billing_email == false ||
						billing_phone == false ||
						billing_city == false  ){
						return false;
					} else {
						return true;
					}
				}

				function press_validate(){
					var validator = $( "#checkout" ).validate();
					validator.settings.ignore = "";
					var press_title = validator.element( "#press_title" ),
					press_subtitle = validator.element( "#press_subtitle" ),
					press_content = validator.element( "#press_content" ),
					press_location = validator.element( "#press_location" ),
					press_category = validator.element( "#press_category" );
					//alert(first);
					if(press_title == false ||
						press_subtitle == false ||
						press_content == false ||
						press_location == false ||
						press_category == false ){
						return false;
					} else {
						return true;
					}
				}

				// $('.input-text').change(function(){
				// 	if($(this).val()){
				// 		var id = $(this).attr('name');
				// 		var label = $(this).parent().parent().find('label').html();
				// 		var newhtml = "<p><label>"+label+"</label> : "+$(this).val()+"</p>";
				// 		$("#step3").append(newhtml);
				// 	}
				// 	label = "",id = "", newhtml="";
				// });

				var last_valid_selection = null;
				$('#press_category').change(function(event) {

					if ($(this).val().length > $(this).attr('data-limit')) {
						$(this).val(last_valid_selection);
					} else {
					  last_valid_selection = $(this).val();
					}
				});
			});

		</script>
	<?php }?>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>

	</body>
</html>

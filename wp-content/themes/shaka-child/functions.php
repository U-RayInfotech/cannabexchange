<?php
function my_theme_enqueue_styles() {

	$parent_style = 'parent-style'; // This is 'twentyfifteen-style' for the Twenty Fifteen theme.

	wp_enqueue_style($parent_style, get_template_directory_uri() . '/style.css');
	wp_enqueue_style('child-style',
		get_stylesheet_directory_uri() . '/style.css',
		array($parent_style),
		wp_get_theme()->get('Version')
	);

	//wp_localize_script('ajax', 'myajax', array('ajax_url' => admin_url('admin-ajax.php')));
}
add_action('wp_enqueue_scripts', 'my_theme_enqueue_styles');

/* WooCommerce: The Code Below Removes The Additional Information Tab */
add_filter('woocommerce_product_tabs', 'woo_remove_product_tabs', 98);
function woo_remove_product_tabs($tabs) {
	unset($tabs['additional_information']);
	return $tabs;
}

/* WooCommerce: The Code Below Removes The Additional Information Title Text */
add_filter('woocommerce_enable_order_notes_field', '__return_false');

function disable_checkout_button_no_shipping() {

	remove_action('woocommerce_proceed_to_checkout', 'woocommerce_button_proceed_to_checkout', 20);
	echo '<a href="#" class="checkout-button button alt wc-forward">Proceed to checkout</a>';

}
add_filter('the_editor', 'add_required_attribute_to_wp_editor', 10, 1);

function add_required_attribute_to_wp_editor($editor) {
	if (is_page('checkout')) {
		$editor = str_replace('<textarea', '<textarea required="required"', $editor);
	}
	return $editor;
}

add_action('woocommerce_proceed_to_checkout', 'disable_checkout_button_no_shipping', 1);

/**
 * Pre-populate Woocommerce checkout fields
 */
// add_filter('woocommerce_checkout_get_value', function($input, $key ) {
// 	global $current_user;
// 	switch ($key) :
// 		case 'billing_first_name':
// 		case 'shipping_first_name':
// 			return $current_user->first_name;
// 		break;

// 		case 'billing_last_name':
// 		case 'shipping_last_name':
// 			return $current_user->last_name;
// 		break;
// 		case 'billing_email':
// 			return $current_user->user_email;
// 		break;
// 		case 'billing_phone':
// 			return $current_user->phone;
// 		break;
// 	endswitch;
// }, 10, 2);
//

add_action('wp_ajax_nopriv_store_press_release', 'store_press_release');
function store_press_release() {
	$user_id = get_current_user_id();
	$post_data = array(
		'post_author' => 1,
		'post_content' => $_POST['press_content'],
		'post_title' => $_POST['press_title'],
		'post_status' => 'pending',
		'post_type' => 'press_release',
	);

	$post_id = wp_insert_post($post_data);

	if (!is_wp_error($post_id)) {
		$tag = explode(',', $_POST['press_category']);
		wp_set_post_terms($post_id, $tag, 'press_release_category');

		update_post_meta($post_id, 'press_location', $_POST['press_location']);
		update_post_meta($post_id, 'press_release_subheadline', $_POST['press_subtitle']);
	} else {
		//there was an error in the post insertion,
		echo $post_id->get_error_message();
	}
	echo 'success';
	exit;
}

?>
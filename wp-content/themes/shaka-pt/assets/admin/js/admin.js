/**
 * Utilities for the admin dashboard
 */

jQuery( document ).ready( function( $ ) {
	'use strict';

	// Select Icon on Click
	$( 'body' ).on( 'click', '.js-selectable-icon', function ( ev ) {
		ev.preventDefault();
		var $this = $( this );
		$this.siblings( '.js-icon-input' ).val( $this.data( 'iconname' ) ).change();
	} );

	// Show/hide CTA box on change (Instagram widget).
	$( document ).on( 'change', '.js-cta-box-control', function() {
		if( $( this ).is( ':checked' ) ) {
			$( this ).parent().siblings( '.js-cta-box' ).show();
		}
		else {
			$( this ).parent().siblings( '.js-cta-box' ).hide();
		}
	});

	// Show/hide CTA url inputs on url_type change (Special Offer widget).
	$( document ).on( 'change', '.js-pt-feature_cta_type', function() {
		$( this ).parent().siblings( '.js-pt-feature-custom-url, .js-pt-feature-wc-product' ).toggle();
	});

} );


/********************************************************
 			Jetpack Gallery Settings
********************************************************/
// src: Jetpack, file: _inc/gallery-settings.js

jQuery( document ).ready( function( $ ) {
	var media = wp.media;

	// Wrap the render() function to append controls.
	media.view.Settings.Gallery = media.view.Settings.Gallery.extend({
		render: function() {
			var $el = this.$el;


			media.view.Settings.prototype.render.apply( this, arguments );

			// Append the type template and update the settings.
			$el.append( media.template( 'shaka-gallery-settings' ) );
			media.gallery.defaults.type = 'default'; // lil hack that lets media know there's a type attribute.
			this.update.apply( this, ['type'] );

			// Hide the Columns setting for all types except Default
			$el.find( 'select[name=type]' ).on( 'change', function () {

				var columnSetting = $el.find( 'select[name=columns]' ).closest( 'label.setting' );

				if ( 'wp' === $( this ).val() || 'thumbnails' === $( this ).val() ) {
					columnSetting.show();
				} else {
					columnSetting.hide();
				}
			} ).change();

			return this;
		}
	});
} );


/********************************************************
 			Backbone code for repeating fields in widgets
********************************************************/

// Namespace for Backbone elements
window.Shaka = {
	Models:    {},
	ListViews: {},
	Views:     {},
	Utils:     {},
};

/**
 ******************** Backbone Models *******************
 */

_.extend( Shaka.Models, {
	HeroColumn: Backbone.Model.extend( {
		defaults: {
			'title':                '',
			'content':              '',
			'bg_image':             '',
			'bg_opacity':           '',
			'link_text':            '',
			'link_url':             '',
			'link_open_in_new_tab': '',
		}
	} ),

	ContactProfileItem: Backbone.Model.extend( {
		defaults: {
			'text': '',
			'icon': 'fa-home',
		}
	} ),

	FeatureItem: Backbone.Model.extend( {
		defaults: {
			'text': '',
			'icon': 'fa-plus',
		}
	} ),
} );

/**
 ******************** Backbone Views *******************
 */

// Generic single view that others can extend from
Shaka.Views.Abstract = Backbone.View.extend( {
	initialize: function ( params ) {
		this.templateHTML = params.templateHTML;

		return this;
	},

	render: function () {
		this.$el.html( Mustache.render( this.templateHTML, this.model.attributes ) );

		return this;
	},

	destroy: function ( ev ) {
		ev.preventDefault();

		this.remove();
		this.model.trigger( 'destroy' );
	},
} );

_.extend( Shaka.Views, {

	// View of a single hero column
	HeroColumn: Shaka.Views.Abstract.extend( {
		className: 'pt-widget-single-hero-column',

		events: {
			'click .js-pt-remove-hero-column': 'destroy',
		},

		render: function () {
			this.$el.html( Mustache.render( this.templateHTML, this.model.attributes ) );
			return this;
		},
	} ),

	// View of a single contact profile item
	ContactProfileItem: Shaka.Views.Abstract.extend( {
		className: 'pt-widget-single-contact-profile-item',

		events: {
			'click .js-pt-remove-contact-profile-item': 'destroy',
		},

		render: function () {
			this.$el.html( Mustache.render( this.templateHTML, this.model.attributes ) );
			this.$( 'input.js-icon-input' ).val( this.model.get( 'icon' ) );
			return this;
		},
	} ),

	// View of a single feature item
	FeatureItem: Shaka.Views.Abstract.extend( {
		className: 'pt-widget-single-feature-item',

		events: {
			'click .js-pt-remove-feature-item': 'destroy',
		},

		render: function () {
			this.$el.html( Mustache.render( this.templateHTML, this.model.attributes ) );
			this.$( 'input.js-icon-input' ).val( this.model.get( 'icon' ) );
			return this;
		},
	} ),

} );



/**
 ******************** Backbone ListViews *******************
 *
 * Parent container for multiple view nodes.
 */

Shaka.ListViews.Abstract = Backbone.View.extend( {

	initialize: function ( params ) {
		this.widgetId     = params.widgetId;
		this.itemsModel   = params.itemsModel;
		this.itemView     = params.itemView;
		this.itemTemplate = params.itemTemplate;

		// Cached reference to the element in the DOM
		this.$items = this.$( params.itemsClass );

		// Collection of items(locations, people, testimonials,...),
		this.items = new Backbone.Collection( [], {
			model: this.itemsModel
		} );

		// Listen to adding of the new items
		this.listenTo( this.items, 'add', this.appendOne );

		return this;
	},

	addNew: function ( ev ) {
		ev.preventDefault();

		var currentMaxId = this.getMaxId();

		this.items.add( new this.itemsModel( {
			id: (currentMaxId + 1)
		} ) );

		return this;
	},

	getMaxId: function () {
		if ( this.items.isEmpty() ) {
			return -1;
		}
		else {
			var itemWithMaxId = this.items.max( function ( item ) {
				return parseInt( item.id, 10 );
			} );

			return parseInt( itemWithMaxId.id, 10 );
		}
	},

	appendOne: function ( item ) {
		var renderedItem = new this.itemView( {
			model:        item,
			templateHTML: jQuery( this.itemTemplate + this.widgetId ).html()
		} ).render();

		var currentWidgetId = this.widgetId;

		// If the widget is in the initialize state (hidden), then do not append a new item
		if ( '__i__' !== currentWidgetId.slice( -5 ) ) {
			this.$items.append( renderedItem.el );
		}

		return this;
	}
} );

// Collection of all locations, but associated with each individual widget
_.extend( Shaka.ListViews, {

	// Collection of all pricing list items, but associated with each individual widget
	HeroColumns: Shaka.ListViews.Abstract.extend( {
		events: {
			'click .js-pt-add-hero-column': 'addNew'
		}
	} ),

	// Collection of all contact profile items, but associated with each individual widget
	ContactProfileItems: Shaka.ListViews.Abstract.extend( {
		events: {
			'click .js-pt-add-contact-profile-item': 'addNew'
		}
	} ),

	// Collection of all feature items, but associated with each individual widget
	FeatureItems: Shaka.ListViews.Abstract.extend( {
		events: {
			'click .js-pt-add-feature-item': 'addNew'
		}
	} ),
} );

/**
 ******************** Repopulate Functions *******************
 */

_.extend( Shaka.Utils, {
	// Generic repopulation function used in all repopulate functions
	repopulateGeneric: function ( collectionType, parameters, json, widgetId ) {
		var collection = new collectionType( parameters );

		// Convert to array if needed
		if ( _( json ).isObject() ) {
			json = _( json ).values();
		}

		// Add all items to collection of newly created view
		collection.items.add( json, { parse: true } );
	},

	/**
	 * Function which adds the existing pricing list items to the DOM
	 * @param  {json} heroColumnJSON
	 * @param  {string} widgetId ID of widget from PHP $this->id
	 * @return {void}
	 */
	repopulateHeroColumns: function ( heroColumnJSON, widgetId ) {
		var parameters = {
			el:           '#hero-columns-' + widgetId,
			widgetId:     widgetId,
			itemsClass:   '.hero-columns',
			itemTemplate: '#js-pt-hero-column-',
			itemsModel:   Shaka.Models.HeroColumn,
			itemView:     Shaka.Views.HeroColumn,
		};

		this.repopulateGeneric( Shaka.ListViews.HeroColumns, parameters, heroColumnJSON, widgetId );
	},

	/**
	 * Function which adds the existing contact profile items to the DOM
	 * @param  {json} contactProfileItemJSON
	 * @param  {string} widgetId ID of widget from PHP $this->id
	 * @return {void}
	 */
	repopulateContactProfileItems: function ( contactProfileItemJSON, widgetId ) {
		var parameters = {
			el:           '#contact-profile-items-' + widgetId,
			widgetId:     widgetId,
			itemsClass:   '.contact-profile-items',
			itemTemplate: '#js-pt-contact-profile-item-',
			itemsModel:   Shaka.Models.ContactProfileItem,
			itemView:     Shaka.Views.ContactProfileItem,
		};

		this.repopulateGeneric( Shaka.ListViews.ContactProfileItems, parameters, contactProfileItemJSON, widgetId );
	},

	/**
	 * Function which adds the existing Feature items to the DOM
	 * @param  {json} featureItemsJSON
	 * @param  {string} widgetId ID of widget from PHP $this->id
	 * @return {void}
	 */
	repopulateFeatureItems: function ( featureItemsJSON, widgetId ) {
		var parameters = {
			el:           '#feature-items-' + widgetId,
			widgetId:     widgetId,
			itemsClass:   '.feature-items',
			itemTemplate: '#js-pt-feature-item-',
			itemsModel:   Shaka.Models.FeatureItem,
			itemView:     Shaka.Views.FeatureItem,
		};

		this.repopulateGeneric( Shaka.ListViews.FeatureItems, parameters, featureItemsJSON, widgetId );
	},
} );

define( [ 'jquery', 'underscore', 'stampit', 'assets/js/funky-item/funky-item' ], function( $, _, stampit, funkyItem ) {
	return stampit( {
		init: function() {
			var funkyItems = [];

			this.createFunkyItem = function( opts ) {
				var item = funkyItem( opts );

				funkyItems.push( item );

				return item;
			};

			this.getFunkyItemOfHtmlEl = function( $el ) {
				return _.find( funkyItems, function( singleFunkyItem ) {
					return singleFunkyItem.$el.is( $el );
				} );
			};

			this
				.addAllFunkyUnderlines()
				.addEventListeners();

			return this;
		},

		props: {
			$menuItems:        null,
			regularFillColor:  '',
			activeItemSel:     '',
			activeFillColor:   '',
			funkyItemDefaults: {
				type:            'underline',
				drunkness:       5,
				canvasHtmlClass: 'funky-underline',
				css: {
					bottom: 10,
					left:   10,
					right:  10,
				},
				responsive: [
					{
						min: 992,
						max: Infinity,
					}
				],
				dimensions: {
					height: 15,
				}
			},
			optsModifier: null, // clb function
			onclick: null, // clb function
		},

		methods: {
			addAllFunkyUnderlines: function() {
				this.$menuItems.each( _.bind(function( index, el ) {
					var $el = $( el );

					if ( $el.is( this.activeItemSel ) ) {
						this.addActiveUnderline( $el );
					}
					else {
						this.addRegularUnderlines( $el );
					}
				}, this ) );

				return this;
			},

			addRegularUnderlines: function( $el ) {
				var opts = this.regularUnderlinesFunkyItemOpts( $el );

				opts = this.modifyOpts( opts, $el, false );

				this.createFunkyItem( opts );

				return this;
			},

			addActiveUnderline: function( $el ) {
				var opts = this.activeUnderlinesFunkyItemOpts( $el );

				opts = this.modifyOpts( opts, $el, true );

				this.createFunkyItem( opts );

				return this;
			},

			activeUnderlinesFunkyItemOpts: function( $el ) {
				return _.extend( {}, this.regularUnderlinesFunkyItemOpts( $el ), {
					fillColor: this.activeFillColor,
					rerenderOnHover: false,
				} );
			},

			regularUnderlinesFunkyItemOpts: function( $el ) {
				return _.extend( {}, this.funkyItemDefaults, {
					fillColor: this.regularFillColor,
					rerenderOnHover: true,
					el: $el,
				} );
			},

			modifyOpts: function( opts, $el, isActiveItem ) {
				if ( _.isFunction( this.optsModifier ) ) {
					opts = this.optsModifier( $.extend( true, {}, opts ), $el, isActiveItem ); // $.extend( true, ...) = deep clone
				}

				return opts;
			},

			addEventListeners: function() {
				if ( _.isFunction( this.onclick ) ) {
					this.$menuItems.on( 'click', 'a', _.bind( function( ev ) {
						this.onclick( ev );
					}, this ) );
				}

				return this;
			},

			// changes the fill colors of two funky items
			swapRenderColors: function( funkyItem1, funkyItem2, render ) {
				var fill1 = funkyItem1.fillColor,
					fill2 = funkyItem2.fillColor;

				funkyItem1
					.setFillStyle( fill2 )
					.renderIfAllowed();

				funkyItem2
					.setFillStyle( fill1 )
					.renderIfAllowed();

				return this;
			}
		}
	} );
} );

define( [ 'underscore', 'stampit', 'assets/js/funky-item/module/drunk-box' ], function( _, stampit, drunkBox ) {
	return stampit( {
		init: function() {
			this.currentBox = this.createNewRandomizedBox();

			this.renderIfAllowed();
		},

		props: {
			animationDuration: 100, // ms
			animationProgress: 0, // 0-1
		},

		methods: {
			createNewBox: function( boxOpts ) {
				boxOpts = boxOpts || {};
				boxOpts = _.defaults( boxOpts, {
					width:     this.getCalculatedWidth(),
					height:    this.dimensions.height,
					drunkness: this.drunkness,
				} );

				return drunkBox(boxOpts);
			},

			createNewRandomizedBox: function( boxOpts ) {
				return this
					.createNewBox()
					.randomizeCoordinates();
			},

			renderIfAllowed: function( box ) {
				var willRender = true;

				if ( ! this.shouldBeRenderedAtWindowWidth() ) {
					willRender = false;
				}

				if ( ! this.$el.is(':visible') ) {
					willRender = false;
				}

				if ( willRender ) {
					this.render( box );
				}

				return this;
			},

			render: function( box ) {
				box = box || this.currentBox;

				this.cleanCanvas();

				this.canvasContext.beginPath();

				var copyOfCoord = _.clone( box.coordinates ),
					moveToCoordPair = copyOfCoord.shift();

				this.canvasContext.moveTo( moveToCoordPair[0], moveToCoordPair[1] );

				_.forEach( copyOfCoord, function( coordPair ) {
					this.canvasContext.lineTo( coordPair[0], coordPair[1] );
				}, this );

				this.canvasContext.fill();

				return this;
			},

			rerender: function() {
				this.currentBox = this.createNewRandomizedBox();

				this.renderIfAllowed();

				return this;
			},

			/**
			 * Animation which will animate one existing box to the new one
			 * @return {this}
			 */
			animate: function() {
				if ( ! this.shouldBeRenderedAtWindowWidth() ) {
					return;
				}

				var futureBox = this.createNewRandomizedBox(),
					oldBox = this.currentBox,
					currentFrameBox = null,
					progress = 0,
					startTimestamp = null;

				var step = function( timestamp )  {
					startTimestamp = startTimestamp || timestamp;
					progress = ( timestamp - startTimestamp ) / this.animationDuration;

					currentFrameBox = this.mixTwoBoxes( oldBox, futureBox, progress );
					this.render( currentFrameBox );

					if ( progress < 1 ) {
						window.requestAnimationFrame( _.bind( step, this ) );
					} else {
						this.currentBox = futureBox;
					}
				};

				window.requestAnimationFrame( _.bind( step, this ) );

				return this;
			},

			/**
			 * It will create the box that is the mix of this.currentBox and this.futureBox,
			 * respecting the ratio - mixing parameter.
			 *
			 * @param  {double} ratio  Ratio of mixing. 0 means only box1, 1 only box2
			 * @return {drunkBox}      New mixed box
			 */
			mixTwoBoxes: function( oldBox, futureBox, ratio ) {
				var box = drunkBox( oldBox );

				box.applyFunctionToCoordinates( function( coord, index, axis ) {
					return coord + ( ratio * ( futureBox.coordinates[ index ][ axis ] - coord ) );
				} );

				return box;
			},
		},
	} );
} );

define( [ 'underscore', 'stampit', 'assets/js/funky-item/module/utils' ], function( _, stampit, utils ) {
	var drunkBox = stampit( {
		props: {
			width:  0,
			height: 0,
			coordinates: [ // x, y
				[0, 0], // top left
				[0, 0], // top right
				[0, 0], // bottom right
				[0, 0]  // bottom left
			],
			drunkness: 8, // the higher the value, the more eccentric the box
		},

		methods: {
			applyFunctionToCoordinates: function( callee ) {
				this.coordinates = _.map( this.coordinates, function( coordPair, index ) {
					return _.map( coordPair, function( coord, axis ) {
						return callee.call( this, coord, index, axis );
					}, this );
				}, this);

				return this;
			},

			randomizeCoordinates: function() {
				this
					.setCoorinatesToBoudingCanvas()
					.applyFunctionToCoordinates( function( coord ) {
						return this.randomizeNumber( coord, this.drunkness );
					} );

				return this;
			},

			setCoorinatesToBoudingCanvas: function() {
				this.applyFunctionToCoordinates( function( coord, index, axis ) {
					if ( 0 === axis ) { // x
						if ( -1 !== [0, 3].indexOf( index ) ) { // left
							return this.drunkness / 2;
						}
						else { // right
							return this.width - ( this.drunkness / 2 );
						}
					} else { // y
						if ( index < 2 ) { // top
							return this.drunkness / 2;
						}
						else { // bottom
							return this.height - ( this.drunkness / 2 );
						}
					}
				} );

				return this;
			},
		},
	} )
		.compose( utils );

	return drunkBox;
} );

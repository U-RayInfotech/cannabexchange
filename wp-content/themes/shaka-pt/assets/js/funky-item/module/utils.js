define( [ 'stampit', 'underscore' ], function( stampit, _ ) {
	return stampit().methods( {
		randomizeNumber: function( number, multiplier ) {
			multiplier = _.isNumber( multiplier ) ? multiplier : 8;

			return number + Math.round( ( Math.random() - 0.5 ) * multiplier );
		},

		isBetween: function( num, min, max ) {
			return num >= min && num <= max;
		},

		inlineCssFromObject: function( obj ) {
			var cssRules = _.map( obj, function( value, prop ) {
				if ( /^\-?\d+$/.test( value ) ) {
					value += 'px';
				}

				return prop + ': ' + value;
			} );

			return cssRules.join( '; ' );
		},
	} );
} );

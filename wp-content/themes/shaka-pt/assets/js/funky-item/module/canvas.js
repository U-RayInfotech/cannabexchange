define( [ 'underscore', 'stampit', 'jquery', 'assets/js/funky-item/module/utils' ], function( _, stampit, $, utils ) {
	return stampit( {
		props: {
			canvasEl:        null,
			canvasContext:   null,
			fillColor:       '#000000',
			canvasHtmlClass: 'funky-item',
		},

		methods: {
			createCanvas: function( width, height ) {
				var canvas = document.createElement( 'canvas' );

				canvas.width  = width;
				canvas.height = height;

				if ( this.canvasHtmlClass ) { // can be disabled
					$( canvas ).addClass( this.canvasHtmlClass );
				}

				this.canvasEl = canvas;

				return this;
			},

			setContext: function( canvas ) {
				canvas = canvas || this.canvasEl;

				this.canvasContext = canvas.getContext( '2d' );

				return this;
			},

			removeCanvas: function() {
				if ( this.canvasEl ) {
					this.canvasEl.remove();
				}

				return this;
			},

			cleanCanvas: function( argument ) {
				this.canvasContext.clearRect( 0, 0, this.canvasEl.width, this.canvasEl.height );

				return this;
			},

			hideCanvas: function() {
				$( this.canvasEl ).hide();

				return this;
			},

			showCanvas: function() {
				$( this.canvasEl ).show();

				return this;
			},

			setCanvasPosition: function() {
				var whitelistedProps = _.pick( this.css, 'position', 'top', 'left', 'bottom' );

				$( this.canvasEl ).css( whitelistedProps );

				return this;
			},

			setFillStyle: function( fillStyle ) {
				fillStyle = fillStyle || this.fillColor;

				this.fillColor = fillStyle;

				if ( this.canvasContext ) {
					this.canvasContext.fillStyle = fillStyle;
				}

				return this;
			},
		}
	} )
		.compose( utils );
} );

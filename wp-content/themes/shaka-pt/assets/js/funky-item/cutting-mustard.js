/*global Modernizr*/
define( [], function() {
	return Modernizr.requestanimationframe && Modernizr.canvas;
} );

define( [ 'stampit', 'assets/js/funky-item/facade/funky-underline', 'assets/js/funky-item/facade/funky-box' ], function( stampit, funkyUnderline, funkyBox ) {
	return function( args ) {
		switch( args.type ) {
			case 'underline':
				return funkyUnderline( args );
			case 'box':
				return funkyBox( args );
		}
	};
} );

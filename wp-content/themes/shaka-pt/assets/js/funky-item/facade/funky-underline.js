define( [ 'jquery', 'stampit', 'assets/js/funky-item/module/canvas', 'assets/js/funky-item/module/box-renderer' ], function( $, stampit, canvas, boxRenderer ) {
	var funkyUnderline = stampit( {
		init: function() {
			this.$el = this.$el || $( this.el );

			this.attachEventListeners();

			if ( this.shouldBeRenderedAtWindowWidth() ) {
				this.setupCanvas();
			}

			return this;
		},

		props: {
			css: {
				position: 'absolute',
				bottom:   0,
				left:     0,
				right:    0,
			},
			dimensions: {
				width:  undefined,
				height: 10,
			},
			responsive: [
				{
					min: 0,
					max: Infinity,
				}
			],
			eventNS: 'funkyItem',
			rerenderOnHover: false,
		},

		methods: {
			getCalculatedWidth: function() {
				return this.getContainerWidth() - ( this.css.left + this.css.right );
			},

			getContainerWidth: function() {
				return this.$el.width();
			},

			setupCanvas: function() {
				this
					.createCanvas( this.getCalculatedWidth(), this.dimensions.height )
					.putCanvasInsideEl()
					.setCanvasPosition()
					.setContext()
					.setFillStyle();

				return this;
			},

			putCanvasInsideEl: function() {
				this.$el.append( this.canvasEl );

				return this;
			},

			shouldBeRenderedAtWindowWidth: function( windowWidth ) {
				windowWidth = windowWidth || $( window ).width();

				return undefined !== _.find( this.responsive, function( widthRange ) {
					return this.isBetween( windowWidth, widthRange.min, widthRange.max );
				}, this );
			},

			attachEventListeners: function() {
				if ( this.rerenderOnHover ) {
					this.$el.on( 'mouseenter.' + this.eventNS + ' focusin.' + this.eventNS, _.bind( this.rerender, this ) );
				}

				$( window ).on( 'resize.' + this.eventNS, _( this.resizeHandler ).debounce( 500 ).bind( this ) );

				return this;
			},

			resizeHandler: function() {
				this.removeCanvas();

				if ( this.shouldBeRenderedAtWindowWidth() ) {
					this
						.setupCanvas()
						.render();
				}
			},
		}
	} )
		.compose( canvas, boxRenderer );

	return funkyUnderline;
} );

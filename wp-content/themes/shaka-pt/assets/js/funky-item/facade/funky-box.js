define( [ 'jquery', 'stampit', 'assets/js/funky-item/module/canvas', 'assets/js/funky-item/module/box-renderer' ], function( $, stampit, canvas, boxRenderer ) {
	var funkyBox = stampit( {
		init: function() {
			this.$el = this.$el || $( this.el );

			// set CSS props
			_.each( [ 'top', 'bottom', 'left', 'right' ], function( prop ) {
				this.css[ prop ] = -this.drunkness;
			}, this );

			this.attachEventListeners();

			this.setHeight();

			// add extra element for text holder
			var text = this.$el.text(),
				$textHolder = $( '<div class="js-' + this.eventNS + '-text-holder" style="position: absolute; z-index: 2; top: 0; left: 0; right: 0; bottom: 0;"></div>' );

			$textHolder.css( {
				paddingLeft:  this.$el.css('padding-left'),
				paddingRight: this.$el.css('padding-right'),
				paddingTop:   this.$el.css('padding-top'),
			} );
			$textHolder.text( text );
			this.$el.append( $textHolder );

			if ( this.shouldBeRenderedAtWindowWidth() ) {
				this.setupCanvas();
			}

			return this;
		},

		props: {
			css: {
				position: 'absolute',
				top:      0,
				bottom:   0,
				left:     0,
				right:    0,
			},
			responsive: [
				{
					min: 0,
					max: Infinity,
				}
			],
			dimensions: {
				height: 30,
			},
			eventNS: 'funkyItem',
		},

		methods: {
			getContainerWidth: function() {
				return this.$el.outerWidth();
			},

			getCalculatedWidth: function() {
				return this.getContainerWidth() - ( this.css.left + this.css.right );
			},

			getCalculatedHeight: function() {
				return this.$el.outerHeight() - ( this.css.top + this.css.bottom );
			},

			setHeight: function() {
				this.dimensions.height = this.getCalculatedHeight();

				return this;
			},

			setupCanvas: function() {
				this
					.createCanvas( this.getCalculatedWidth(), this.getCalculatedHeight() )
					.putCanvasInsideEl()
					.setCanvasPosition()
					.setContext()
					.setFillStyle();

				return this;
			},

			putCanvasInsideEl: function() {
				this.$el.append( this.canvasEl );

				return this;
			},

			shouldBeRenderedAtWindowWidth: function( windowWidth ) {
				windowWidth = windowWidth || $( window ).width();

				return undefined !== _.find( this.responsive, function( widthRange ) {
					return this.isBetween( windowWidth, widthRange.min, widthRange.max );
				}, this );
			},

			attachEventListeners: function() {
				$( window ).on( 'resize.' + this.eventNS, _( this.resizeHandler ).debounce( 500 ).bind( this ) );

				return this;
			},

			resizeHandler: function() {
				this.removeCanvas();

				if ( this.shouldBeRenderedAtWindowWidth() ) {
					this
						.setupCanvas()
						.render();
				}
			},
		}
	} )
		.compose( canvas, boxRenderer );

	return funkyBox;
} );

define( [ 'jquery', 'assets/js/funky-item/funky-item', 'assets/js/funky-item/cutting-mustard' ], function( $, funkyItem, cuttingMustard ) {
	if ( ! cuttingMustard ) { // bail out early if certain features are not supported by the browser
		return;
	}

	// initialize
	[
		'.person-profile__label',
		'.contact-profile__label',
		'.special-offer__label',
		'.latest-news--block .latest-news__date',
		'.latest-news--featured .latest-news__date',
		'.featured-product__price',
		'.masonry .hentry__date',
	].forEach( function(selector) {
		$( selector ).each( function() {
			funkyItem( {
				type:      'box',
				$el:        $( this ),
				drunkness: 10,
				fillColor: $( this ).css( 'backgroundColor' ),
			} );
		} );
	} );

	// shortcodes
	$( '.js-funky-box' ).each( function() {
		funkyItem( {
			type:      'box',
			$el:        $( this ),
			drunkness: 8,
			fillColor: $( this ).css( 'backgroundColor' ),
		} );
	} );

	// portfolio grids are somewhat special
	$( '.portfolio-grid__card-price' ).each( function() {
		( function( $elm ) {
			var item = funkyItem( {
				type:      'box',
				$el:        $elm,
				drunkness: 10,
				fillColor: $elm.css( 'backgroundColor' ),
			} ),
				$bsSlider = item.$el.parents( '.carousel' ),
				$portfolioGridContainer = item.$el.parents( '.portfolio-grid' ),
				fullRerender = function( itemInstance ) {
					itemInstance
						.removeCanvas()
						.setHeight()
						.setupCanvas()
						.rerender();
				};

			// sliding of items
			$bsSlider.on( 'slid.bs.carousel', function() {
				fullRerender( item );
			} );

			// filtering of items
			$portfolioGridContainer.on( 'wpge_on_elements_switch', function() {
				setTimeout( function() {
					fullRerender( item );
				}, 200 );
			} );
		} )( $( this ) );
	} );

	// sidebar menus
	$( '.sidebar .menu > .current-menu-item > a, .sidebar .product-categories .current-cat > a' ).each( function() {
		funkyItem( {
			type:      'box',
			$el:        $( this ),
			drunkness: 7,
			fillColor: $( this ).css( 'backgroundColor' ),
		} );
	} );
} );

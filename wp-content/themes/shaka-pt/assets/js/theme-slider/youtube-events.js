/* global YT */

/**
 * Youtube events for Slick Carousel - Theme Slider
 */

define( [ 'jquery' ], function( $ ) {
	'use strict';

	// Everything has to be global, so that it can be used in the "onYouTubeIframeAPIReady" callback.
	var themeSliderOptions = {},
		themeSlider,
		youtubeVideoClass = '.js-carousel-item-yt-video',
		$personProfileSliders = $( '.js-person-profile-initialize-carousel' );

	var YoutubeEvents = function( themeSliderSlickCarouselInstance ) {
		// Get slider instance and get it's default settings.
		themeSlider                     = themeSliderSlickCarouselInstance;
		themeSliderOptions.pauseOnHover = themeSlider.$slider.slick( 'slickGetOption', 'pauseOnHover');
		themeSliderOptions.autoplay     = themeSlider.$slider.slick( 'slickGetOption', 'autoplay');

		/**
		 * Global function (it must be), which is triggered, when Youtube API loads.
		 * Creates Youtube objects of existing iframes and registers player event listeners.
		 */
		window.onYouTubeIframeAPIReady = function() {
			// Register event listeners only if the theme slider has slides.
			if ( themeSlider.$slider.length ) {
				themeSlider.$slider.find( youtubeVideoClass + ' iframe' ).each( function( index, element ) {
					new YT.Player( $( element ).attr( 'id' ), {
						events: {
							'onStateChange': onPlayerStateChange
						}
					});
				});
			}

			// Register YT players for the Person Profile widget sliders.
			if ( $personProfileSliders.length ) {
				$personProfileSliders.find( youtubeVideoClass + ' iframe' ).each( function( index, element ) {
					new YT.Player( $( element ).attr( 'id' ) );
				});
			}
		};

		// Trigger the YouTube API.
		this.initializeYoutubeIframeApi();

		// Register the slick carousel events.
		this.registerSliderEvents();
	};

	/**
	 * Event function, which is triggered, when Youtube video changes state.
	 */
	function onPlayerStateChange( ev ) {
		if ( YT.PlayerState.PLAYING === ev.data ) {
			onPlay();
		}
		else if ( YT.PlayerState.PAUSED === ev.data ) {
			onPause();
		}
	}

	/**
	 * Helper function for when 'play' event is triggered.
	 */
	function onPlay() {
		// Slick Carousel pauseOnHover option enables autoplay on hover exit, so it has to be disabled.
		themeSlider.$slider.slick( 'slickSetOption', 'pauseOnHover', false, false );

		// Pause the autoplay, when video plays.
		themeSlider.$slider.slick( 'slickPause' );
	}

	/**
	 * Helper function for when 'pause' event is triggered.
	 */
	function onPause() {
		// Set the pauseOnHover option back to the original setting.
		if ( themeSliderOptions.pauseOnHover ) {
			themeSlider.$slider.slick( 'slickSetOption', 'pauseOnHover', true, false );
		}
	}

	_.extend( YoutubeEvents.prototype, {
		/**
		 * Hook into the slider events.
		 * Pause the youtube video, when the slide is changed.
		 */
		registerSliderEvents: function() {
			themeSlider.$slider.on( 'beforeChange', _.bind( function( ev, slick, currentSlide, nextSlide ) {
				 this.onSliderBeforeChange( ev, slick, currentSlide, nextSlide );
			}, this ) );

			$personProfileSliders.on( 'beforeChange', _.bind( function( ev, slick, currentSlide, nextSlide ) {
				 this.onSliderBeforeChange( ev, slick, currentSlide, nextSlide );
			}, this ) );
		},

		/**
		 * Helper function to pause the currentSlide, if it is a Vimeo video -> on slide change event.
		 */
		onSliderBeforeChange: function( ev, slick, currentSlide, nextSlide ) {
			var $currentSlideDiv = $( ev.target ).find( '.slick-slide[data-slick-index="' + currentSlide + '"] ' + youtubeVideoClass);

			if( $currentSlideDiv.length ) {
				YT.get( $currentSlideDiv.children( 'iframe' ).attr( 'id' ) ).pauseVideo();
			}
		},

		/**
		 * Loads the Youtube iframe API code asynchronously and fires the onYouTubeIframeAPIReady function.
		 */
		initializeYoutubeIframeApi: function() {
			var tag = document.createElement( 'script' ),
				firstScriptTag = document.getElementsByTagName( 'script' )[0];

			tag.src = 'https://www.youtube.com/iframe_api';
			firstScriptTag.parentNode.insertBefore( tag, firstScriptTag );
		},
	} );

	return YoutubeEvents;
});
/**
 * Vimeo events for Slick Carousel - Theme Slider
 */

define( [ 'jquery', 'underscore' ], function( $, _ ) {
	'use strict';

	var playerOrigin    = '*',
			vimeoVideoClass = '.js-carousel-item-vimeo-video';

	var VimeoEvents = function( slickCarouselInstance ) {
		this.sc                   = slickCarouselInstance;
		this.personProfileSliders = $( '.js-person-profile-initialize-carousel' );

		this.pauseOnHover = this.sc.$slider.slick( 'slickGetOption', 'pauseOnHover');
		this.autoplay     = this.sc.$slider.slick( 'slickGetOption', 'autoplay');

		// Only register Vimeo listeners, if slider has slides.
		if ( this.sc.$slider.length ) {
			this.registerEventListeners();
		}

		this.registerSliderEvents();

		return this;
	};

	_.extend( VimeoEvents.prototype, {

		/**
		 * Register event listeners for Vimeo videos.
		 */
		registerEventListeners: function() {

			// Listen for messages from the player.
			if ( window.addEventListener ) {
				window.addEventListener( 'message', _.bind( this.onMessageReceived, this ), false );
			}
			else {
				window.attachEvent( 'onmessage', _.bind( this.onMessageReceived, this ), false );
			}

			return this;
		},

		/**
		 * Handle messages received from the player.
		 */
		onMessageReceived: function( ev ) {

			// Handle messages from the vimeo player only.
			if ( ! ( /^https?:\/\/player.vimeo.com/ ).test( ev.origin ) ) {
				return false;
			}

			if ( '*' === playerOrigin ) {
				playerOrigin = ev.origin;
			}

			var data = JSON.parse( ev.data );

			switch ( data.event ) {
				case 'ready':
					this.onReady( data );
					break;

				case 'play':
					this.onPlay();
					break;

				case 'pause':
					this.onPause();
					break;
			}
		},

		/**
		 * Helper function for sending a message to the player.
		 */
		post: function( action, value, data ) {
			var sendData = {
				method: action
			};

			if ( value ) {
				sendData.value = value;
			}

			var message = JSON.stringify( sendData );

			// Send messages to current vimeo player.
			$( '#' + data.player_id )[0].contentWindow.postMessage( message, playerOrigin );
		},

		/**
		 * Helper function for when 'ready' event is triggered.
		 */
		onReady: function( data ) {
			this.post( 'addEventListener', 'play', data );
			this.post( 'addEventListener', 'pause', data );
		},

		/**
		 * Helper function for when 'pause' event is triggered.
		 */
		onPause: function() {

			// Set the pauseOnHover option back to the original setting.
			if ( this.pauseOnHover ) {
				this.sc.$slider.slick( 'slickSetOption', 'pauseOnHover', true, false );
			}
		},

		/**
		 * Helper function for when 'play' event is triggered.
		 */
		onPlay: function() {

			// Slick Carousel pauseOnHover option enables autoplay on hover exit, so it has to be disabled.
			this.sc.$slider.slick( 'slickSetOption', 'pauseOnHover', false, false );

			// Pause the autoplay, when video plays.
			this.sc.$slider.slick( 'slickPause' );
		},

		/**
		 * Hook into the slider events.
		 * Pause the vimeo video, when the slide is changed.
		 */
		registerSliderEvents: function() {
			this.sc.$slider.on( 'beforeChange', _.bind( function( ev, slick, currentSlide, nextSlide ) {
				this.onBeforeChange( ev, slick, currentSlide, nextSlide );
			}, this ) );

			this.personProfileSliders.on( 'beforeChange', _.bind( function( ev, slick, currentSlide, nextSlide ) {
				this.onBeforeChange( ev, slick, currentSlide, nextSlide );
			}, this ) );
		},

		/**
		 * Helper function to pause the currentSlide, if it is a Vimeo video -> on slide change event.
		 */
		onBeforeChange: function( ev, slick, currentSlide, nextSlide ) {
			var $currentSlideDiv = $( ev.target ).find( '.slick-slide[data-slick-index="' + currentSlide + '"] ' + vimeoVideoClass );

			// Pause the video, if the currentSlide was a Vimeo video.
			if( $currentSlideDiv.length ) {
				this.post( 'pause', '', { player_id: $currentSlideDiv.children( 'iframe' ).attr( 'id' ) } );
			}
		}

	} );

	return VimeoEvents;
} );

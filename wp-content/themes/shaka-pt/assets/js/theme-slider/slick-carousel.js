/* global ShakaSliderCaptions */

/**
 * Slick Carousel - Theme Slider
 */

define( [ 'jquery', 'underscore', 'SlickCarousel', 'isElementInView' ], function( $, _, slick, isElementInView ) {
	'use strict';

	var $captions = {
			container: $( '.js-pt-slick-carousel-captions' ),
			title:     $( '.js-pt-slick-carousel-captions__title' ),
			text:      $( '.js-pt-slick-carousel-captions__text' ),
		},
		transitionClass = 'is-in-transition',
		youtubeVideoContainerClass = '.js-carousel-item-yt-video';

	var SlickCarousel = function( $slider ) {
		this.$slider = $slider;
		this.$parentContainer = $slider.parent();

		if ( this.$slider.length && 'object' === typeof ShakaSliderCaptions ) {
			this.changeCaptions();
		}

		if ( this.$slider.length ) {
			this.initializeCarousel();
			this.pauseCarouselIfNotVisible();
		}

		return this;
	};

	_.extend( SlickCarousel.prototype, {

		/**
		 * Change the title and the text for the current (new) slider.
		 * Captions for the theme slider - change them in the out-of-bounds element.
		 */
		changeCaptions: function() {

			this.$slider.on( 'beforeChange', function( ev, slick, currentSlide, nextSlide ) {
				$captions.container.addClass( transitionClass );
				if ( '' === ShakaSliderCaptions[ nextSlide ].title || ShakaSliderCaptions[ nextSlide ].is_video ) {
					if ( Modernizr.mq( '(max-width: 991px)' ) ) {
						$captions.container.slideUp( 300 );
					}
					else {
						$captions.container.hide();
					}
				}

				setTimeout( function() {
					$captions.title.html( ShakaSliderCaptions[ nextSlide ].title );
					$captions.text.html( ShakaSliderCaptions[ nextSlide ].text );
					if ( '' !== ShakaSliderCaptions[ nextSlide ].title && ! ShakaSliderCaptions[ nextSlide ].is_video ) {
						if ( Modernizr.mq( '(max-width: 991px)' ) ) {
							$captions.container.slideDown( 300 );
						}
						else {
							$captions.container.show();
						}
					}
				}, slick.options.speed );
			});

			this.$slider.on( 'afterChange', function() {
				$captions.container.removeClass( transitionClass );
			});

			return this;
		},

		/**
		 * Pause carousel, if it's not visible and only if it's set to autoplay.
		 */
		pauseCarouselIfNotVisible: function() {
			$( document ).on( 'scroll', _.bind( _.throttle( function() {
				if ( this.$slider.slick( 'slickGetOption', 'autoplay' ) ) {
					if ( isElementInView( this.$slider ) ) {

						// 'slickPlay' also sets 'autoplay' option to true!
						// https://github.com/kenwheeler/slick#methods
						this.$slider.slick( 'slickPlay' );
					}
					else {
						this.$slider.slick( 'slickPause' );
					}
				}
			}, 1000, { leading: false } ), this ) );

			return this;
		},

		/**
		 * Initialize the Slick Carousel.
		 */
		initializeCarousel: function() {
			this.$slider.slick();

			// Show the whole slider (parent container is hidden by default).
			this.$parentContainer.css( 'visibility', 'visible' );

			return this;
		}

	} );

	return SlickCarousel;
} );

/* global ShakaVars */

// config
require.config( {
	paths: {
		jquery:          'assets/js/fix.jquery',
		underscore:      'assets/js/fix.underscore',
		util:            'bower_components/bootstrap/dist/js/umd/util',
		alert:           'bower_components/bootstrap/dist/js/umd/alert',
		button:          'bower_components/bootstrap/dist/js/umd/button',
		carousel:        'bower_components/bootstrap/dist/js/umd/carousel',
		collapse:        'bower_components/bootstrap/dist/js/umd/collapse',
		dropdown:        'bower_components/bootstrap/dist/js/umd/dropdown',
		modal:           'bower_components/bootstrap/dist/js/umd/modal',
		scrollspy:       'bower_components/bootstrap/dist/js/umd/scrollspy',
		tab:             'bower_components/bootstrap/dist/js/umd/tab',
		tooltip:         'bower_components/bootstrap/dist/js/umd/tooltip',
		popover:         'bower_components/bootstrap/dist/js/umd/popover',
		stampit:         'assets/js/vendor/stampit',
		SlickCarousel:   'bower_components/slick-carousel/slick/slick',
		isElementInView: 'assets/js/utils/isElementInView',
	}
} );

require.config( {
	baseUrl: ShakaVars.pathToTheme
} );

require( [
		'jquery',
		'underscore',
		'isElementInView',
		'assets/js/utils/objectFitFallback',
		'assets/js/portfolio-grid-filter/gridFilter',
		'assets/js/portfolio-grid-filter/sliderFilter',
		'assets/js/utils/easeInOutQuad',
		'vendor/proteusthemes/proteuswidgets/assets/js/NumberCounter',
		'assets/js/theme-slider/slick-carousel',
		'assets/js/theme-slider/vimeo-events',
		'assets/js/theme-slider/youtube-events',
		'assets/js/instagram-widget',
		'assets/js/weather-widget',
		'vendor/proteusthemes/sticky-menu/assets/js/sticky-menu',
		'assets/js/TouchDropdown',
		'assets/js/FunkyMenus',
		'assets/js/FunkyBoxes',
		'SlickCarousel',
		'carousel',
		'collapse',
		'tab',
], function ( $, _, isElementInView, objectFitFallback, gridFilter, sliderFilter, easeInOutQuad, NumberCounter, ThemeSlider, VimeoEvents, YoutubeEvents, InstagramWidget, WeatherWidget ) {
	'use strict';

	/**
	 * Footer widgets fix
	 */
	$( '.col-lg-__col-num__' ).removeClass( 'col-lg-__col-num__' ).addClass( 'col-lg-3' );

	/**
	 * Number Counter Widget JS code
	 */
	// Get all number counter widgets
	var $counterWidgets = $( '.number-counters' );

	if ( $counterWidgets.length ) {

		// jQuery easing function: easeInOutQuad, for use in NumberCounter
		easeInOutQuad();

		$counterWidgets.each( function () {
			new NumberCounter( $( this ) );
		} );
	}

	/**
	 * Gallery - fallback for old browsers
	 * @return {[type]} [description]
	 */
	(function () {
		if ( ! Modernizr.objectfit ) {

			// gallery
			$('.shaka-gallery__item').each(function () {
				objectFitFallback({
					$container: $(this)
				});
			});
		}
	})();

	/**
	 * Portfolio grid filtering
	 */
	$('.portfolio-grid').each(function () {
		var hash = window.location.hash,
			portfolioGrid;

		if ('slider' === $(this).data('type')) {
			portfolioGrid = sliderFilter({
				$container: $(this),
			});
		}
		else {
			portfolioGrid = gridFilter({
				$container: $(this),
			});
		}

		// Getting on visit or if "All" nav button is disabled.
		if ( new RegExp('^#' + portfolioGrid.hashPrefix).test(hash) ) {
			$(this).find('a[href="' + hash.replace(portfolioGrid.hashPrefix, '') + '"]').trigger('click');
		}
		else if ( $(this).find('.portfolio-grid__nav-item').first().hasClass('is-disabled') ) {
			// Trigger click for the second nav grid item, if the "All" nav button is missing.
			$(this).find('.portfolio-grid__nav-item:nth-child(2)').children('.portfolio-grid__nav-link')
				.data( 'skip-hash-update', true )
				.trigger( 'click' )
				.removeData( 'skip-hash-update' );
		}

		// Recalculate the mobile nav height, if the "All" nav button is disabled. Fix for both cases above.
		if ( ! portfolioGrid.isDesktopLayout() && $(this).find('.portfolio-grid__nav-item').first().hasClass('is-disabled') ) {
			portfolioGrid.initNavHolderHeight();
		}
	});

	/**
	 * Slick carousel for the Person profile widget (from the PW composer package).
	 */
	$( '.js-person-profile-initialize-carousel' ).slick();

	/**
	 * Slick Carousel - Theme Slider
	 */
	(function () {
		var themeSliderInstance = new ThemeSlider( $( '.js-pt-slick-carousel-initialize-slides' ) );
		new VimeoEvents( themeSliderInstance );

		// Conditionally load the YouTube events (YouTube API).
		if ( $( '.js-person-profile-initialize-carousel .js-carousel-item-yt-video' ).length > 0 || $( '.js-pt-slick-carousel-initialize-slides .js-carousel-item-yt-video' ).length > 0 ) {
			new YoutubeEvents( themeSliderInstance );
		}
	})();

	/**
	 * Instagram widget - initialize.
	 */
	$( '.js-pw-instagram' ).each( function () {
		new InstagramWidget( $( this ) );
	} );

	/**
	 * Weather widget - initialize.
	 */
	$( '.js-weather' ).each( function () {
		new WeatherWidget( $( this ) );
	} );

	/**
	 * Masnory JS - initialization.
	 *
	 * Check if jquery-masonry is enqueued and so the $.masonry function is available.
	 */
	if ( $.isFunction( $.fn.masonry ) ) {
		$( window ).on( 'load', function() {
			$( '.js-pt-masonry' ).masonry( {
				itemSelector: '.grid-item'
			} );
		} );
	}

});

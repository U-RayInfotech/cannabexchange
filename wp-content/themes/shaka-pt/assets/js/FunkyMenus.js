define( [ 'jquery', 'assets/js/funky-item/funky-menu', 'assets/js/funky-item/cutting-mustard' ], function( $, funkyMenu, cuttingMustard ) {
	if ( ! cuttingMustard ) { // bail out early if certain features are not supported by the browser
		return;
	}

	// main menu
	funkyMenu( {
		$menuItems:        $( '.js-main-nav > .menu-item' ),
		activeItemSel:     '.current-menu-item',
		activeFillColor:   $( '.js-main-nav > .current-menu-item > a' ).css( 'color' ),
		regularFillColor:  'rgba(0,0,0,0.15)',
		funkyItemDefaults: {
			css: {
				bottom: 38
			},
		},
		optsModifier: function( opts, $el ) {
			if ( $el.is( ':last-child' ) ) {
				opts.css.right = -3;
			}

			return opts;
		},
	} );

	// portfolio menus
	$( '.js-wpg-nav-holder' ).each( function() {
		funkyMenu( {
			$menuItems:        $( this ).find( '.portfolio-grid__nav-item' ),
			activeItemSel:     '.is-active',
			activeFillColor:   '#ffffff',
			regularFillColor:  'rgba(255,255,255,0.5)',
			funkyItemDefaults: {
				css: {
					bottom: 0
				},
			},
			optsModifier: function( opts, $el ) {
				if ( $el.is( ':last-child' ) ) {
					opts.css.right = 0;
				}

				return opts;
			},
			onclick: function( ev ) {
				var oldActive = this.getFunkyItemOfHtmlEl( this.$menuItems.filter( this.activeItemSel ) ),
					newActive = this.getFunkyItemOfHtmlEl( $( ev.currentTarget ).parent() );

				this.swapRenderColors( oldActive, newActive );
			},
		} );
	} );
} );

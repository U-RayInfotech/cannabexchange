/* global ShakaVars */
define( ['jquery', 'underscore'], function ( $, _ ) {
	'use strict';

	var config = {
		currentDayClass: '.js-weather-current',
		forecastClass:   '.js-weather-forecast',
	};

	var template = {
		currentWeather: _.template( '<img class="weather__current-icon" src="<%= pathToTheme %>/assets/images/weather-widget/color-icons/<%= icon %>.svg" alt="<%= icon %>"> <div class="weather__current-temperature-container"><p class="weather__current-temperature"><%= temperature %><sup><%= temperatureUnit %></sup></p> <p class="weather__current-description"><%= weatherInWords %></p></div> <div class="weather__current-wind"><img src="<%= pathToTheme %>/assets/images/weather-widget/color-icons/compass.svg" style="-webkit-transform: rotate( <%= windDirection %>deg ); -ms-transform: rotate( <%= windDirection %>deg ); transform: rotate( <%= windDirection %>deg );"><div class="weather__current-wind-speed-container"><span class="weather__current-wind-speed"><%= wind %></span><span class="weather__current-wind-speed-unit"><%= windUnit %></span></div></div>' ),
		weatherRow: _.template( '<div class="weather__forecast"><span class="weather__forecast-day"><%= dayOfTheWeek %></span> <div class="weather__forecast-temperature"><img class="weather__forecast-icon" src="<%= pathToTheme %>/assets/images/weather-widget/gray-icons/<%= icon %>.svg" alt="<%= icon %>"> <span><%= temperature %><%= temperatureUnit %></span></div> <div class="weather__forecast-wind"><img src="<%= pathToTheme %>/assets/images/weather-widget/gray-icons/compass.svg" style="-webkit-transform: rotate( <%= windDirection %>deg ); -ms-transform: rotate( <%= windDirection %>deg ); transform: rotate( <%= windDirection %>deg );"><span><%= wind %> <%= windUnit %></span></div></div>' ),
		error: _.template( '<div class="weather__error"><%= error %></div>' )
	};

	var WeatherWidget = function ( $widget ) {
		this.$widget = $widget;

		this.latitude               = this.$widget.data( 'latitude' );
		this.longitude              = this.$widget.data( 'longitude' );
		this.temperatureUnit        = this.$widget.data( 'temperature-unit' );
		this.windUnit               = this.$widget.data( 'wind-unit' );
		this.forecastDays           = this.$widget.data( 'forecast' );
		this.currentWeatherEnabled  = this.$widget.data( 'current-weather' ) || '';

		this.getDataFromWeatherAPI();

		return this;
	};

	_.extend( WeatherWidget.prototype, {
		/**
		 * Get weather data for the specified location.
		 */
		getDataFromWeatherAPI: function() {
			$.ajax( {
				method:     'GET',
				url:        ShakaVars.ajax_url,
				data:       {
					'action':    'pt_shaka_get_weather_data',
					'security':  ShakaVars.ajax_nonce,
					'latitude':  this.latitude,
					'longitude': this.longitude,
					'current_weather_enabled': this.currentWeatherEnabled,
				},
				context:    this,
				beforeSend: function() {
					this.$widget.append( '<p class="weather__loader"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="screen-reader-text">Loading...</span></p>' );
				},
				complete:   function() {
					this.$widget.find( '.weather__loader' ).remove();
				}
			} )
			.done( function( response ) {
				if ( response.success ) {
					this.outputData( response );
				}
				else {
					this.outputError( response.data );
				}
			});

			return this;
		},

		/**
		 * Output the retrieved data.
		 *
		 * @param response Response with data from the weather API.
		 */
		outputData: function( response ) {
			var today = _.first( response.data ),
				forecast = _.chain( response.data )
					.rest()
					.first( this.forecastDays )
					.value();

			// Weather today
			this.render( today, true );

			// Loop through the forecast days
			_.each( forecast, function( obj ) {
				this.render( obj );
			}, this );
		},

		/**
		 * Render the appropriate template
		 * @param  {object}  obj
		 * @param  {Boolean} isCurrent
		 * @return {void}
		 */
		render: function ( weatherData, isCurrent ) {
			var templateData =  {
				icon:            weatherData.icon,
				temperature:     this.getCorrectTemperatureValue( weatherData.temperature_max ),
				temperatureUnit: this.getCorrectTemperatureUnit( weatherData.temperature_max ),
				wind:            this.getCorrectWindValue( weatherData.wind_speed ),
				windUnit:        this.getCorrectWindUnit( weatherData.wind_speed ),
				windDirection:   weatherData.wind_direction,
				pathToTheme:     ShakaVars.pathToTheme,
			};

			if ( isCurrent ) { // Append a the current day weather info.
				this.$widget.find( config.currentDayClass ).append(
					template.currentWeather( _.extend( templateData, {
						weatherInWords: weatherData.weather_in_words,
					} ) )
				);
			}
			else { // Append a row of data to the widget. One for each day of forecast.
				this.$widget.find( config.forecastClass ).append(
					template.weatherRow( _.extend( templateData, {
						dayOfTheWeek: weatherData.day_of_the_week_short,
					} ) )
				);
			}
		},

		/**
		 * Output the error message.
		 *
		 * @param response Response with data from the weather API.
		 */
		outputError: function( response ) {
			this.$widget.find( config.forecastClass ).after(
				template.error( {
					error: response,
				} )
			);
		},

		/**
		 * Get correct temperature value.
		 *
		 * @param temperatureObj Temperature data object.
		 */
		getCorrectTemperatureValue: function( temperatureObj ) {
			if ( 'fahrenheit' === this.temperatureUnit ) {
				return temperatureObj.fahrenheit.value;
			}

			return temperatureObj.celsius.value;
		},

		/**
		 * Get correct temperature unit.
		 *
		 * @param temperatureObj Temperature data object.
		 */
		getCorrectTemperatureUnit: function( temperatureObj ) {
			if ( 'fahrenheit' === this.temperatureUnit ) {
				return temperatureObj.fahrenheit.unit;
			}

			return temperatureObj.celsius.unit;
		},

		/**
		 * Get correct wind speed value.
		 *
		 * @param windObj Wind speed data object.
		 */
		getCorrectWindValue: function( windObj ) {
			if ( 'kilometers_per_hour' === this.windUnit ) {
				return windObj.kilometers_per_hour.value;
			}
			else if ( 'knots' === this.windUnit ) {
				return windObj.knots.value;
			}
			else if ( 'miles_per_hour' === this.windUnit ) {
				return windObj.miles_per_hour.value;
			}

			return windObj.meters_per_second.value;
		},

		/**
		 * Get correct wind speed unit.
		 *
		 * @param windObj Wind speed data object.
		 */
		getCorrectWindUnit: function( windObj ) {
			if ( 'kilometers_per_hour' === this.windUnit ) {
				return windObj.kilometers_per_hour.unit;
			}
			else if ( 'knots' === this.windUnit ) {
				return windObj.knots.unit;
			}
			else if ( 'miles_per_hour' === this.windUnit ) {
				return windObj.miles_per_hour.unit;
			}

			return windObj.meters_per_second.unit;
		},
	} );

	return WeatherWidget;
} );

<?php
/**
 * Template Name: Front Page With Layer/Revolution Slider
 *
 * @package shaka-pt
 */

get_header();

// Slider.
$shaka_type = get_field( 'slider_type' );

if ( 'layer' === $shaka_type && function_exists( 'layerslider' ) ) { // Layer slider.
	layerslider( (int) get_field( 'layerslider_id' ) );
}
elseif ( 'revolution' === $shaka_type && function_exists( 'putRevSlider' ) ) { // Revolution slider.
	putRevSlider( get_field( 'revolution_slider_alias' ) );
}

?>

<div id="primary" class="content-area  container" role="main">
	<div class="hentry__content  entry-content">
		<?php
		if ( have_posts() ) {
			while ( have_posts() ) {
				the_post();
				the_content();
			}
		}
		?>
	</div>
</div>

<?php get_footer(); ?>

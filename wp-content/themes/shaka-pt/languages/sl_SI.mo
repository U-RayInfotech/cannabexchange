��            )         �     �     �     �     �  "   �           3     B  	   G     Q  '   e     �  W   �     �  	                  -  H   ;     �     �  F   �     �     �     �  \        b     g  $   o     �  �  �  6   �     �  	   �     �  ,   	     1	     E	     S	  
   Y	     d	  +   s	     �	  2   �	     �	     �	     �	     
     &
  Q   <
     �
     �
  ;   �
     �
     �
     �
  @        G     L  !   Z     |                                                          
      	                                                                        %s Comment %s Comments &larr; Older Comments 404 Picture Comment Navigation Comments for this post are closed. Default WP gallery E-mail Address Edit Error 404 First and Last name Front Page With Layer/Revolution Slider Front Page With Slider It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help. MENU Main Menu New Comment Newer Comments &rarr; Nothing Found Page you are looking for is not here. %s Go %s Home %s or try to search: Pages: Read more %s Ready to publish your first post? <a href="%1$s">Get started here</a>. Search Search Results For Search for: Sorry, but nothing matched your search terms. Please try again with some different keywords. Type Website Your comment is awaiting moderation. nounYour comment Project-Id-Version: Shaka PT
Report-Msgid-Bugs-To: http://support.proteusthemes.com/
POT-Creation-Date: 2016-07-06 07:03:12+00:00
PO-Revision-Date: 2017-03-26 10:13+0200
Last-Translator: 
Language-Team: ProteusThemes <hi at proteusthemes.com>
Language: sl_SI
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.12
X-Poedit-KeywordsList: __;_e;_x:1,2c;_ex:1,2c;_n:1,2;_nx:1,2,4c;_n_noop:1,2;_nx_noop:1,2,3c;esc_attr__;esc_html__;esc_attr_e;esc_html_e;esc_attr_x:1,2c;esc_html_x:1,2c
Plural-Forms: nplurals=4; plural=(n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || n%100==4 ? 2 : 3);
X-Poedit-SourceCharset: UTF-8
X-Poedit-Basepath: ..
X-Textdomain-Support: yes
X-Poedit-SearchPath-0: .
 %s Komentar %s Komentarja %s Komentarji %s Komentarjev &larr; Starejši komentarji 404 Slika Navigacija po komentarjih Komentiranje tega prispevka je onemogočeno. Osnovna WP galerija E-mail naslov Uredi Napaka 404 Ime in priimek Naslovna stran z Layer/Revolution sliderjem Naslovna stran s sliderjem Ne moremo najti, kar iščeš. Poskusi z iskanjem. MENI Glavni meni Nov komentar Novejši komentarji &rarr; Nič ni bilo najdeno. Stran, ki jo iščeš ne obstaja. %s Nadaljuj %s Domov %s ali poskusi z iskanjem: Strani: Preberi več %s Pripravljen na prvo objavo?<a href="%1$s">Začni tukaj</a>. Išči Najdeni rezultati za Išči: Nič ni bilo najdeno. Poskusi z drugačnimi ključnimi besedami. Tip: Spletna stran Tvoj komentar čaka na odobritev. Tvoj komentar 
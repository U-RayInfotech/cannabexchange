��    /      �  C                   0     F  B   R     �     �  4   �     �  "   �               -  	   2     <     P  �   W     �  W   �     @  	   E  
   O     Z     f     |     �  H   �     �     �     �     �  F        J     _     f     y     �     �     �     �     �  \        ^     f  $   s     �     �  �  �     �     �  	     	             1  ;   5     q  5   �     �     �     �  
   �     �     �  �        �  g   �     $     )  
   8     C     W     t     |  `   �     �     �            P         q     �     �  
   �      �     �  (     ,   +  $   X  r   }     �     �  0        7     V                    !   "                                         
             )                     %   '         (   ,   .       /                 *   -      #   &                  $         	                                          +    %s Comment %s Comments &larr; Older Comments 404 Picture Add new subset (greek, cyrillic, devanagari, vietnamese)no-subset Blog Sidebar By Click here to see online documentation of the theme! Comment Navigation Comments for this post are closed. Documentation E-mail Address Edit Error 404 First and Last name Footer Footer area works best with %d widgets. This number can be changed in the Appearance &rarr; Customize &rarr; Theme Options &rarr; Footer. Header It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help. MENU Main Menu Navigation New Comment Newer Comments &rarr; Next Nothing Found Page you are looking for is not here. %s Go %s Home %s or try to search: Pages: Previous ProteusThemes Read more %s Ready to publish your first post? <a href="%1$s">Get started here</a>. Regular Page Sidebar Search Search Results For Search for: Set a box around the widget Shop Sidebar Sidebar for the shop page Sidebar on the blog layout. Sidebar on the regular page. Sorry, but nothing matched your search terms. Please try again with some different keywords. Website Widget Style Your comment is awaiting moderation. https://www.proteusthemes.com/ nounYour comment Project-Id-Version: Shaka PT 0.0.0-tmp
Report-Msgid-Bugs-To: http://support.proteusthemes.com/
POT-Creation-Date: 2016-07-06 07:03:12+00:00
PO-Revision-Date: 2016-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: en
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: grunt-wp-i18n 0.5.4
X-Poedit-KeywordsList: __;_e;_x:1,2c;_ex:1,2c;_n:1,2;_nx:1,2,4c;_n_noop:1,2;_nx_noop:1,2,3c;esc_attr__;esc_html__;esc_attr_e;esc_html_e;esc_attr_x:1,2c;esc_html_x:1,2c;
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-Country: United States
X-Poedit-SourceCharset: UTF-8
X-Poedit-Basepath: ../
X-Poedit-SearchPath-0: .
X-Poedit-Bookmarks: 
X-Textdomain-Support: yes
 %s commentaire %s commentaires &larr; commentaires antérieurs Image 404 latin-ext Barre latérale du blog Par Cliquez ici pour voir la documentation en ligne du thème ! Commentaire navigation Les commentaires pour cette publication sont fermés. Documentation Adresse e-mail  Modifier Erreur 404 Prénom et nom Bas de page La zone de bas de page fonctionne mieux avec %d widgets. Ce nombre peut être modifié dans Apparence &rarr; Personnaliser &rarr; Options de thème &rarr; Bas de page. En-tête Il semble que nous ne puissions trouver ce que vous cherchez. Peut-être que chercher peut être utile. MENU Menu principal Navigation Nouveau commentaire Commentaires récents &rarr; Suivant Rien de trouvé La page que vous recherchez n'est pas ici. %s Allez à l' %s Accueil %s ou essayez de chercher : Pages : Précédent ProteusThemes Lire la suite %s Prêt à publier votre première publication ? <a href="%1$s">Commencez ici</a>. Barre latérale de page normale Chercher Résultats de recherche pour Chercher : Placer une case autour du widget Barre latérale de la boutique Barre latérale pour la page de boutique Barre latérale sur la mise en page du blog. Barre latérale sur la page normale. Désolé, mais rien ne correspond à vos termes de recherche. Veuillez réessayer avec des mots-clés différents. Site Web Style widget Votre commentaire est en attente de modération. https://www.proteusthemes.com/ Votre commentaire 
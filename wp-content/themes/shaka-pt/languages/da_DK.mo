��    /      �  C                   0     F  B   R     �     �  4   �     �  "   �               -  	   2     <     P  �   W     �  W   �     @  	   E  
   O     Z     f     |     �  H   �     �     �     �     �  F        J     _     f     y     �     �     �     �     �  \        ^     f  $   s     �     �  �  �     �     �     �  	             #  6   &     ]  %   r     �     �     �     �     �     �  �   �  	   l  a   v     �  	   �  
   �     �     �            M   ,     z  	   �     �     �  K   �     �               '     3     O      g     �  &   �  S   �     "     *  ,   6     c     �                    !   "                                         
             )                     %   '         (   ,   .       /                 *   -      #   &                  $         	                                          +    %s Comment %s Comments &larr; Older Comments 404 Picture Add new subset (greek, cyrillic, devanagari, vietnamese)no-subset Blog Sidebar By Click here to see online documentation of the theme! Comment Navigation Comments for this post are closed. Documentation E-mail Address Edit Error 404 First and Last name Footer Footer area works best with %d widgets. This number can be changed in the Appearance &rarr; Customize &rarr; Theme Options &rarr; Footer. Header It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help. MENU Main Menu Navigation New Comment Newer Comments &rarr; Next Nothing Found Page you are looking for is not here. %s Go %s Home %s or try to search: Pages: Previous ProteusThemes Read more %s Ready to publish your first post? <a href="%1$s">Get started here</a>. Regular Page Sidebar Search Search Results For Search for: Set a box around the widget Shop Sidebar Sidebar for the shop page Sidebar on the blog layout. Sidebar on the regular page. Sorry, but nothing matched your search terms. Please try again with some different keywords. Website Widget Style Your comment is awaiting moderation. https://www.proteusthemes.com/ nounYour comment Project-Id-Version: Shaka PT 0.0.0-tmp
Report-Msgid-Bugs-To: http://support.proteusthemes.com/
POT-Creation-Date: 2016-07-06 07:03:12+00:00
PO-Revision-Date: 2016-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: en
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: grunt-wp-i18n 0.5.4
X-Poedit-KeywordsList: __;_e;_x:1,2c;_ex:1,2c;_n:1,2;_nx:1,2,4c;_n_noop:1,2;_nx_noop:1,2,3c;esc_attr__;esc_html__;esc_attr_e;esc_html_e;esc_attr_x:1,2c;esc_html_x:1,2c;
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-Country: United States
X-Poedit-SourceCharset: UTF-8
X-Poedit-Basepath: ../
X-Poedit-SearchPath-0: .
X-Poedit-Bookmarks: 
X-Textdomain-Support: yes
 %s kommentar %s kommentarer &larr; Ældre kommentarer 404 Billede no-subset Blog sidebjælke Af Klik her for at se online dokumentation for temaet!    Kommentar navigation Kommentarer til denne post er lukket. Dokumentation E-mail adresse Rediger Fejl 404 For- og efternavn Sidefod Sidefodsområder fungerer bedst med  %d widgets. Dette tal kan ændres Tilsynekomst &rarr; Tilpas &rarr; Tema-valgmuligheder &rarr; Sidefod Sidehoved Det lader til at vi kan&rsquo;t finde, hvad du&rsquo;kigger efter. Måske kan søgning hjælpe.   MENU Hovedmenu Navigation Ny kommentar Nyere kommentarer &rarr; Næste Intet fundet Siden, du leder efter, er ikke her. %s Gå %s Hjem %s eller forsøg at søge: Sider: Tidligere ProteusThemes Læs mere %s Parat til at publicere din første post? <a href="%1$s">Kom i gang her</a>. Almindelig side-sidebjælke Søg Søgeresultater for Søg efter: Sæt en boks omkring widget Forretnings-sidebjælke Sidebjælke til forretningssiden Sidebjælke på blog-layoutet. Sidebjælke på den almindelige side.  Intet matchede desvære dit søgebegrev. Forsøg igen med nogle andre nøgleord.    Webside Widget-stil Din kommentar venter på at blive modereret. https://www.proteusthemes.com/ Din kommentar 
<?php
/**
 * Footer
 *
 * @package shaka-pt
 */

$shaka_footer_widgets_layout = ShakaHelpers::footer_widgets_layout_array();
$shaka_footer_allowed_html = array(
	'a'      => array(
		'class'   => array(),
		'href'   => array(),
		'target' => array(),
		'title'  => array(),
	),
	'em'     => array(),
	'strong' => array(),
	'img'    => array(
		'src'    => array(),
		'alt'    => array(),
		'width'  => array(),
		'height' => array(),
	),
	'span'   => array(
		'class'  => array(),
		'style'  => array(),
	),
	'i'      => array(
		'class'  => array(),
	),
);

?>

	<footer class="footer">
		<!-- Footer Top -->
		<?php if ( ! empty( $shaka_footer_widgets_layout ) && is_active_sidebar( 'footer-widgets' ) ) : ?>
		<div class="footer-top">
			<div class="container">
				<div class="row">
					<?php
					if ( is_active_sidebar( 'footer-widgets' ) ) {
						dynamic_sidebar( 'footer-widgets' );
					}
					?>
				</div>
			</div>
		</div>
		<?php endif; ?>
		<!-- Footer Bottom -->
		<div class="footer-bottom">
			<div class="container">
				<div class="row">
					<div class="col-xs-12  col-lg-4">
						<div class="footer-bottom__left">
							<?php echo wp_kses( do_shortcode( apply_filters( 'shaka_footer_bottom_left_txt', get_theme_mod( 'footer_bottom_left_txt', '<a href="https://www.proteusthemes.com/wordpress-themes/shaka/">Shaka Theme</a> - Made by ProteusThemes.' ) ) ), $shaka_footer_allowed_html ); ?>
						</div>
					</div>
					<div class="col-xs-12  col-lg-4">
						<div class="footer-bottom__center">
							<?php echo wp_kses( do_shortcode( apply_filters( 'shaka_footer_bottom_center_txt', get_theme_mod( 'footer_bottom_center_txt', '[fa icon="fa-facebook" href="https://www.facebook.com/ProteusThemes/"] [fa icon="fa-youtube" href="https://www.youtube.com/user/ProteusNetCompany"] [fa icon="fa-twitter" href="https://twitter.com/ProteusThemes"]' ) ) ), $shaka_footer_allowed_html ); ?>
						</div>
					</div>
					<div class="col-xs-12  col-lg-4">
						<div class="footer-bottom__right">
							<?php echo wp_kses( do_shortcode( apply_filters( 'shaka_footer_bottom_right_txt', get_theme_mod( 'footer_bottom_right_txt', '&copy; ' . date( 'Y' ) . ' Shaka. All rights reserved.' ) ) ), $shaka_footer_allowed_html ); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>
	</div><!-- end of .boxed-container -->

	<?php wp_footer(); ?>
	</body>
</html>

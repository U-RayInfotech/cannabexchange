<?php
/**
 * The template for displaying all single posts.
 *
 * @package shaka-pt
 */

get_header();

$shaka_sidebar = get_field( 'sidebar', (int) get_option( 'page_for_posts' ) );

if ( ! $shaka_sidebar ) {
	$shaka_sidebar = 'left';
}

get_template_part( 'template-parts/page-header' );

?>

	<div id="primary" class="content-area  container">
		<div class="row">
			<main id="main" class="site-main  col-xs-12<?php echo 'left' === $shaka_sidebar ? '  col-lg-9  col-lg-push-3' : ''; ?><?php echo 'right' === $shaka_sidebar ? '  col-lg-9' : ''; ?>" role="main">

				<?php while ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'template-parts/content', 'single' ); ?>

					<?php
					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) {
						comments_template();
					}
					?>

				<?php endwhile; // End of the loop. ?>

			</main><!-- #main -->

			<?php get_template_part( 'template-parts/sidebar', 'blog' ); ?>

		</div>
	</div><!-- #primary -->

<?php get_footer(); ?>

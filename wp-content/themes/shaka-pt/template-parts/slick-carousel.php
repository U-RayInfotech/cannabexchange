<?php
/**
 * Template part for Slick carousel
 *
 * @package shaka-pt
 */

$slider_content   = get_field( 'slider_content' );
$cycle_interval   = (int) get_field( 'cycle_interval' );
$transition_speed = (int) get_field( 'transition_speed' );

$slick_data = apply_filters( 'pt/slick_carousel_data', array(
	'autoplay'       => get_field( 'auto_cycle' ),
	'autoplaySpeed'  => empty( $cycle_interval ) ? 5000 : $cycle_interval,
	'fade'           => 'fade' === get_field( 'slide_effects' ),
	'dots'           => get_field( 'navigation_dots' ),
	'arrows'         => get_field( 'navigation_arrows' ),
	'adaptiveHeight' => get_field( 'adaptive_height' ),
	'speed'          => empty( $transition_speed ) ? 300 : $transition_speed,
	'prevArrow'      => '<button type="button" class="slick-prev  slick-arrow"><span class="screen-reader-text">' . esc_html__( 'Previous', 'shaka-pt' ) . '</span><i class="fa fa-chevron-left" aria-hidden="true"></i></button>',
	'nextArrow'      => '<button type="button" class="slick-next  slick-arrow"><span class="screen-reader-text">' . esc_html__( 'Next', 'shaka-pt' ) . '</span><i class="fa fa-chevron-right" aria-hidden="true"></i></button>',
));

?>

<div class="pt-slick-carousel">
	<div class="pt-slick-carousel__slides  js-pt-slick-carousel-initialize-slides  pt-slick-carousel__slides--<?php echo 'caption' === $slider_content ? 'with-captions' : 'no-captions'; ?>" data-slick='<?php echo wp_json_encode( $slick_data ); ?>'>
		<?php
		$slider_captions = array();
		$slider_video_id = 1;

		while ( have_rows( 'slides' ) ) :
			the_row();

			$slider_sizes = array( 'shaka-jumbotron-slider-l', 'shaka-jumbotron-slider-m', 'shaka-jumbotron-slider-s' );

			$image_or_video     = get_sub_field( 'image_or_video' );
			$slide_image        = get_sub_field( 'slide_image' );
			$slide_link         = get_sub_field( 'slide_link' );
			$slide_video_url    = get_sub_field( 'video_url' );
			$slider_src_img     = wp_get_attachment_image_src( absint( $slide_image ), 'shaka-jumbotron-slider-s' );
			$slide_image_srcset = ShakaHelpers::get_image_srcset( $slide_image, $slider_sizes );

			if ( 'caption' === $slider_content ) {
				$slider_captions[] = array(
					'title'    => get_sub_field( 'slide_title' ),
					'text'     => get_sub_field( 'slide_text' ),
					'is_video' => 'video' === $image_or_video,
				);
			}
		?>

			<div class="carousel-item">
				<?php if ( 'image' === $image_or_video && ! empty( $slide_image ) ) : ?>

					<?php if ( 'link' === $slider_content && ! empty( $slide_link ) ) : ?>
						<a href="<?php echo esc_url( $slide_link ); ?>" target="<?php echo ( get_sub_field( 'slide_open_link_in_new_window' ) ) ?  '_blank' : '_self' ?>">
					<?php endif; ?>

						<img src="<?php echo esc_url( $slider_src_img[0] ); ?>" srcset="<?php echo esc_attr( $slide_image_srcset ); ?>" sizes="100vw" alt="<?php echo esc_attr( get_sub_field( 'slide_title' ) ); ?>">

					<?php if ( 'link' === $slider_content && ! empty( $slide_link ) ) : ?>
						</a>
					<?php endif; ?>

				<?php	elseif ( 'video' === $image_or_video && ! empty( $slide_video_url ) ) : ?>
					<?php
						$video_class = '';
						if ( strstr( $slide_video_url, 'youtube.com/' ) ) {
							$video_class = '  js-carousel-item-yt-video';
						}
						elseif ( strstr( $slide_video_url, 'vimeo.com/' ) ) {
							$video_class = '  js-carousel-item-vimeo-video';
						}
					?>
					<div class="carousel-item__video<?php echo esc_attr( $video_class ); ?>">
						<?php
							echo wp_oembed_get(
								esc_url( $slide_video_url ),
								array(
									'api' => '1',
									'player_id' => 'pt-sc-video-' . absint( $slider_video_id ),
								)
							);
							$slider_video_id++;
						?>
					</div>
				<?php endif; ?>
			</div>

		<?php endwhile; ?>
	</div>

	<!-- Slider Content - is populated by JS -->
	<?php if ( ! empty( $slider_captions ) ) : ?>
	<div class="container">
		<div class="pt-slick-carousel__content  js-pt-slick-carousel-captions" style="display: <?php echo ( empty( $slider_captions[0]['title'] ) || $slider_captions[0]['is_video'] ) ? 'none' : 'block'; ?>;">
			<p class="pt-slick-carousel__content-title  js-pt-slick-carousel-captions__title"><?php echo wp_kses_post( $slider_captions[0]['title'] ); ?></p>
			<div class="pt-slick-carousel__content-description  js-pt-slick-carousel-captions__text">
				<?php echo wp_kses_post( $slider_captions[0]['text'] ); ?>
			</div>
		</div>
	</div>
	<script>window.ShakaSliderCaptions = <?php echo wp_json_encode( $slider_captions ); ?>;</script>
	<?php endif; ?>

</div>

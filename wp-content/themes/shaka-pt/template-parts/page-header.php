<?php
/**
 * The page title part of the header
 *
 * @package shaka-pt
 */

$shaka_style_attr = '';

// Regular page id.
$shaka_bg_id        = get_the_ID();
$shaka_blog_id      = absint( get_option( 'page_for_posts' ) );
$shaka_shop_id      = absint( get_option( 'woocommerce_shop_page_id', 0 ) );
$shaka_portfolio_id = absint( get_theme_mod( 'portfolio_parent_page', 0 ) );

// If blog or single post use the ID of the blog.
if ( is_home() || is_singular( 'post' ) ) {
	$shaka_bg_id = $shaka_blog_id;
}

// If woocommerce page, use the shop page id.
if ( ShakaHelpers::is_woocommerce_active() && is_woocommerce() ) {
	$shaka_bg_id = $shaka_shop_id;
}

// If Portfolio widget is activate use the parent page for single portfolio pages.
if ( ShakaHelpers::is_portfolio_plugin_active() && is_singular( 'portfolio' ) ) {
	$shaka_bg_id = $shaka_portfolio_id;
}

$show_title_area = get_field( 'show_title_area', $shaka_bg_id );
if ( ! $show_title_area ) {
	$show_title_area = 'yes';
}

$show_breadcrumbs = get_field( 'show_breadcrumbs', $shaka_bg_id );
if ( ! $show_breadcrumbs ) {
	$show_breadcrumbs = 'yes';
}

// Show/hide page title area (ACF control - single page && customizer control - all pages).
if ( 'yes' === $show_title_area && 'yes' === get_theme_mod( 'show_page_title_area', 'yes' ) ) :

	$shaka_style_attr = ShakaHelpers::page_header_background_style( $shaka_bg_id );

	?>

	<div class="page-header<?php echo ( ! is_active_sidebar( 'header-left-widgets' ) && ! is_active_sidebar( 'header-right-widgets' ) ) ? '  page-header--no-widgets' : ''; ?>"<?php echo empty( $shaka_style_attr ) ? '' : ' style="' . esc_attr( $shaka_style_attr ) . ';"'; ?>>
		<div class="container">
			<div class="page-header__text">
				<?php
				$shaka_main_tag = 'h1';
				$shaka_subtitle = false;

				if ( is_home() || ( is_single() && 'post' === get_post_type() ) ) {
					$shaka_title    = 0 === $shaka_blog_id ? esc_html__( 'Blog', 'shaka-pt' ) : get_the_title( $shaka_blog_id );
					$shaka_subtitle = get_field( 'subtitle', $shaka_blog_id );

					if ( is_single() ) {
						$shaka_main_tag = 'h2';
					}
				}
				elseif ( ShakaHelpers::is_woocommerce_active() && is_woocommerce() ) {
					ob_start();
					woocommerce_page_title();
					$shaka_title    = ob_get_clean();
					$shaka_subtitle = get_field( 'subtitle', $shaka_shop_id );

					if ( is_product() ) {
						$shaka_main_tag = 'h2';
					}
				}
				elseif ( ShakaHelpers::is_portfolio_plugin_active() && is_singular( 'portfolio' ) ) {
					$shaka_title    = get_the_title( $shaka_portfolio_id );
					$shaka_subtitle = get_field( 'subtitle', $shaka_portfolio_id );
				}
				elseif ( is_category() || is_tag() || is_author() || is_post_type_archive() || is_tax() || is_day() || is_month() || is_year() ) {
					$shaka_title = get_the_archive_title();
				}
				elseif ( is_search() ) {
					$shaka_title = esc_html__( 'Search Results For' , 'shaka-pt' ) . ' &quot;' . get_search_query() . '&quot;';
				}
				elseif ( is_404() ) {
					$shaka_title = esc_html__( 'Error 404' , 'shaka-pt' );
				}
				else {
					$shaka_title    = get_the_title();
					$shaka_subtitle = get_field( 'subtitle' );
				}

				?>

				<?php printf( '<%1$s class="page-header__title">%2$s</%1$s>', tag_escape( $shaka_main_tag ), esc_html( $shaka_title ) ); ?>

				<?php if ( $shaka_subtitle ) : ?>
					<p class="page-header__subtitle"><?php echo esc_html( $shaka_subtitle ); ?></p>
				<?php endif; ?>
			</div>

			<?php
			if ( 'yes' === $show_breadcrumbs ) {
				get_template_part( 'template-parts/breadcrumbs' );
			}
			?>

		</div>
	</div>
<?php endif; ?>

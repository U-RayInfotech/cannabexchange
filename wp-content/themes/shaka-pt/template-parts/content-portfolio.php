<?php
/**
 * Template part for displaying portfolio posts on archive page.
 *
 * @package shaka-pt
 */

$blog_columns = get_theme_mod( 'blog_columns', 6 );

?>

<article id="post-<?php the_ID(); ?>" <?php post_class( array( 'grid-item', 'col-xs-12', 'col-sm-6', esc_attr( sprintf( 'col-lg-%s', $blog_columns ) ) ) ); ?>>
	<!-- Featured Image -->
	<?php if ( has_post_thumbnail() ) : ?>
		<header class="hentry__header">
			<a class="hentry__featured-image" href="<?php the_permalink(); ?>">
				<?php the_post_thumbnail( 'post-thumbnail', array( 'class' => 'img-fluid' ) ); ?>
			</a>
		</header><!-- .hentry__header -->
	<?php endif; ?>

	<!-- Content Box -->
	<div class="hentry__content  entry-content">
		<!-- Date -->
		<a href="<?php the_permalink(); ?>"><time class="hentry__date" datetime="<?php the_time( 'c' ); ?>"><?php echo get_the_date(); ?></time></a>
		<!-- Author -->
		<span class="hentry__author"><i class="fa  fa-user"></i> <?php echo esc_html__( 'By' , 'shaka-pt' ) . ' ' . get_the_author(); ?></span>
		<!-- Content -->
		<?php the_title( sprintf( '<h2 class="hentry__title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
		<p>
			<?php echo wp_kses_post( get_the_excerpt() ); ?>
		</p>
		<p>
			<a href="<?php echo esc_url( get_permalink() ); ?>" class="more-link"><?php printf( esc_html__( 'Read more %s', 'shaka-pt' ), the_title( '<span class="screen-reader-text">', '</span>', false ) ); ?></a>
		</p>
	</div><!-- .hentry__content -->
</article><!-- .hentry -->

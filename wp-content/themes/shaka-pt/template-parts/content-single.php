<?php
/**
 * Template part for displaying single posts.
 *
 * @package shaka-pt
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php if ( has_post_thumbnail() ) : ?>
		<header class="hentry__header">
			<?php the_post_thumbnail( 'post-thumbnail', array( 'class' => 'img-fluid  hentry__featured-image' ) ); ?>
		</header><!-- .entry-header -->
	<?php endif; ?>

	<div class="hentry__content  entry-content">
		<!-- Date -->
		<a href="<?php the_permalink(); ?>"><time class="hentry__date" datetime="<?php the_time( 'c' ); ?>"><?php echo get_the_date(); ?></time></a>
		<!-- Title -->
		<?php the_title( '<h1 class="hentry__title">', '</h1>' ); ?>

		<?php the_content(); ?>

		<!-- Multi Page in One Post -->
		<?php
			$shaka_args = array(
				'before'      => '<div class="multi-page  clearfix">' . /* translators: after that comes pagination like 1, 2, 3 ... 10 */ esc_html__( 'Pages:', 'shaka-pt' ) . ' &nbsp; ',
				'after'       => '</div>',
				'link_before' => '<span class="btn  btn-primary">',
				'link_after'  => '</span>',
				'echo'        => 1,
			);
			wp_link_pages( $shaka_args );
		?>

		<div class="hentry__meta  meta  clearfix">
			<!-- Author -->
			<span class="meta__item  meta__item--author"><i class="fa  fa-user"></i> <?php echo esc_html__( 'By' , 'shaka-pt' ) . ' ' . get_the_author(); ?></span>
			<!-- Categories -->
			<?php if ( has_category() ) : ?>
				<span class="meta__item  meta__item--categories"><?php the_category( ' ' ); ?></span>
			<?php endif; ?>
			<!-- Tags -->
			<?php if ( has_tag() ) : ?>
				<span class="meta__item  meta__item--tags"><?php the_tags( '', '' ); ?></span>
			<?php endif; ?>
			<!-- Comments -->
			<?php if ( comments_open( get_the_ID() ) ) : // Only show comments count if the comments are open. ?>
				<span class="meta__item  meta__item--comments"><a href="<?php comments_link(); ?>"><?php ShakaHelpers::pretty_comments_number(); ?></a></span>
			<?php endif; ?>
		</div><!-- .hentry__meta -->
	</div><!-- .entry-content -->
</article><!-- #post-## -->

<?php
/**
 * Template part for displaying page sidebar
 *
 * @package shaka-pt
 */

$shaka_sidebar = get_field( 'sidebar' );

if ( ! $shaka_sidebar ) {
	$shaka_sidebar = 'left';
}

if ( 'none' !== $shaka_sidebar && is_active_sidebar( 'regular-page-sidebar' ) ) : ?>
	<div class="col-xs-12  col-lg-3<?php echo 'left' === $shaka_sidebar ? '  col-lg-pull-9' : ''; ?>">
		<div class="sidebar" role="complementary">
			<?php dynamic_sidebar( apply_filters( 'shaka_regular_page_sidebar', 'regular-page-sidebar', get_the_ID() ) ); ?>
		</div>
	</div>
<?php endif; ?>

<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package shaka-pt
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="hentry__content  entry-content">
		<?php the_content(); ?>
		<!-- Multi Page in One Post -->
		<?php
			$shaka_args = array(
				'before'      => '<div class="multi-page  clearfix">' . /* translators: after that comes pagination like 1, 2, 3 ... 10 */ esc_html__( 'Pages:', 'shaka-pt' ) . ' &nbsp; ',
				'after'       => '</div>',
				'link_before' => '<span class="btn  btn-primary">',
				'link_after'  => '</span>',
				'echo'        => 1,
			);
			wp_link_pages( $shaka_args );
		?>
	</div><!-- .entry-content -->
</article><!-- #post-## -->

<?php
/**
 * The Header for Shaka Theme
 *
 * @package shaka-pt
 */

?>

<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
		<?php endif; ?>

		<?php wp_head(); ?>
	</head>

	<body <?php body_class(); ?>>
	<div class="boxed-container">

	<header class="site-header"<?php echo empty( $shaka_style_attr ) ? '' : ' style="' . esc_attr( $shaka_style_attr ) . ';"'; ?>>

	<?php
	// Hide top bar, if both top sidebars are not in use.
	if ( is_active_sidebar( 'top-left-widgets' ) || is_active_sidebar( 'top-right-widgets' ) ) {
		get_template_part( 'template-parts/top-bar' );
	}
	?>

		<div class="header__container">
			<div class="container">
				<div class="header">
					<!-- Logo and site name -->
					<div class="header__logo">
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
							<?php
							$shaka_logo   = get_theme_mod( 'logo_img', false );
							$shaka_logo2x = get_theme_mod( 'logo2x_img', false );

							if ( ! empty( $shaka_logo ) ) :

							?>
								<img src="<?php echo esc_url( $shaka_logo ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>" srcset="<?php echo esc_attr( $shaka_logo ); ?><?php echo empty( $shaka_logo2x ) ? '' : ', ' . esc_url( $shaka_logo2x ) . ' 2x'; ?>" class="img-fluid" <?php echo ShakaHelpers::get_logo_dimensions(); ?> />
							<?php
							else :
							?>
								<h1><?php bloginfo( 'name' ); ?></h1>
							<?php
							endif;
							?>
						</a>
					</div>
					<!-- Toggle button for Main Navigation on mobile -->
					<button class="btn  btn-primary  header__navbar-toggler  hidden-lg-up  js-sticky-mobile-option" type="button" data-toggle="collapse" data-target="#shaka-main-navigation"><i class="fa  fa-bars  hamburger"></i> <span><?php esc_html_e( 'MENU' , 'shaka-pt' ); ?></span></button>
					<!-- Main Navigation -->
					<nav class="header__navigation  collapse  navbar-toggleable-md  js-sticky-desktop-option" id="shaka-main-navigation" aria-label="<?php esc_html_e( 'Main Menu', 'shaka-pt' ); ?>">
						<?php
						if ( has_nav_menu( 'main-menu' ) ) {
							wp_nav_menu( array(
								'theme_location' => 'main-menu',
								'container'      => false,
								'menu_class'     => 'main-navigation  js-main-nav  js-dropdown',
								'walker'         => new Aria_Walker_Nav_Menu(),
								'items_wrap'     => '<ul id="%1$s" class="%2$s" role="menubar">%3$s</ul>',
							) );
						}
						?>
						<?php
						// Display the featured page button if the page is selected in customizer.
						$selected_page = get_theme_mod( 'featured_page_select', 'none' );
						if ( 'none' !== $selected_page ) :
							$target = get_theme_mod( 'featured_page_open_in_new_window', '' ) ? '_blank' : '_self';
							$title  = '';
							$link   = '';

							if ( 'custom-url' === $selected_page ) {
								$title = get_theme_mod( 'featured_page_custom_text', 'Featured Page' );
								$link  = get_theme_mod( 'featured_page_custom_url', '#' );
							}
							else {
								$selected_page = ShakaHelpers::get_page_id( $selected_page );

								$title = get_the_title( absint( $selected_page ) );
								$link  = get_permalink( $selected_page );
							}
						?>
							<a href="<?php echo esc_url( $link ) ?>" class="btn  btn-primary-outline  main-navigation__featured-link" target="<?php echo esc_attr( $target ); ?>"><?php echo esc_html( $title ); ?></a>
						<?php endif; ?>
					</nav>
					<!-- Header left widget area -->
					<?php if ( is_active_sidebar( 'header-left-widgets' ) ) : ?>
						<div class="header__left-widgets">
							<?php dynamic_sidebar( apply_filters( 'shaka_header_left_widgets', 'header-left-widgets', get_the_ID() ) ); ?>
						</div>
					<?php endif; ?>
					<!-- Header right widget area -->
					<?php if ( is_active_sidebar( 'header-right-widgets' ) ) : ?>
						<div class="header__right-widgets">
							<?php dynamic_sidebar( apply_filters( 'shaka_header_right_widgets', 'header-right-widgets', get_the_ID() ) ); ?>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>

	</header>

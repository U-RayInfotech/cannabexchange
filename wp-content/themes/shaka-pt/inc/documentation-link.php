<?php
/**
 * Add the link to documentation under Appearance in the wp-admin
 *
 * @package shaka-pt
 */

if ( ! function_exists( 'shaka_add_docs_page' ) ) {

	/**
	 * Creates the Documentation page under the Appearance menu in wp-admin.
	 */
	function shaka_add_docs_page() {
		add_theme_page(
			esc_html__( 'Documentation', 'shaka-pt' ),
			esc_html__( 'Documentation', 'shaka-pt' ),
			'',
			'proteusthemes-theme-docs',
			'shaka_docs_page_output'
		);
	}
	add_action( 'admin_menu', 'shaka_add_docs_page' );

	/**
	 * This is the callback function for the Documentation page above.
	 * This is the output of the Documentation page.
	 */
	function shaka_docs_page_output() {
		?>
		<div class="wrap">
			<h2><?php esc_html_e( 'Documentation', 'shaka-pt' ); ?></h2>

			<p>
				<strong><a href="https://www.proteusthemes.com/docs/shaka-pt/" class="button button-primary " target="_blank"><?php esc_html_e( 'Click here to see online documentation of the theme!', 'shaka-pt' ); ?></a></strong>
			</p>
		</div>
		<?php
	}
}

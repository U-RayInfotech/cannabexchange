<?php
/**
 * Weather Widget
 *
 * @package shaka-pt
 */

if ( ! class_exists( 'PW_Weather' ) ) {
	class PW_Weather extends WP_Widget {

		// Basic widget settings.
		function widget_id_base() { return 'weather'; }
		function widget_name() { return esc_html__( 'Weather', 'shaka-pt' ); }
		function widget_description() { return esc_html__( 'Weather widget for a given location.', 'shaka-pt' ); }
		function widget_class() { return 'widget-weather'; }

		public function __construct() {
			parent::__construct(
				'pw_' . $this->widget_id_base(),
				sprintf( 'ProteusThemes: %s', $this->widget_name() ),
				array(
					'description' => $this->widget_description(),
					'classname'   => $this->widget_class(),
				)
			);
		}

		/**
		 * Front-end display of widget.
		 *
		 * @see WP_Widget::widget()
		 *
		 * @param array $args Widget arguments.
		 * @param array $instance Widget data.
		 */
		public function widget( $args, $instance ) {
			$api_key          = ! empty( $instance['api_key'] ) ? $instance['api_key'] : '';
			$location         = ! empty( $instance['location'] ) ? $instance['location'] : '';
			$latitude         = ! empty( $instance['latitude'] ) ? $instance['latitude'] : '';
			$longitude        = ! empty( $instance['longitude'] ) ? $instance['longitude'] : '';
			$temperature_unit = ! empty( $instance['temperature_unit'] ) ? $instance['temperature_unit'] : 'celsius';
			$wind_unit        = ! empty( $instance['wind_unit'] ) ? $instance['wind_unit'] : 'meters_per_second';
			$forecast         = ! empty( $instance['forecast'] ) ? $instance['forecast'] : '3';
			$current_weather  = ! empty( $instance['current_weather'] ) ? $instance['current_weather'] : '';
			$link_text        = ! empty( $instance['link_text'] ) ? $instance['link_text'] : 'More details';
			$link_url         = ! empty( $instance['link_url'] ) ? $instance['link_url'] : '';

			echo $args['before_widget'];
			?>

			<div
				class="weather  js-weather"
				data-latitude="<?php echo esc_attr( $latitude ); ?>"
				data-longitude="<?php echo esc_attr( $longitude ); ?>"
				data-temperature-unit="<?php echo esc_attr( $temperature_unit ); ?>"
				data-wind-unit="<?php echo esc_attr( $wind_unit ); ?>"
				data-forecast="<?php echo esc_attr( $forecast ); ?>"
				data-current-weather="<?php echo esc_attr( $current_weather ); ?>"
			>
				<div class="weather__location">
					<?php echo esc_html( $location ); ?>
					<?php if ( ! empty( $current_weather ) ) : ?>
						<span class="weather__current-text">
							<?php esc_html_e( 'Current', 'shaka-pt' ); ?>
						</span>
					<?php endif; ?>
				</div>
				<div class="weather__current  js-weather-current"></div>
				<div class="weather__forecast-container  js-weather-forecast"></div>
				<?php if ( ! empty( $link_url ) ) : ?>
					<a class="weather__custom-link" href="<?php echo esc_url( $link_url ); ?>" target="_blank"><?php echo esc_html( $link_text ); ?><i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
				<?php endif; ?>
				<a class="weather__powered-by-link" href="https://darksky.net/poweredby/" target="_blank" rel="nofollow"><?php esc_html_e( 'Powered by Dark Sky', 'shaka-pt' ); ?></a>
			</div>

			<?php
			echo $args['after_widget'];
		}

		/**
		 * Sanitize widget form values as they are saved.
		 *
		 * @param array $new_instance The new options.
		 * @param array $old_instance The previous options.
		 */
		public function update( $new_instance, $old_instance ) {
			$instance = array();

			$instance['api_key']          = sanitize_text_field( $new_instance['api_key'] );
			$instance['location']         = sanitize_text_field( $new_instance['location'] );
			$instance['latitude']         = sanitize_text_field( $new_instance['latitude'] );
			$instance['longitude']        = sanitize_text_field( $new_instance['longitude'] );
			$instance['temperature_unit'] = sanitize_key( $new_instance['temperature_unit'] );
			$instance['wind_unit']        = sanitize_key( $new_instance['wind_unit'] );
			$instance['forecast']         = sanitize_key( $new_instance['forecast'] );
			$instance['current_weather']  = ! empty( $new_instance['current_weather'] ) ? sanitize_key( $new_instance['current_weather'] ) : '';
			$instance['link_text']        = sanitize_text_field( $new_instance['link_text'] );
			$instance['link_url']         = esc_url_raw( $new_instance['link_url'] );

			// Set WP option for API key, so it can be retrieved when needed in API requests.
			// In this way we don't expose the API key to the frontend (in HTML or JS).
			update_option( ShakaHelpers::create_location_key( 'pt_shaka_api_key', $instance['latitude'], $instance['longitude'] ), $instance['api_key'] );

			// Delete currently cached weather data for this location (API response), if any.
			delete_transient( ShakaHelpers::create_location_key( 'pt_shaka_weather_data', $instance['latitude'], $instance['longitude'] ) );

			return $instance;
		}

		/**
		 * Back-end widget form.
		 *
		 * @param array $instance The widget options.
		 */
		public function form( $instance ) {
			$api_key          = ! empty( $instance['api_key'] ) ? $instance['api_key'] : '';
			$location         = ! empty( $instance['location'] ) ? $instance['location'] : '';
			$latitude         = ! empty( $instance['latitude'] ) ? $instance['latitude'] : '';
			$longitude        = ! empty( $instance['longitude'] ) ? $instance['longitude'] : '';
			$temperature_unit = ! empty( $instance['temperature_unit'] ) ? $instance['temperature_unit'] : 'celsius';
			$wind_unit        = ! empty( $instance['wind_unit'] ) ? $instance['wind_unit'] : 'meters_per_second';
			$current_weather  = ! empty( $instance['current_weather'] ) ? $instance['current_weather'] : '';
			$forecast         = ! empty( $instance['forecast'] ) ? $instance['forecast'] : '3';
			$link_text        = ! empty( $instance['link_text'] ) ? $instance['link_text'] : '';
			$link_url         = ! empty( $instance['link_url'] ) ? $instance['link_url'] : '';
		?>

			<div style="background-color: #eff8fb; border: 1px solid #5ac6ea; border-radius: 4px; padding: 20px; ">
				<strong><?php esc_html_e( 'Widget instructions:', 'shaka-pt' ); ?></strong>
				<ol>
					<li><strong><?php printf( esc_html__( '%sRegister at darksky.net/dev and get your API key%s.', 'shaka-pt' ), '<a href="https://darksky.net/dev/" target="_blank">', '</a>' ); ?></strong></li>
					<li><?php esc_html_e( 'Once you register, copy the API key to the field below.', 'shaka-pt' ); ?></li>
				</ol>
			</div>
			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'api_key' ) ); ?>"><?php esc_html_e( 'API key:', 'shaka-pt' ); ?></label>
				<br>
				<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'api_key' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'api_key' ) ); ?>" type="text" value="<?php echo esc_attr( $api_key ); ?>" />
			</p>
			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'location' ) ); ?>"><?php esc_html_e( 'Location:', 'shaka-pt' ); ?></label>
				<br>
				<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'location' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'location' ) ); ?>" type="text" value="<?php echo esc_attr( $location ); ?>" />
			</p>
			<p>
				<strong><?php esc_html_e( 'Location coordinates', 'shaka-pt' ); ?></strong>
				<br>
				<label for="<?php echo esc_attr( $this->get_field_id( 'latitude' ) ); ?>"><?php esc_html_e( 'Latitude:', 'shaka-pt' ); ?></label>
				<input id="<?php echo esc_attr( $this->get_field_id( 'latitude' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'latitude' ) ); ?>" type="text" value="<?php echo esc_attr( $latitude ); ?>" />
				<label for="<?php echo esc_attr( $this->get_field_id( 'longitude' ) ); ?>"><?php esc_html_e( 'Longitude:', 'shaka-pt' ); ?></label>
				<input id="<?php echo esc_attr( $this->get_field_id( 'longitude' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'longitude' ) ); ?>" type="text" value="<?php echo esc_attr( $longitude ); ?>" />
				<br>
				<small><?php printf( esc_html__( '%sGet the coordinates of your desired location here%s. Just copy/paste the latitude and longitude to the above fields.', 'shaka-pt' ), '<a href="http://www.latlong.net/" target="_blank">', '</a>' ); ?></small>
			</p>
			<p>
				<input class="checkbox" type="checkbox" <?php checked( $current_weather, 'on' ); ?> id="<?php echo esc_attr( $this->get_field_id( 'current_weather' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'current_weather' ) ); ?>" value="on" />
				<label for="<?php echo esc_attr( $this->get_field_id( 'current_weather' ) ); ?>"><?php esc_html_e( 'Display current weather in the main area instead of the today\'s forecast.', 'shaka-pt' ); ?></label>
				<br>
				<small><?php esc_html_e( 'If this is checked, the main area will display the current weather and the today\'s forecast will show right below it.', 'shaka-pt' ); ?></small>
			</p>
			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'forecast' ) ); ?>"><?php esc_html_e( 'Forecast for:', 'shaka-pt' ); ?></label>
				<br>
				<select name="<?php echo esc_attr( $this->get_field_name( 'forecast' ) ); ?>" id="<?php echo esc_attr( $this->get_field_id( 'forecast' ) ); ?>">
					<option value="0" <?php selected( '0', $forecast ); ?>><?php esc_html_e( 'Current day only', 'shaka-pt' ); ?></option>
					<option value="1" <?php selected( '1', $forecast ); ?>><?php esc_html_e( '1 day', 'shaka-pt' ); ?></option>
					<option value="2" <?php selected( '2', $forecast ); ?>><?php esc_html_e( '2 days', 'shaka-pt' ); ?></option>
					<option value="3" <?php selected( '3', $forecast ); ?>><?php esc_html_e( '3 days', 'shaka-pt' ); ?></option>
					<option value="4" <?php selected( '4', $forecast ); ?>><?php esc_html_e( '4 days', 'shaka-pt' ); ?></option>
					<option value="5" <?php selected( '5', $forecast ); ?>><?php esc_html_e( '5 days', 'shaka-pt' ); ?></option>
					<option value="6" <?php selected( '6', $forecast ); ?>><?php esc_html_e( '6 days', 'shaka-pt' ); ?></option>
					<option value="7" <?php selected( '7', $forecast ); ?>><?php esc_html_e( '7 days', 'shaka-pt' ); ?></option>
				</select>
			</p>
			<p>
				<strong><?php esc_html_e( 'Measurement units:', 'shaka-pt' ); ?></strong>
				<br>
				<label for="<?php echo esc_attr( $this->get_field_id( 'temperature_unit' ) ); ?>"><?php esc_html_e( 'Temperature unit:', 'shaka-pt' ); ?></label>
				<select name="<?php echo esc_attr( $this->get_field_name( 'temperature_unit' ) ); ?>" id="<?php echo esc_attr( $this->get_field_id( 'temperature_unit' ) ); ?>">
					<option value="celsius" <?php selected( 'celsius', $temperature_unit ); ?>><?php esc_html_e( 'Celsius', 'shaka-pt' ); ?></option>
					<option value="fahrenheit" <?php selected( 'fahrenheit', $temperature_unit ); ?>><?php esc_html_e( 'Fahrenheit', 'shaka-pt' ); ?></option>
				</select>
				<label for="<?php echo esc_attr( $this->get_field_id( 'wind_unit' ) ); ?>"><?php esc_html_e( 'Wind unit:', 'shaka-pt' ); ?></label>
				<select name="<?php echo esc_attr( $this->get_field_name( 'wind_unit' ) ); ?>" id="<?php echo esc_attr( $this->get_field_id( 'wind_unit' ) ); ?>">
					<option value="meters_per_second" <?php selected( 'meters_per_second', $wind_unit ); ?>><?php esc_html_e( 'm/s', 'shaka-pt' ); ?></option>
					<option value="kilometers_per_hour" <?php selected( 'kilometers_per_hour', $wind_unit ); ?>><?php esc_html_e( 'km/s', 'shaka-pt' ); ?></option>
					<option value="knots" <?php selected( 'knots', $wind_unit ); ?>><?php esc_html_e( 'knots', 'shaka-pt' ); ?></option>
					<option value="miles_per_hour" <?php selected( 'miles_per_hour', $wind_unit ); ?>><?php esc_html_e( 'mph (miles per hour)', 'shaka-pt' ); ?></option>
				</select>
			</p>
			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'link_text' ) ); ?>"><?php esc_html_e( 'Link text:', 'shaka-pt' ); ?></label>
				<br>
				<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'link_text' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'link_text' ) ); ?>" type="text" value="<?php echo esc_attr( $link_text ); ?>" />
			</p>
			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'link_url' ) ); ?>"><?php esc_html_e( 'Link url:', 'shaka-pt' ); ?></label>
				<br>
				<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'link_url' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'link_url' ) ); ?>" type="text" value="<?php echo esc_attr( $link_url ); ?>" />
			</p>

		<?php
		}
	}
	register_widget( 'PW_Weather' );
}

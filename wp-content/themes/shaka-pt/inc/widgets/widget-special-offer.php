<?php
/**
 * Special Offer Widget
 *
 * @package shaka-pt
 */

if ( ! class_exists( 'PW_Special_Offer' ) ) {
	class PW_Special_Offer extends WP_Widget {

		private $current_widget_id;
		private $font_awesome_icons_list;

		// Basic widget settings.
		function widget_id_base() { return 'special_offer'; }
		function widget_name() { return esc_html__( 'Special Offer', 'shaka-pt' ); }
		function widget_description() { return esc_html__( 'Display your special offer with important info.', 'shaka-pt' ); }
		function widget_class() { return 'widget-special-offer'; }

		/**
		 * Register widget with WordPress.
		 */
		public function __construct() {
			parent::__construct(
				'pw_' . $this->widget_id_base(),
				sprintf( 'ProteusThemes: %s', $this->widget_name() ),
				array(
					'description' => $this->widget_description(),
					'classname'   => $this->widget_class(),
				)
			);
			// A list of icons to choose from in the widget backend.
			$this->font_awesome_icons_list = apply_filters(
				'pw/special_offer_fa_icons_list',
				array(
					'fa-plus',
					'fa-minus',
					'fa-map-marker',
					'fa-home',
					'fa-phone',
					'fa-envelope-o',
					'fa-users',
					'fa-female',
					'fa-male',
					'fa-compass',
					'fa-money',
					'fa-suitcase',
					'fa-warning',
				)
			);
		}

		/**
		 * Front-end display of widget.
		 *
		 * @see WP_Widget::widget()
		 *
		 * @param array $args
		 * @param array $instance
		 */
		public function widget( $args, $instance ) {
			$items               = isset( $instance['items'] ) ? $instance['items'] : array();
			$instance['new_tab'] = ! empty( $instance['new_tab'] ) ? '_blank' : '_self';

			// Setup correct CTA URL.
			$cta_url  = '';
			$cta_type = empty( $instance['cta_type'] ) ? 'custom-url' : $instance['cta_type'];
			if ( 'custom-url' === $cta_type ) {
				$cta_url = empty( $instance['cta_custom_url'] ) ? '' : $instance['cta_custom_url'];
			}
			else if ( ! empty( $instance['cta_wc_product'] ) && 'none' !== $instance['cta_wc_product'] ) {
				$cta_url = add_query_arg( 'add-to-cart', esc_attr( $instance['cta_wc_product'] ), ShakaHelpers::get_woocommerce_cart_url() );
			}

			echo $args['before_widget'];
			?>
				<div class="special-offer">
					<div class="special-offer__image">
						<img class="img-fluid" src="<?php echo esc_url( $instance['image'] ); ?>" alt="<?php echo esc_html( $instance['title'] ); ?>">
					</div>
					<div class="special-offer__content">
						<?php if ( ! empty( $instance['label'] ) ) : ?>
							<div class="special-offer__label">
								<?php echo esc_html( $instance['label'] ); ?>
							</div>
						<?php endif; ?>
						<?php if ( ! empty( $instance['title'] ) ) : ?>
							<h4 class="special-offer__title">
								<?php echo esc_html( $instance['title'] ); ?>
							</h4>
						<?php endif; ?>
						<?php if ( ! empty( $instance['price'] ) ) : ?>
							<div class="special-offer__price">
								<?php echo esc_html( $instance['price'] ); ?>
							</div>
						<?php endif; ?>
						<?php if ( ! empty( $items ) ) : ?>
							<div class="special-offer__features">
								<?php foreach ( $items as $item ) : ?>
								<div class="special-offer__feature">
									<i class="fa  <?php echo esc_attr( $item['icon'] ); ?>" aria-hidden="true"></i>
									<?php echo esc_html( $item['text'] ); ?>
								</div>
								<?php endforeach; ?>
							</div>
						<?php endif; ?>
						<?php if ( ! empty( $instance['text'] ) ) : ?>
							<p class="special-offer__text">
								<?php echo wp_kses_post( $instance['text'] ); ?>
							</p>
						<?php endif; ?>
						<?php if ( ! empty( $cta_url ) ) : ?>
							<a href="<?php echo esc_url( $cta_url ); ?>" target="<?php echo ! empty( $instance['new_tab'] ) ? '_blank' : '_self'; ?>" class="btn  btn-primary  special-offer__cta"><?php echo ! empty( $instance['cta_text'] ) ? esc_html( $instance['cta_text'] ) : esc_html__( 'Book Now', 'shaka-pt' ); ?></a>
						<?php endif; ?>
					</div>
				</div>

			<?php
			echo $args['after_widget'];
		}

		/**
		 * Sanitize widget form values as they are saved.
		 *
		 * @param array $new_instance The new options.
		 * @param array $old_instance The previous options.
		 */
		public function update( $new_instance, $old_instance ) {
			$instance = array();

			$instance['title']          = sanitize_text_field( $new_instance['title'] );
			$instance['label']          = sanitize_text_field( $new_instance['label'] );
			$instance['price']          = sanitize_text_field( $new_instance['price'] );
			$instance['image']          = esc_url_raw( $new_instance['image'] );
			$instance['text']           = wp_kses_post( $new_instance['text'] );
			$instance['cta_text']       = sanitize_text_field( $new_instance['cta_text'] );
			$instance['cta_type']       = sanitize_key( $new_instance['cta_type'] );
			$instance['cta_custom_url'] = esc_url_raw( $new_instance['cta_custom_url'] );
			$instance['cta_wc_product'] = sanitize_key( $new_instance['cta_wc_product'] );
			$instance['new_tab']        = ! empty( $new_instance['new_tab'] ) ? sanitize_key( $new_instance['new_tab'] ) : '';

			if ( ! empty( $new_instance['items'] )  ) {
				foreach ( $new_instance['items'] as $key => $item ) {
					$instance['items'][ $key ]['id']   = sanitize_key( $item['id'] );
					$instance['items'][ $key ]['icon'] = sanitize_html_class( $item['icon'] );
					$instance['items'][ $key ]['text'] = wp_kses_post( $item['text'] );
				}
			}

			return $instance;
		}

		/**
		 * Back-end widget form.
		 *
		 * @param array $instance The widget options.
		 */
		public function form( $instance ) {

			$title          = empty( $instance['title'] ) ? '' : $instance['title'];
			$label          = empty( $instance['label'] ) ? '' : $instance['label'];
			$price          = empty( $instance['price'] ) ? '' : $instance['price'];
			$image          = empty( $instance['image'] ) ? '' : $instance['image'];
			$text           = empty( $instance['text'] ) ? '' : $instance['text'];
			$cta_text       = empty( $instance['cta_text'] ) ? '' : $instance['cta_text'];
			$cta_type       = empty( $instance['cta_type'] ) ? 'custom-url' : $instance['cta_type'];
			$cta_custom_url = empty( $instance['cta_custom_url'] ) ? '' : $instance['cta_custom_url'];
			$cta_wc_product = empty( $instance['cta_wc_product'] ) ? '' : $instance['cta_wc_product'];
			$new_tab        = empty( $instance['new_tab'] ) ? '' : $instance['new_tab'];
			$items          = isset( $instance['items'] ) ? $instance['items'] : array();

			// Page Builder fix when using repeating fields.
			if ( 'temp' === $this->id ) {
				$this->current_widget_id = $this->number;
			}
			else {
				$this->current_widget_id = $this->id;
			}

			?>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title:', 'shaka-pt' ); ?></label>
				<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
			</p>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'label' ) ); ?>"><?php esc_html_e( 'Label:', 'shaka-pt' ); ?></label>
				<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'label' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'label' ) ); ?>" type="text" value="<?php echo esc_attr( $label ); ?>" />
			</p>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'price' ) ); ?>"><?php esc_html_e( 'Price:', 'shaka-pt' ); ?></label>
				<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'price' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'price' ) ); ?>" type="text" value="<?php echo esc_attr( $price ); ?>" />
			</p>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'image' ) ); ?>"><?php esc_html_e( 'Picture URL:', 'shaka-pt' ); ?></label>
				<input class="widefat" style="margin-bottom: 6px;" id="<?php echo esc_attr( $this->get_field_id( 'image' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'image' ) ); ?>" type="text" value="<?php echo esc_attr( $image ); ?>" />
				<input type="button" onclick="ProteusWidgetsUploader.imageUploader.openFileFrame('<?php echo esc_attr( $this->get_field_id( 'image' ) ); ?>');" class="button button-secondary" value="<?php esc_html_e( 'Upload Image', 'shaka-pt' ); ?>" />
			</p>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'text' ) ); ?>"><?php esc_html_e( 'Description:', 'shaka-pt' ); ?></label>
				<textarea rows="4" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'text' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'text' ) ); ?>"><?php echo wp_kses_post( $text ); ?></textarea>
			</p>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'cta_text' ) ); ?>"><?php esc_html_e( 'CTA button text:', 'shaka-pt' ); ?></label>
				<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'cta_text' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'cta_text' ) ); ?>" type="text" value="<?php echo esc_attr( $cta_text ); ?>" />
			</p>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'cta_type' ) ); ?>"><?php esc_html_e( 'CTA button type:', 'shaka-pt' ); ?></label>
				<select class="js-pt-feature_cta_type" name="<?php echo esc_attr( $this->get_field_name( 'cta_type' ) ); ?>" id="<?php echo esc_attr( $this->get_field_id( 'cta_type' ) ); ?>">
					<option value="custom-url" <?php selected( $cta_type, 'custom-url' ) ?>><?php esc_html_e( 'Custom URL', 'shaka-pt' ); ?></option>
					<option value="wc-product" <?php selected( $cta_type, 'wc-product' ) ?>><?php esc_html_e( 'Link with WooCommerce product', 'shaka-pt' ); ?></option>
				</select>
			</p>

			<p class="js-pt-feature-custom-url" style="display: none;">
				<label for="<?php echo esc_attr( $this->get_field_id( 'cta_custom_url' ) ); ?>"><?php esc_html_e( 'CTA button URL:', 'shaka-pt' ); ?></label>
				<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'cta_custom_url' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'cta_custom_url' ) ); ?>" type="text" value="<?php echo esc_attr( $cta_custom_url ); ?>" />
			</p>

			<p class="js-pt-feature-wc-product" style="display: none;">
				<label for="<?php echo esc_attr( $this->get_field_id( 'cta_wc_product' ) ); ?>"><?php esc_html_e( 'CTA button product link:', 'shaka-pt' ); ?></label><br>
				<small><?php esc_html_e( 'Link a WooCommerce product to this CTA button. (Woocommerce plugin has to be active).', 'shaka-pt' );?></small><br>
				<?php if ( ShakaHelpers::is_woocommerce_active() ) : ?>
					<select name="<?php echo esc_attr( $this->get_field_name( 'cta_wc_product' ) ); ?>" id="<?php echo esc_attr( $this->get_field_id( 'cta_wc_product' ) ); ?>">
						<option value="none" <?php selected( $cta_wc_product, 'none' ); ?>><?php esc_html_e( 'Select product', 'shaka-pt' ); ?></option>
					<?php
						$args = array( 'post_type' => 'product', 'post_status' => array( 'publish' ) );
						$loop = new WP_Query( $args );
						while ( $loop->have_posts() ) :
							$loop->the_post();
							$id = get_the_ID();
					?>
							<option value="<?php echo esc_attr( $id ); ?>" <?php selected( $cta_wc_product, $id ); ?>><?php the_title(); ?></option>
					<?php
						endwhile;

						// Restore original Post Data.
						wp_reset_postdata();
					?>
					</select>
					<?php else : ?>
						<span style="color: red;"><?php esc_html_e( 'Woocommerce plugin has to be active, for this option!', 'shaka-pt' );?></span>
					<?php endif; ?>
			</p>

			<p>
				<input class="checkbox" type="checkbox" <?php checked( $new_tab, 'on' ); ?> id="<?php echo esc_attr( $this->get_field_id( 'new_tab' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'new_tab' ) ); ?>" value="on" />
				<label for="<?php echo esc_attr( $this->get_field_id( 'new_tab' ) ); ?>"><?php esc_html_e( 'Open CTA in new tab', 'shaka-pt' ); ?></label>
			</p>

			<hr>

			<h3><?php esc_html_e( 'Features:', 'shaka-pt' ); ?></h3>

			<script type="text/template" id="js-pt-feature-item-<?php echo esc_attr( $this->current_widget_id ); ?>">
				<p>
					<label for="<?php echo esc_attr( $this->get_field_id( 'items' ) ); ?>-{{id}}-text"><?php esc_html_e( 'Text:', 'shaka-pt' ); ?></label>
					<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'items' ) ); ?>-{{id}}-text" name="<?php echo esc_attr( $this->get_field_name( 'items' ) ); ?>[{{id}}][text]" type="text" value="{{text}}" />
				</p>

				<p>
					<label for="<?php echo esc_attr( $this->get_field_id( 'items' ) ); ?>-{{id}}-icon"><?php esc_html_e( 'Select icon:', 'shaka-pt' ); ?></label> <br />
					<small><?php printf( esc_html__( 'Click on the icon below or manually select from the %s website', 'shaka-pt' ), '<a href="http://fontawesome.io/icons/" target="_blank">FontAwesome</a>' ); ?>.</small>
					<input id="<?php echo esc_attr( $this->get_field_id( 'items' ) ); ?>-{{id}}-icon" name="<?php echo esc_attr( $this->get_field_name( 'items' ) ); ?>[{{id}}][icon]" type="text" value="{{icon}}" class="widefat  js-icon-input" /> <br><br>
					<?php foreach ( $this->font_awesome_icons_list as $icon ) : ?>
						<a class="js-selectable-icon  icon-widget" href="#" data-iconname="<?php echo esc_attr( $icon ); ?>"><i class="fa fa-lg <?php echo esc_attr( $icon ); ?>"></i></a>
					<?php endforeach; ?>
				</p>

				<p>
					<input name="<?php echo esc_attr( $this->get_field_name( 'items' ) ); ?>[{{id}}][id]" type="hidden" value="{{id}}" />
					<a href="#" class="pt-remove-feature-item  js-pt-remove-feature-item"><span class="dashicons dashicons-dismiss"></span> <?php esc_html_e( 'Remove item', 'shaka-pt' ); ?></a>
				</p>
			</script>
			<div class="pt-widget-feature-items" id="feature-items-<?php echo esc_attr( $this->current_widget_id ); ?>">
				<div class="feature-items"></div>
				<p>
					<a href="#" class="button  js-pt-add-feature-item"><?php esc_html_e( 'Add New Item', 'shaka-pt' ); ?></a>
				</p>
			</div>

			<script type="text/javascript">
				(function( $ ) {
					// Show/hide cta_type.
					if ( 'custom-url' === $( '.js-pt-feature_cta_type' ).val() ) {
						$( '.js-pt-feature-custom-url' ).show();
					}
					else {
						$( '.js-pt-feature-wc-product' ).show();
					}

					// repopulate the form - items
					var featureItemsJSON = <?php echo wp_json_encode( $items ) ?>;

					// get the right widget id and remove the added < > characters at the start and at the end.
					var widgetId = '<<?php echo esc_js( $this->current_widget_id ); ?>>'.slice( 1, -1 );

					if ( _.isFunction( Shaka.Utils.repopulateFeatureItems ) ) {
						Shaka.Utils.repopulateFeatureItems( featureItemsJSON, widgetId );
					}
				})( jQuery );
			</script>

			<?php
		}
	}
	register_widget( 'PW_Special_Offer' );
}

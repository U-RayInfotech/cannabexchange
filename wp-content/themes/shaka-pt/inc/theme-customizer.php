<?php
/**
 * Load the Customizer with some custom extended addons
 *
 * @package shaka-pt
 * @link http://codex.wordpress.org/Theme_Customization_API
 */

/**
 * This funtion is only called when the user is actually on the customizer page
 *
 * @param  WP_Customize_Manager $wp_customize
 */
if ( ! function_exists( 'shaka_customizer' ) ) {
	function shaka_customizer( $wp_customize ) {
		// Add required files.
		ShakaHelpers::load_file( '/inc/customizer/class-customize-base.php' );

		new Shaka_Customizer_Base( $wp_customize );
	}
	add_action( 'customize_register', 'shaka_customizer' );
}


/**
 * Takes care for the frontend output from the customizer and nothing else
 */
if ( ! function_exists( 'shaka_customizer_frontend' ) && ! class_exists( 'Shaka_Customize_Frontent' ) ) {
	function shaka_customizer_frontend() {
		ShakaHelpers::load_file( '/inc/customizer/class-customize-frontend.php' );
		$shaka_customize_frontent = new Shaka_Customize_Frontent();
	}
	add_action( 'init', 'shaka_customizer_frontend' );
}

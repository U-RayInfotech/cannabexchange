<?php
/**
 * Renders extra controls in the Gallery Settings section of the new media UI.
 *
 * Copied from Jetpack, original file: functions.gallery.php
 *
 * @package shaka-pt
 */

class ShakaGallery {
	public function __construct() {
		add_action( 'admin_init', array( $this, 'admin_init' ) );
		add_filter( 'post_gallery', array( $this, 'gallery_shortcode' ), 20, 2 );
	}

	/**
	 * Filter the available gallery types.
	 */
	public function admin_init() {
		$this->gallery_types = array(
			'wp'                    => esc_html__( 'Default WP gallery', 'shaka-pt' ),
			'shaka-gallery'        => esc_html__( 'Shaka gallery', 'shaka-pt' ),
			'shaka-gallery-bigger' => esc_html__( 'Shaka gallery (bigger first image)', 'shaka-pt' ),
		);

		add_action( 'print_media_templates', array( $this, 'print_media_templates' ) );
	}

	/**
	 * Outputs a view template which can be used with wp.media.template
	 */
	public function print_media_templates() {
		/**
		 * Filter the default gallery type.
		 *
		 * @module tiled-gallery
		 *
		 * @since 2.5.1
		 *
		 * @param string $value A string of the gallery type. Default is ‘default’.
		 */
		?>
		<script type="text/html" id="tmpl-shaka-gallery-settings">
			<label class="setting">
				<span><?php esc_html_e( 'Type', 'shaka-pt' ); ?></span>
				<select class="type" name="type" data-setting="type">
					<?php foreach ( $this->gallery_types as $value => $caption ) : ?>
						<option value="<?php echo esc_attr( $value ); ?>" <?php selected( $value, 'shaka-gallery' ); ?>><?php echo esc_html( $caption ); ?></option>
					<?php endforeach; ?>
				</select>
			</label>
		</script>
		<?php
	}

	/**
	 * Handler of the gallery shortcode.
	 * Copied from wp-includes/media.php, function gallery_shortcode
	 * and rewritten for our needs.
	 *
	 * @param string $output Shortcode output.
	 * @param array  $attr   Shortcode attributes.
	 *
	 * @return {String} HTML of the gallery or empty string
	 */
	public function gallery_shortcode( $output, $attr ) {
		global $post;

		if ( ! empty( $output ) ) { // Something else is overriding post_gallery.
			return $output;
		}

		$atts = shortcode_atts( array(
			'order'   => 'ASC',
			'orderby' => 'menu_order ID',
			'id'      => $post ? $post->ID : 0,
			'include' => '',
			'type'    => '',
		), $attr, 'gallery' );

		if ( 'shaka-gallery' !== $atts['type'] && 'shaka-gallery-bigger' !== $atts['type'] ) {
			return '';
		}

		$id = intval( $atts['id'] );

		$_attachments = get_posts( array( 'include' => $atts['include'], 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $atts['order'], 'orderby' => $atts['orderby'] ) );

		$attachments = array();
		foreach ( $_attachments as $key => $val ) {
			$attachments[ $val->ID ] = $_attachments[ $key ];
		}

		if ( empty( $attachments ) ) {
			return '';
		}

		if ( is_feed() ) {
			$output = "\n";
			foreach ( $attachments as $attachment ) {
				$output .= wp_get_attachment_link( $attachment->ID, 'thumbnail', true ) . "\n";
			}
			return $output;
		}

		$output = sprintf( '<div class="shaka-gallery beauy-galleryid-%d" data-featherlight-gallery data-featherlight-filter="a">', $id );

		// Add some copies of the 1st image for the 2nd style of gallery.
		$is_bigger = 'shaka-gallery-bigger' === $atts['type'];

		// Loop and output.
		$i = 0;
		foreach ( $attachments as $attachment ) {
			$tmpl = '<a href="%s" class="shaka-gallery__item"><img src="%s" srcset="%s" alt="" sizes="(min-width: 1200px) 275px, (min-width: 992px) 226px, (min-width: 768px) 172px, calc(50vw - 15px)"></a>';

			if ( $is_bigger && 0 === $i ) {
				$tmpl = '<a href="%s" class="shaka-gallery__item  shaka-gallery__item--bigger"><img src="%s" srcset="%s" alt="" sizes="(min-width: 1200px) 552px, (min-width: 992px) 453px, (min-width: 768px) 345px, calc(50vw - 15px)"></a>';
			}

			$srcset = ShakaHelpers::get_image_srcset( $attachment->ID, array( 'medium', 'large' ) );

			$img_full   = wp_get_attachment_image_src( $attachment->ID, 'full' );
			$img_medium = wp_get_attachment_image_src( $attachment->ID, 'medium' );

			$output .= sprintf(
				$tmpl,
				esc_url( $img_full[0] ),
				esc_url( $img_medium[0] ),
				esc_attr( $srcset )
			);

			if ( $is_bigger && in_array( $i, array( 0, 2 ) ) ) {
				$output .= '<span class="shaka-gallery__item  shaka-gallery__item--blank"></span><span class="shaka-gallery__item  shaka-gallery__item--blank"></span>';
			}

			$i++;
		}

		$output .= '</div>';

		return $output;
	}
}
new ShakaGallery;

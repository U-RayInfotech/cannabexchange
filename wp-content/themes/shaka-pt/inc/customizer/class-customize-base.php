<?php
/**
 * Customizer
 *
 * @package shaka-pt
 */

use ProteusThemes\CustomizerUtils\Setting;
use ProteusThemes\CustomizerUtils\Control;
use ProteusThemes\CustomizerUtils\CacheManager;
use ProteusThemes\CustomizerUtils\Helpers as WpUtilsHelpers;

/**
 * Contains methods for customizing the theme customization screen.
 *
 * @link http://codex.wordpress.org/Theme_Customization_API
 */
class Shaka_Customizer_Base {
	/**
	 * The singleton manager instance
	 *
	 * @see wp-includes/class-wp-customize-manager.php
	 * @var WP_Customize_Manager
	 */
	protected $wp_customize;

	/**
	 * Instance of the DynamicCSS cache manager
	 * @var ProteusThemes\CustomizerUtils\CacheManager
	 */
	private $dynamic_css_cache_manager;

	/**
	 * Holds the array for the DynamiCSS.
	 */
	private $dynamic_css = array();

	public function __construct( WP_Customize_Manager $wp_customize ) {
		// Set the private propery to instance of wp_customize.
		$this->wp_customize = $wp_customize;

		// Set the private propery to instance of DynamicCSS CacheManager.
		$this->dynamic_css_cache_manager = new CacheManager( $this->wp_customize );

		// Init the dynamic_css property.
		$this->dynamic_css = $this->dynamic_css_init();

		// Register the settings/panels/sections/controls
		$this->register_settings();
		$this->register_panels();
		$this->register_sections();
		$this->register_partials();
		$this->register_controls();

		/**
		 * Action and filters
		 */

		// Render the CSS and cache it to the theme_mod when the setting is saved.
		add_action( 'wp_head', array( 'ProteusThemes\CustomizerUtils\Helpers', 'add_dynamic_css_style_tag' ), 50, 0 );
		add_action( 'customize_save_after', function() {
			$shaka_woocommerce_selectors_filter_callable = false;

			if ( ! ShakaHelpers::is_woocommerce_active() ) {
				$shaka_woocommerce_selectors_filter_callable = array( '\ProteusThemes\CustomizerUtils\Helpers', 'is_not_woocommerce_css_selector' );
			}

			$this->dynamic_css_cache_manager->cache_rendered_css( $shaka_woocommerce_selectors_filter_callable );
		}, 10, 0 );

		// Save logo width/height dimensions.
		add_action( 'customize_save_logo_img', array( 'ProteusThemes\CustomizerUtils\Helpers', 'save_logo_dimensions' ), 10, 1 );
	}


	/**
	 * Initialization of the dynamic CSS settings with config arrays
	 * @return array
	 */
	private function dynamic_css_init() {
		$darken5   = new Setting\DynamicCSS\ModDarken( 5 );
		$darken13  = new Setting\DynamicCSS\ModDarken( 13 );
		$lighten8  = new Setting\DynamicCSS\ModLighten( 8 );

		return array(
			'top_bar_bg' => array(
				'default'    => '#ffffff',
				'css_props' => array( // List of all css properties this setting controls.
					array( // Each property in it's own array.
						'name'      => 'background-color',
						'selectors' => array(
							'noop' => array( // Regular selectors.
								'.top',
							),
						),
					),
				),
			),

			'top_bar_color' => array(
				'default'    => '#888888',
				'css_props' => array(
					array(
						'name'      => 'color',
						'selectors' => array(
							'noop' => array(
								'.top',
								'.top-navigation a',
								'.top .widget_nav_menu .menu a',
								'.top .social-icons__link',
								'.top .icon-box__title',
								'.top .icon-box',
							),
						),
					),
				),
			),

			'header_bg' => array(
				'default'    => '#ffffff',
				'css_props' => array(
					array(
						'name'      => 'background-color',
						'selectors' => array(
							'noop' => array(
								'.header__container',
							),
						),
					),
				),
			),

			'header_widgets_bg' => array(
				'default'    => '#3b3331',
				'css_props' => array(
					array(
						'name'      => 'background-color',
						'selectors' => array(
							'noop' => array(
								'.header__left-widgets .widget',
								'.header__right-widgets .widget',
								'.header .social-icons__link',
								'.header__left-widgets .widget_search .search-field',
								'.header__right-widgets .widget_search .search-field',
								'.header__left-widgets .widget_search .search-submit',
								'.header__right-widgets .widget_search .search-submit',
							),
						),
					),
					array(
						'name'      => 'border-color',
						'selectors' => array(
							'noop' => array(
								'.header__left-widgets .widget_search .search-field',
								'.header__right-widgets .widget_search .search-field',
							),
						),
					),
					array(
						'name'      => 'border-color',
						'selectors' => array(
							'noop' => array(
								'.header__left-widgets .widget',
								'.header__right-widgets .widget',
								'.header .social-icons__link + .social-icons__link',
							),
						),
						'modifier'  => $lighten8,
					),
				),
			),

			'header_widgets_text' => array(
				'default'    => '#f7f7f7',
				'css_props' => array(
					array(
						'name'      => 'color',
						'selectors' => array(
							'noop' => array(
								'.header__left-widgets .widget',
								'.header__right-widgets .widget',
								'.header .icon-box',
								'.header .icon-box__title',
								'.header a.icon-box:focus .fa',
								'.header a.icon-box:hover .fa',
								'.header .social-icons__link:focus',
								'.header .social-icons__link:hover',
								array(
									'.header__left-widgets .widget_search .search-field::-webkit-input-placeholder',
									'.header__right-widgets .widget_search .search-field::-webkit-input-placeholder',
								),
								array(
									'.header__left-widgets .widget_search .search-field::-moz-placeholder',
									'.header__right-widgets .widget_search .search-field::-moz-placeholder',
								),
								array(
									'.header__left-widgets .widget_search .search-field:-ms-input-placeholder',
									'.header__right-widgets .widget_search .search-field:-ms-input-placeholder',
								),
								array(
									'.header__left-widgets .widget_search .search-field::placeholder',
									'.header__right-widgets .widget_search .search-field::placeholder',
								),
							),
						),
					),
				),
			),

			'main_navigation_mobile_background' => array(
				'default' => '#f2f2f2',
				'css_props' => array(
					array(
						'name' => 'background-color',
						'selectors' => array(
							'@media (max-width: 991px)' => array(
								'.main-navigation',
							),
						),
					),
					array(
						'name' => 'border-color',
						'selectors' => array(
							'@media (max-width: 991px)' => array(
								'.main-navigation',
								'.main-navigation a',
							),
						),
						'modifier'  => $darken13,
					),
				),
			),

			'main_navigation_mobile_color' => array(
				'default' => '#3b3331',
				'css_props' => array(
					array(
						'name' => 'color',
						'selectors' => array(
							'@media (max-width: 991px)' => array(
								'.main-navigation a',
							),
						),
					),
				),
			),

			'main_navigation_mobile_color_hover' => array(
				'default' => '#000000',
				'css_props' => array(
					array(
						'name' => 'color',
						'selectors' => array(
							'@media (max-width: 991px)' => array(
								'.main-navigation .menu-item:focus > a',
								'.main-navigation .menu-item:hover > a',
							),
						),
					),
				),
			),

			'main_navigation_mobile_sub_color' => array(
				'default' => '#999999',
				'css_props' => array(
					array(
						'name' => 'color',
						'selectors' => array(
							'@media (max-width: 991px)' => array(
								'.main-navigation .sub-menu .menu-item > a',
							),
						),
					),
				),
			),

			'main_navigation_mobile_sub_color_hover' => array(
				'default' => '#333333',
				'css_props' => array(
					array(
						'name' => 'color',
						'selectors' => array(
							'@media (max-width: 991px)' => array(
								'.main-navigation .sub-menu .menu-item:hover > a',
								'.main-navigation .sub-menu .menu-item:focus > a',
							),
						),
					),
				),
			),

			'main_navigation_color' => array(
				'default' => '#3b3331',
				'css_props' => array(
					array(
						'name' => 'color',
						'selectors' => array(
							'@media (min-width: 992px)' => array(
								'.main-navigation a',
								'.main-navigation > .menu-item-has-children > a::after',
							),
						),
					),
				),
			),

			'main_navigation_color_hover' => array(
				'default' => '#d80019',
				'css_props' => array(
					array(
						'name' => 'color',
						'selectors' => array(
							'@media (min-width: 992px)' => array(
								'.main-navigation > .menu-item:focus > a',
								'.main-navigation > .menu-item:hover > a',
								'.main-navigation > .current-menu-item > a',
								'.main-navigation .menu-item:focus > a::after',
								'.main-navigation .menu-item:hover > a::after',
								'.main-navigation > .current-menu-ancestor > a',
								'.main-navigation > .current-menu-ancestor.menu-item-has-children > a::after',
								'.main-navigation > .current-menu-item.menu-item-has-children > a::after',
							),
						),
					),
				),
			),

			'main_navigation_sub_bg' => array(
				'default' => '#d80019',
				'css_props' => array(
					array(
						'name' => 'background-color',
						'selectors' => array(
							'@media (min-width: 992px)' => array(
								'.main-navigation .sub-menu a',
							),
						),
					),
					array(
						'name' => 'background-color',
						'selectors' => array(
							'@media (min-width: 992px)' => array(
								'.main-navigation .sub-menu .menu-item > a:hover',
							),
						),
						'modifier'  => $darken5,
					),
					array(
						'name' => 'border-color',
						'selectors' => array(
							'@media (min-width: 992px)' => array(
								'.main-navigation .sub-menu a',
								'.main-navigation .sub-menu .sub-menu a',
							),
						),
						'modifier'  => $darken5,
					),
					array(
						'name' => 'color',
						'selectors' => array(
							'@media (min-width: 992px)' => array(
								'.main-navigation .sub-menu .menu-item-has-children::after',
							),
						),
						'modifier'  => $darken5,
					),
				),
			),

			'main_navigation_sub_color' => array(
				'default' => '#ffffff',
				'css_props' => array(
					array(
						'name' => 'color',
						'selectors' => array(
							'@media (min-width: 992px)' => array(
								'.main-navigation .sub-menu .menu-item a',
								'.main-navigation .sub-menu .menu-item > a:hover',
							),
						),
					),
				),
			),

			'page_header_bg_color' => array(
				'default' => '#f2f2f2',
				'css_props' => array(
					array(
						'name' => 'background-color',
						'selectors' => array(
							'noop' => array(
								'.page-header',
							),
						),
					),
				),
			),

			'page_header_color' => array(
				'default' => '#3b3331',
				'css_props' => array(
					array(
						'name' => 'color',
						'selectors' => array(
							'noop' => array(
								'.page-header__title',
							),
						),
					),
				),
			),

			'page_header_subtitle_color' => array(
				'default' => '#888888',
				'css_props' => array(
					array(
						'name' => 'color',
						'selectors' => array(
							'noop' => array(
								'.page-header__subtitle',
							),
						),
					),
				),
			),

			'breadcrumbs_color' => array(
				'default'    => '#999999',
				'css_props' => array(
					array(
						'name'      => 'color',
						'selectors' => array(
							'noop' => array(
								'.breadcrumbs a',
								'.breadcrumbs a::after',
							),
						),
					),
				),
			),

			'breadcrumbs_color_hover' => array(
				'default'    => '#d80019',
				'css_props' => array(
					array(
						'name'      => 'color',
						'selectors' => array(
							'noop' => array(
								'.breadcrumbs a:focus',
								'.breadcrumbs a:hover',
							),
						),
					),
				),
			),

			'breadcrumbs_color_active' => array(
				'default'    => '#d80019',
				'css_props' => array(
					array(
						'name'      => 'color',
						'selectors' => array(
							'noop' => array(
								'.breadcrumbs .current-item',
							),
						),
					),
				),
			),

			'text_color_content_area' => array(
				'default' => '#888888',
				'css_props' => array(
					array(
						'name' => 'color',
						'selectors' => array(
							'noop' => array(
								'.content-area',
								'.content-area .icon-box',
							),
						),
					),
				),
			),

			'headings_color' => array(
				'default' => '#3b3331',
				'css_props' => array(
					array(
						'name' => 'color',
						'selectors' => array(
							'noop' => array(
								'h1',
								'h2',
								'h3',
								'h4',
								'h5',
								'h6',
								'hentry__title',
								'.hentry__title a',
								'.latest-news--block .latest-news__title a',
								'.latest-news--more-news',
								'.page-box__title a',
								'.sidebar__headings',
								'body.woocommerce-page ul.products li.product h3',
								'.woocommerce ul.products li.product h3',
								'body.woocommerce-page .entry-summary .entry-title',
							),
						),
					),
				),
			),

			'primary_color' => array(
				'default' => '#d80019',
				'css_props' => array(
					array(
						'name' => 'color',
						'selectors' => array(
							'noop' => array(
								'.btn-primary-outline',
								'.contact-profile__social-icon',
								'.person-profile__social-icon',
								'.content-area a.icon-box:focus .fa',
								'.content-area a.icon-box:hover .fa',
								'.content-area .widget_nav_menu .menu a:focus',
								'.content-area .widget_nav_menu .menu a:hover',
								'.sidebar .widget_nav_menu .menu a:focus',
								'.sidebar .widget_nav_menu .menu a:hover',
								'.top .social-icons__link:focus',
								'.top .social-icons__link:hover',
								'.top .widget_nav_menu .menu a:focus',
								'.top .widget_nav_menu .menu a:hover',
								'.content-area .widget_nav_menu .menu a::after',
								'.sidebar .widget_nav_menu .menu a::after',
								'.time-table .week-day.today',
								'.accordion .more-link:focus',
								'.accordion .more-link:hover',
								'.accordion__panel .panel-title a:hover',
								'.testimonial__author',
								'.testimonial__carousel',
								'.testimonial__carousel:focus',
								'.special-offer__price',
								'body.woocommerce-page .widget_product_categories .product-categories a:focus',
								'body.woocommerce-page .widget_product_categories .product-categories a:hover',
								'body.woocommerce-page .widget_product_categories .product-categories a::after',
								'body.woocommerce-page ul.products li.product .price',
								'.woocommerce ul.products li.product .price',
								'body.woocommerce-page ul.products li.product a:hover img',
								'.woocommerce ul.products li.product a:hover img',
								'body.woocommerce-page ul.products li.product a',
								'.woocommerce ul.products li.product a',
								'body.woocommerce-page div.product p.price',
								'.accordion__panel .panel-title a',
								'body.woocommerce-page nav.woocommerce-pagination ul li .prev',
								'body.woocommerce-page nav.woocommerce-pagination ul li .next',
								'.widget_archive a:hover',
								'.widget_pages a:hover',
								'.widget_categories a:hover',
								'.widget_meta a:hover',
								'.widget_recent_comments a:hover',
								'.widget_recent_entries a:hover',
								'.widget_rss a:hover',
								'.pw-instagram .loader',
							),
						),
					),
					array(
						'name' => 'color',
						'selectors' => array(
							'noop' => array(
								'.person-profile__social-icon:focus',
								'.person-profile__social-icon:hover',
								'.contact-profile__social-icon:focus',
								'.contact-profile__social-icon:hover',
								'.testimonial__carousel:hover',
								'html body.woocommerce-page nav.woocommerce-pagination ul li .next:focus',
								'html body.woocommerce-page nav.woocommerce-pagination ul li .next:hover',
								'html body.woocommerce-page nav.woocommerce-pagination ul li .prev:focus',
								'html body.woocommerce-page nav.woocommerce-pagination ul li .prev:hover',
							),
						),
						'modifier'  => $darken5,
					),
					array(
						'name' => 'background-color',
						'selectors' => array(
							'noop' => array(
								'.btn-primary-outline:hover',
								'.btn-primary-outline:focus',
								'.btn-primary-outline.focus',
								'btn-primary-outline:active',
								'.btn-primary-outline.active',
								'.open > .btn-primary-outline.dropdown-toggle',
								'.btn-primary',
								'.featured-product__price',
								'.shaka-table thead th',
								'.person-profile__label',
								'.pricing-list__badge',
								'.content-area .widget_nav_menu .menu li.current-menu-item > a',
								'.sidebar .widget_nav_menu .menu li.current-menu-item > a',
								'.portfolio-grid__card-price',
								'.latest-news--featured .latest-news__date',
								'.latest-news--block .latest-news__date',
								'.testimonial__container::before',
								'.testimonial__container::after',
								'.widget_calendar caption',
								'.masonry .hentry__date',
								'.contact-profile__label',
								'.special-offer__label',
								'.pagination .prev',
								'.pagination .next',
								'.post-password-form input',
								'body.woocommerce-page .widget_price_filter .ui-slider .ui-slider-handle',
								'body.woocommerce-page .widget_price_filter .ui-slider .ui-slider-range',
								'body.woocommerce-page .widget_product_categories .product-categories li.current-cat > a',
								'body.woocommerce-page a.button:hover',
								'body.woocommerce-page input.button:hover',
								'body.woocommerce-page input.button.alt:hover',
								'body.woocommerce-page button.button:hover',
								'body.woocommerce-page #review_form #respond input#submit',
								'body.woocommerce-page div.product form.cart .button.single_add_to_cart_button',
								'body.woocommerce-page div.product form.cart .button.single_add_to_cart_button:focus',
								'body.woocommerce-page div.product form.cart .button.single_add_to_cart_button:hover',
								'body.woocommerce-page .woocommerce-error a.button',
								'body.woocommerce-page .woocommerce-info a.button',
								'body.woocommerce-page .woocommerce-message a.button',
								'.woocommerce button.button.alt:disabled',
								'.woocommerce button.button.alt:disabled:hover',
								'.woocommerce button.button.alt:disabled[disabled]',
								'.woocommerce button.button.alt:disabled[disabled]:hover',
								'.woocommerce-cart .wc-proceed-to-checkout a.checkout-button',
								'body.woocommerce-page #payment #place_order',
								'body.woocommerce-page a.add_to_cart_button:hover',
								'.woocommerce a.add_to_cart_button:hover',
							),
						),
					),
					array(
						'name' => 'background-color',
						'selectors' => array(
							'noop' => array(
								'.btn-primary:focus',
								'.btn-primary:hover',
								'body.woocommerce-page #review_form #respond input#submit:hover',
								'.woocommerce-cart .wc-proceed-to-checkout a.checkout-button:hover',
								'body.woocommerce-page #payment #place_order:hover',
								'body.woocommerce-page .woocommerce-error a.button:hover',
								'body.woocommerce-page .woocommerce-info a.button:hover',
								'body.woocommerce-page .woocommerce-message a.button:hover',
								'.pagination .prev:focus',
								'.pagination .prev:hover',
								'.pagination .next:focus',
								'.pagination .next:hover',
								'body.woocommerce-page #review_form #respond input#submit:hover',
								'.woocommerce-cart .wc-proceed-to-checkout a.checkout-button:hover',
							),
						),
						'modifier'  => $darken5,
					),
					array(
						'name' => 'background-color',
						'selectors' => array(
							'noop' => array(
								'.btn-primary:active:hover',
								'.btn-primary:active:focus',
								'.btn-primary:active.focus',
								'.btn-primary.active.focus',
								'.btn-primary.active:focus',
								'.btn-primary.active:hover',
							),
						),
						'modifier'  => $darken13,
					),
					array(
						'name' => 'background',
						'selectors' => array(
							'noop' => array(
								'body.woocommerce-page nav.woocommerce-pagination ul li .prev',
								'body.woocommerce-page nav.woocommerce-pagination ul li .next',
							),
						),
					),
					array(
						'name' => 'background',
						'selectors' => array(
							'noop' => array(
								'body.woocommerce-page nav.woocommerce-pagination ul li .prev:hover',
								'body.woocommerce-page nav.woocommerce-pagination ul li .next:hover',
							),
						),
						'modifier'  => $darken5,
					),
					array(
						'name' => 'border-color',
						'selectors' => array(
							'noop' => array(
								'.btn-primary-outline',
								'.btn-primary-outline:hover',
								'.btn-primary-outline:focus',
								'.btn-primary-outline.focus',
								'btn-primary-outline:active',
								'.btn-primary-outline.active',
								'.open > .btn-primary-outline.dropdown-toggle',
								'.btn-primary',
								'.pagination .prev',
								'.pagination .next',
								'.post-password-form input',
								'body.woocommerce-page nav.woocommerce-pagination ul li .prev',
								'body.woocommerce-page nav.woocommerce-pagination ul li .next',
							),
						),
					),
					array(
						'name' => 'border-color',
						'selectors' => array(
							'noop' => array(
								'.btn-primary:focus',
								'.btn-primary:hover',
								'.pagination .prev:focus',
								'.pagination .prev:hover',
								'.pagination .next:focus',
								'.pagination .next:hover',
								'body.woocommerce-page nav.woocommerce-pagination ul li .prev:hover',
								'body.woocommerce-page nav.woocommerce-pagination ul li .next:hover',
							),
						),
						'modifier'  => $darken5,
					),
					array(
						'name' => 'border-color',
						'selectors' => array(
							'noop' => array(
								'.btn-primary:active:hover',
								'.btn-primary:active:focus',
								'.btn-primary:active.focus',
								'.btn-primary.active.focus',
								'.btn-primary.active:focus',
								'.btn-primary.active:hover',
							),
						),
						'modifier'  => $darken13,
					),
				),
			),

			'link_color' => array(
				'default' => '#5897cc',
				'css_props' => array(
					array(
						'name' => 'color',
						'selectors' => array(
							'noop' => array(
								'a',
							),
						),
					),
					array(
						'name' => 'color',
						'selectors' => array(
							'noop' => array(
								'a:focus',
								'a:hover',
							),
						),
						'modifier'  => $darken5,
					),
				),
			),

			'dark_button_color' => array(
				'default' => '#3b3331',
				'css_props' => array(
					array(
						'name' => 'background-color',
						'selectors' => array(
							'noop' => array(
								'.btn-dark',
							),
						),
					),
					array(
						'name' => 'background-color',
						'selectors' => array(
							'noop' => array(
								'.btn-dark:focus',
								'.btn-dark:hover',
							),
						),
						'modifier'  => $darken5,
					),
				),
			),

			'light_button_color' => array(
				'default' => '#ffffff',
				'css_props' => array(
					array(
						'name' => 'background-color',
						'selectors' => array(
							'noop' => array(
								'.btn-light',
							),
						),
					),
					array(
						'name' => 'background-color',
						'selectors' => array(
							'noop' => array(
								'.btn-light:focus',
								'.btn-light:hover',
							),
						),
						'modifier'  => $darken5,
					),
				),
			),

			'body_bg' => array(
				'default'   => '#ffffff',
				'css_props' => array(
					array(
						'name'      => 'background-color',
						'selectors' => array(
							'noop' => array(
								'body .boxed-container',
							),
						),
					),
				),
			),

			'footer_bg_color' => array(
				'default' => '#3b3331',
				'css_props' => array(
					array(
						'name' => 'background-color',
						'selectors' => array(
							'noop' => array(
								'.footer-top',
							),
						),
					),
				),
			),

			'footer_title_color' => array(
				'default' => '#ffffff',
				'css_props' => array(
					array(
						'name' => 'color',
						'selectors' => array(
							'noop' => array(
								'.footer-top__heading',
							),
						),
					),
				),
			),

			'footer_text_color' => array(
				'default' => '#a5a2a1',
				'css_props' => array(
					array(
						'name' => 'color',
						'selectors' => array(
							'noop' => array(
								'.footer-top',
							),
						),
					),
				),
			),

			'footer_link_color' => array(
				'default' => '#888888',
				'css_props' => array(
					array(
						'name' => 'color',
						'selectors' => array(
							'noop' => array(
								'.footer-top a',
								'.footer-top .widget_nav_menu .menu a',
							),
						),
					),
				),
			),

			'footer_bottom_bg_color' => array(
				'default' => '#3b3331',
				'css_props' => array(
					array(
						'name' => 'background-color',
						'selectors' => array(
							'noop' => array(
								'.footer',
							),
						),
					),
				),
			),

			'footer_bottom_text_color' => array(
				'default' => '#a5a2a1',
				'css_props' => array(
					array(
						'name' => 'color',
						'selectors' => array(
							'noop' => array(
								'.footer-bottom',
							),
						),
					),
				),
			),

			'footer_bottom_link_color' => array(
				'default' => '#a5a2a1',
				'css_props' => array(
					array(
						'name' => 'color',
						'selectors' => array(
							'noop' => array(
								'.footer-bottom a',
							),
						),
					),
				),
			),
		);
	}

	/**
	 * Register customizer settings
	 *
	 * @return void
	 */
	public function register_settings() {
		// Branding.
		$this->wp_customize->add_setting( 'logo_img' );
		$this->wp_customize->add_setting( 'logo2x_img' );
		$this->wp_customize->add_setting( 'logo_top_margin', array( 'default' => 18 ) );

		// Header.
		$this->wp_customize->add_setting( 'top_bar_visibility', array( 'default' => 'yes' ) );
		$this->wp_customize->add_setting( 'header_bg_img' );
		$this->wp_customize->add_setting( 'header_bg_img_repeat', array( 'default' => 'repeat' ) );
		$this->wp_customize->add_setting( 'header_bg_img_position_x', array( 'default' => 'left' ) );
		$this->wp_customize->add_setting( 'header_bg_img_attachment', array( 'default' => 'scroll' ) );

		// Featured page.
		$this->wp_customize->add_setting( 'featured_page_select', array( 'default' => 'none' ) );
		$this->wp_customize->add_setting( 'featured_page_custom_text' );
		$this->wp_customize->add_setting( 'featured_page_custom_url' );
		$this->wp_customize->add_setting( 'featured_page_open_in_new_window' );

		// Page header area.
		$this->wp_customize->add_setting( 'page_header_bg_img' );
		$this->wp_customize->add_setting( 'page_header_bg_img_repeat', array( 'default' => 'repeat' ) );
		$this->wp_customize->add_setting( 'page_header_bg_img_position_x', array( 'default' => 'left' ) );
		$this->wp_customize->add_setting( 'page_header_bg_img_attachment', array( 'default' => 'scroll' ) );
		$this->wp_customize->add_setting( 'show_page_title_area', array( 'default' => 'yes' ) );

		// Typography.
		$this->wp_customize->add_setting( 'charset_setting', array( 'default' => 'latin' ) );

		// Theme layout & color.
		$this->wp_customize->add_setting( 'layout_mode', array( 'default' => 'wide' ) );
		$this->wp_customize->add_setting( 'blog_columns', array( 'default' => 6 ) );
		$this->wp_customize->add_setting( 'body_bg_img' );
		$this->wp_customize->add_setting( 'body_bg_img_repeat', array( 'default' => 'repeat' ) );
		$this->wp_customize->add_setting( 'body_bg_img_position_x', array( 'default' => 'left' ) );
		$this->wp_customize->add_setting( 'body_bg_img_attachment', array( 'default' => 'scroll' ) );

		// Shop.
		if ( ShakaHelpers::is_woocommerce_active() ) {
			$this->wp_customize->add_setting( 'products_per_page', array( 'default' => 9 ) );
			$this->wp_customize->add_setting( 'single_product_sidebar', array( 'default' => 'left' ) );
		}

		// Portfolio.
		if ( ShakaHelpers::is_portfolio_plugin_active() ) {
			$this->wp_customize->add_setting( 'portfolio_parent_page', array( 'default' => 0 ) );
			$this->wp_customize->add_setting( 'portfolio_gallery_columns', array( 'default' => 2 ) );
			$this->wp_customize->add_setting( 'portfolio_slug', array(
				'default'           => 'portfolio',
				'sanitize_callback' => function( $title ) {
					return sanitize_title( $title, 'portfolio' );
				},
			) );
		}

		// Footer.
		$this->wp_customize->add_setting( 'footer_widgets_layout', array( 'default' => '[3,5,7,9]' ) );

		$this->wp_customize->add_setting( 'footer_bottom_left_txt', array( 'default' => '<a href="https://www.proteusthemes.com/wordpress-themes/shaka/">Shaka Theme</a> - Made by ProteusThemes.' ) );
		$this->wp_customize->add_setting( 'footer_bottom_center_txt', array( 'default' => '[fa icon="fa-facebook" href="https://www.facebook.com/ProteusThemes/"] [fa icon="fa-youtube" href="https://www.youtube.com/user/ProteusNetCompany"] [fa icon="fa-twitter" href="https://twitter.com/ProteusThemes"]' ) );
		$this->wp_customize->add_setting( 'footer_bottom_right_txt', array( 'default' => 'Copyright &copy; ' . date( 'Y' ) . ' Shaka. All rights reserved.' ) );

		// Custom code (css/js).
		$this->wp_customize->add_setting( 'custom_js_head' );
		$this->wp_customize->add_setting( 'custom_js_footer' );
		$this->wp_customize->add_setting( 'custom_css', array( 'default' => '' ) );

		// Migrate any existing theme CSS to the core option added in WordPress 4.7.
		if ( function_exists( 'wp_update_custom_css_post' ) ) {
			$css = get_theme_mod( 'custom_css', '' );

			if ( ! empty( $css ) ) {
				$core_css = wp_get_custom_css(); // Preserve any CSS already added to the core option.
				$return   = wp_update_custom_css_post( '/* Migrated CSS from old Theme Custom CSS setting: */' . PHP_EOL . $css . PHP_EOL . PHP_EOL . '/* New custom CSS: */' . PHP_EOL . $core_css );
				if ( ! is_wp_error( $return ) ) {
					// Remove the old theme_mod, so that the CSS is stored in only one place moving forward.
					remove_theme_mod( 'custom_css' );
				}
			}

			// Add new "CSS setting" that will only notify the users that the new "Additional CSS" field is available.
			// It can't be the same name ('custom_css'), because the core control is also named 'custom_css' and it would not display the WP core "Additional CSS" control.
			$this->wp_customize->add_setting( 'pt_custom_css', array( 'default' => '' ) );
		}

		// ACF.
		$this->wp_customize->add_setting( 'show_acf', array( 'default' => 'no' ) );
		$this->wp_customize->add_setting( 'use_minified_css', array( 'default' => 'no' ) );

		// All the DynamicCSS settings.
		foreach ( $this->dynamic_css as $setting_id => $args ) {
			$this->wp_customize->add_setting(
				new Setting\DynamicCSS( $this->wp_customize, $setting_id, $args )
			);
		}
	}


	/**
	 * Panels
	 *
	 * @return void
	 */
	public function register_panels() {
		// One ProteusThemes panel to rule them all.
		$this->wp_customize->add_panel( 'panel_shaka', array(
			'title'       => esc_html__( '[PT] Theme Options', 'shaka-pt' ),
			'description' => esc_html__( 'All Shaka theme specific settings.', 'shaka-pt' ),
			'priority'    => 10,
		) );
	}


	/**
	 * Sections
	 *
	 * @return void
	 */
	public function register_sections() {
		$this->wp_customize->add_section( 'shaka_section_logos', array(
			'title'       => esc_html__( 'Logo', 'shaka-pt' ),
			'description' => esc_html__( 'Logo for the Shaka theme.', 'shaka-pt' ),
			'priority'    => 10,
			'panel'       => 'panel_shaka',
		) );

		$this->wp_customize->add_section( 'shaka_section_header', array(
			'title'       => esc_html__( 'Header', 'shaka-pt' ),
			'description' => esc_html__( 'All layout and appearance settings for the header.', 'shaka-pt' ),
			'priority'    => 20,
			'panel'       => 'panel_shaka',
		) );

		$this->wp_customize->add_section( 'shaka_section_navigation', array(
			'title'       => esc_html__( 'Navigation', 'shaka-pt' ),
			'description' => esc_html__( 'Navigation for the Shaka theme.', 'shaka-pt' ),
			'priority'    => 30,
			'panel'       => 'panel_shaka',
		) );

		$this->wp_customize->add_section( 'shaka_section_page_header', array(
			'title'       => esc_html__( 'Page Header Area', 'shaka-pt' ),
			'description' => esc_html__( 'All layout and appearance settings for the page header area (regular pages).', 'shaka-pt' ),
			'priority'    => 33,
			'panel'       => 'panel_shaka',
		) );

		$this->wp_customize->add_section( 'shaka_section_breadcrumbs', array(
			'title'       => esc_html__( 'Breadcrumbs', 'shaka-pt' ),
			'description' => esc_html__( 'All layout and appearance settings for breadcrumbs.', 'shaka-pt' ),
			'priority'    => 35,
			'panel'       => 'panel_shaka',
		) );

		$this->wp_customize->add_section( 'shaka_section_theme_colors', array(
			'title'       => esc_html__( 'Theme Layout &amp; Colors', 'shaka-pt' ),
			'priority'    => 40,
			'panel'       => 'panel_shaka',
		) );

		if ( ShakaHelpers::is_woocommerce_active() ) {
			$this->wp_customize->add_section( 'shaka_section_shop', array(
				'title'       => esc_html__( 'Shop', 'shaka-pt' ),
				'priority'    => 80,
				'panel'       => 'panel_shaka',
			) );
		}

		if ( ShakaHelpers::is_portfolio_plugin_active() ) {
			$this->wp_customize->add_section( 'shaka_section_portfolio', array(
				'title'       => esc_html__( 'Portfolio', 'shaka-pt' ),
				'priority'    => 85,
				'panel'       => 'panel_shaka',
			) );
		}

		$this->wp_customize->add_section( 'section_footer', array(
			'title'       => esc_html__( 'Footer', 'shaka-pt' ),
			'description' => esc_html__( 'All layout and appearance settings for the footer.', 'shaka-pt' ),
			'priority'    => 90,
			'panel'       => 'panel_shaka',
		) );

		$this->wp_customize->add_section( 'section_custom_code', array(
			'title'       => esc_html__( 'Custom Code' , 'shaka-pt' ),
			'priority'    => 100,
			'panel'       => 'panel_shaka',
		) );

		$this->wp_customize->add_section( 'section_other', array(
			'title'       => esc_html__( 'Other' , 'shaka-pt' ),
			'priority'    => 150,
			'panel'       => 'panel_shaka',
		) );
	}


	/**
	 * Partials for selective refresh
	 *
	 * @return void
	 */
	public function register_partials() {
		$this->wp_customize->selective_refresh->add_partial( 'dynamic_css', array(
			'selector' => 'head > #wp-utils-dynamic-css-style-tag',
			'settings' => array_keys( $this->dynamic_css ),
			'render_callback' => function() {
				return $this->dynamic_css_cache_manager->render_css();
			},
		) );
	}


	/**
	 * Controls
	 *
	 * @return void
	 */
	public function register_controls() {
		// Section: shaka_section_logos.
		$this->wp_customize->add_control( new WP_Customize_Image_Control(
			$this->wp_customize,
			'logo_img',
			array(
				'label'       => esc_html__( 'Logo Image', 'shaka-pt' ),
				'description' => esc_html__( 'Max recommended height for the logo image is 86px.', 'shaka-pt' ),
				'section'     => 'shaka_section_logos',
			)
		) );
		$this->wp_customize->add_control( new WP_Customize_Image_Control(
			$this->wp_customize,
			'logo2x_img',
			array(
				'label'       => esc_html__( 'Retina Logo Image', 'shaka-pt' ),
				'description' => esc_html__( '2x logo size, for screens with high DPI.', 'shaka-pt' ),
				'section'     => 'shaka_section_logos',
			)
		) );
		$this->wp_customize->add_control(
			'logo_top_margin',
			array(
				'type'        => 'number',
				'label'       => esc_html__( 'Logo top margin', 'shaka-pt' ),
				'description' => esc_html__( 'In pixels.', 'shaka-pt' ),
				'section'     => 'shaka_section_logos',
				'input_attrs' => array(
					'min'  => 0,
					'max'  => 120,
					'step' => 5,
				),
			)
		);

		// Section: header.
		$this->wp_customize->add_control( new WP_Customize_Image_Control(
			$this->wp_customize,
			'header_bg_img',
			array(
				'priority' => 10,
				'label'    => esc_html__( 'Header Background Image', 'shaka-pt' ),
				'section'  => 'shaka_section_header',
			)
		) );
		$this->wp_customize->add_control( 'header_bg_img_repeat', array(
			'priority'        => 11,
			'label'           => esc_html__( 'Header Background Repeat', 'shaka-pt' ),
			'section'         => 'shaka_section_header',
			'type'            => 'radio',
			'active_callback' => function() {
				return WpUtilsHelpers::is_theme_mod_not_empty( 'header_bg_img' );
			},
			'choices'         => array(
				'no-repeat' => esc_html__( 'No Repeat', 'shaka-pt' ),
				'repeat'    => esc_html__( 'Tile', 'shaka-pt' ),
				'repeat-x'  => esc_html__( 'Tile Horizontally', 'shaka-pt' ),
				'repeat-y'  => esc_html__( 'Tile Vertically', 'shaka-pt' ),
			),
		) );
		$this->wp_customize->add_control( 'header_bg_img_position_x', array(
			'priority'        => 12,
			'label'           => esc_html__( 'Header Background Position', 'shaka-pt' ),
			'section'         => 'shaka_section_header',
			'type'            => 'radio',
			'active_callback' => function() {
				return WpUtilsHelpers::is_theme_mod_not_empty( 'header_bg_img' );
			},
			'choices'         => array(
				'left'   => esc_html__( 'Left', 'shaka-pt' ),
				'center' => esc_html__( 'Center', 'shaka-pt' ),
				'right'  => esc_html__( 'Right', 'shaka-pt' ),
			),
		) );
		$this->wp_customize->add_control( 'header_bg_img_attachment', array(
			'priority'        => 13,
			'label'           => esc_html__( 'Header Background Attachment', 'shaka-pt' ),
			'section'         => 'shaka_section_header',
			'type'            => 'radio',
			'active_callback' => function() {
				return WpUtilsHelpers::is_theme_mod_not_empty( 'header_bg_img' );
			},
			'choices'         => array(
				'scroll' => esc_html__( 'Scroll', 'shaka-pt' ),
				'fixed'  => esc_html__( 'Fixed', 'shaka-pt' ),
			),
		) );
		$this->wp_customize->add_control( new WP_Customize_Color_Control(
			$this->wp_customize,
			'header_bg',
			array(
				'priority' => 14,
				'label'    => esc_html__( 'Header background color', 'shaka-pt' ),
				'section'  => 'shaka_section_header',
			)
		) );
		$this->wp_customize->add_control( new WP_Customize_Color_Control(
			$this->wp_customize,
			'header_widgets_bg',
			array(
				'priority' => 15,
				'label'    => esc_html__( 'Header widgets background color', 'shaka-pt' ),
				'section'  => 'shaka_section_header',
			)
		) );
		$this->wp_customize->add_control( new WP_Customize_Color_Control(
			$this->wp_customize,
			'header_widgets_text',
			array(
				'priority' => 15,
				'label'    => esc_html__( 'Header widgets text color', 'shaka-pt' ),
				'section'  => 'shaka_section_header',
			)
		) );

		$this->wp_customize->add_control( 'top_bar_visibility', array(
			'type'        => 'select',
			'priority'    => 20,
			'label'       => esc_html__( 'Top bar visibility', 'shaka-pt' ),
			'description' => esc_html__( 'Show or hide?', 'shaka-pt' ),
			'section'     => 'shaka_section_header',
			'choices'     => array(
				'yes'         => esc_html__( 'Show', 'shaka-pt' ),
				'no'          => esc_html__( 'Hide', 'shaka-pt' ),
				'hide_mobile' => esc_html__( 'Hide on Mobile', 'shaka-pt' ),
			),
		) );
		$this->wp_customize->add_control( new WP_Customize_Color_Control(
			$this->wp_customize,
			'top_bar_bg',
			array(
				'priority' => 21,
				'label'    => esc_html__( 'Top bar background color', 'shaka-pt' ),
				'section'  => 'shaka_section_header',
			)
		) );
		$this->wp_customize->add_control( new WP_Customize_Color_Control(
			$this->wp_customize,
			'top_bar_color',
			array(
				'priority' => 22,
				'label'    => esc_html__( 'Top bar text color', 'shaka-pt' ),
				'section'  => 'shaka_section_header',
			)
		) );

		// Section: shaka_section_navigation.
		$this->wp_customize->add_control( 'featured_page_select', array(
			'type'        => 'select',
			'priority'    => 113,
			'label'       => esc_html__( 'Featured page', 'shaka-pt' ),
			'description' => esc_html__( 'To which page should the Featured Page button link to?', 'shaka-pt' ),
			'section'     => 'shaka_section_navigation',
			'choices'     => WpUtilsHelpers::get_all_pages_id_title(),
		) );
		$this->wp_customize->add_control( 'featured_page_custom_text', array(
			'priority'        => 115,
			'label'           => esc_html__( 'Custom Button Text', 'shaka-pt' ),
			'section'         => 'shaka_section_navigation',
			'active_callback' => function() {
				return WpUtilsHelpers::is_theme_mod_specific_value( 'featured_page_select', 'custom-url' );
			},
		) );

		$this->wp_customize->add_control( 'featured_page_custom_url', array(
			'priority'        => 117,
			'label'           => esc_html__( 'Custom URL', 'shaka-pt' ),
			'section'         => 'shaka_section_navigation',
			'active_callback' => function() {
				return WpUtilsHelpers::is_theme_mod_specific_value( 'featured_page_select', 'custom-url' );
			},
		) );

		$this->wp_customize->add_control( 'featured_page_open_in_new_window', array(
			'type'            => 'checkbox',
			'priority'        => 120,
			'label'           => esc_html__( 'Open link in a new window/tab.', 'shaka-pt' ),
			'section'         => 'shaka_section_navigation',
			'active_callback' => function() {
				return ( ! WpUtilsHelpers::is_theme_mod_specific_value( 'featured_page_select', 'none' ) );
			},
		) );
		$this->wp_customize->add_control( new WP_Customize_Color_Control(
			$this->wp_customize,
			'main_navigation_color',
			array(
				'priority' => 130,
				'label'    => esc_html__( 'Main navigation link color', 'shaka-pt' ),
				'section'  => 'shaka_section_navigation',
			)
		) );
		$this->wp_customize->add_control( new WP_Customize_Color_Control(
			$this->wp_customize,
			'main_navigation_color_hover',
			array(
				'priority' => 132,
				'label'    => esc_html__( 'Main navigation link hover color', 'shaka-pt' ),
				'section'  => 'shaka_section_navigation',
			)
		) );
		$this->wp_customize->add_control( new WP_Customize_Color_Control(
			$this->wp_customize,
			'main_navigation_sub_bg',
			array(
				'priority' => 160,
				'label'    => esc_html__( 'Main navigation submenu background', 'shaka-pt' ),
				'section'  => 'shaka_section_navigation',
			)
		) );
		$this->wp_customize->add_control( new WP_Customize_Color_Control(
			$this->wp_customize,
			'main_navigation_sub_color',
			array(
				'priority' => 170,
				'label'    => esc_html__( 'Main navigation submenu link color', 'shaka-pt' ),
				'section'  => 'shaka_section_navigation',
			)
		) );
		$this->wp_customize->add_control( new WP_Customize_Color_Control(
			$this->wp_customize,
			'main_navigation_mobile_color',
			array(
				'priority' => 190,
				'label'    => esc_html__( 'Main navigation link color (mobile)', 'shaka-pt' ),
				'section'  => 'shaka_section_navigation',
			)
		) );
		$this->wp_customize->add_control( new WP_Customize_Color_Control(
			$this->wp_customize,
			'main_navigation_mobile_color_hover',
			array(
				'priority' => 192,
				'label'    => esc_html__( 'Main navigation link hover color (mobile)', 'shaka-pt' ),
				'section'  => 'shaka_section_navigation',
			)
		) );
		$this->wp_customize->add_control( new WP_Customize_Color_Control(
			$this->wp_customize,
			'main_navigation_mobile_sub_color',
			array(
				'priority' => 194,
				'label'    => esc_html__( 'Main navigation submenu link color (mobile)', 'shaka-pt' ),
				'section'  => 'shaka_section_navigation',
			)
		) );
		$this->wp_customize->add_control( new WP_Customize_Color_Control(
			$this->wp_customize,
			'main_navigation_mobile_sub_color_hover',
			array(
				'priority' => 195,
				'label'    => esc_html__( 'Main navigation submenu link hover color (mobile)', 'shaka-pt' ),
				'section'  => 'shaka_section_navigation',
			)
		) );
		$this->wp_customize->add_control( new WP_Customize_Color_Control(
			$this->wp_customize,
			'main_navigation_mobile_background',
			array(
				'priority' => 188,
				'label'    => esc_html__( 'Main navigation background color (mobile)', 'shaka-pt' ),
				'section'  => 'shaka_section_navigation',
			)
		) );

		// Section: shaka_section_page_header.
		$this->wp_customize->add_control( new WP_Customize_Color_Control(
			$this->wp_customize,
			'page_header_bg_color',
			array(
				'priority' => 10,
				'label'    => esc_html__( 'Page Header background color', 'shaka-pt' ),
				'section'  => 'shaka_section_page_header',
			)
		) );

		$this->wp_customize->add_control( new WP_Customize_Image_Control(
			$this->wp_customize,
			'page_header_bg_img',
			array(
				'priority' => 20,
				'label'    => esc_html__( 'Page Header Background Image', 'shaka-pt' ),
				'section'  => 'shaka_section_page_header',
			)
		) );
		$this->wp_customize->add_control( 'page_header_bg_img_repeat', array(
			'priority'        => 21,
			'label'           => esc_html__( 'Page Header Background Repeat', 'shaka-pt' ),
			'section'         => 'shaka_section_page_header',
			'type'            => 'radio',
			'active_callback' => function() {
				return WpUtilsHelpers::is_theme_mod_not_empty( 'page_header_bg_img' );
			},
			'choices'         => array(
				'no-repeat'  => esc_html__( 'No Repeat', 'shaka-pt' ),
				'repeat'     => esc_html__( 'Tile', 'shaka-pt' ),
				'repeat-x'   => esc_html__( 'Tile Horizontally', 'shaka-pt' ),
				'repeat-y'   => esc_html__( 'Tile Vertically', 'shaka-pt' ),
			),
		) );
		$this->wp_customize->add_control( 'page_header_bg_img_position_x', array(
			'priority'        => 22,
			'label'           => esc_html__( 'Page Header Background Position', 'shaka-pt' ),
			'section'         => 'shaka_section_page_header',
			'type'            => 'radio',
			'active_callback' => function() {
				return WpUtilsHelpers::is_theme_mod_not_empty( 'page_header_bg_img' );
			},
			'choices'         => array(
				'left'       => esc_html__( 'Left', 'shaka-pt' ),
				'center'     => esc_html__( 'Center', 'shaka-pt' ),
				'right'      => esc_html__( 'Right', 'shaka-pt' ),
			),
		) );
		$this->wp_customize->add_control( 'page_header_bg_img_attachment', array(
			'priority'        => 23,
			'label'           => esc_html__( 'Page Header Background Attachment', 'shaka-pt' ),
			'section'         => 'shaka_section_page_header',
			'type'            => 'radio',
			'active_callback' => function() {
				return WpUtilsHelpers::is_theme_mod_not_empty( 'page_header_bg_img' );
			},
			'choices'         => array(
				'scroll'     => esc_html__( 'Scroll', 'shaka-pt' ),
				'fixed'      => esc_html__( 'Fixed', 'shaka-pt' ),
			),
		) );

		$this->wp_customize->add_control( new WP_Customize_Color_Control(
			$this->wp_customize,
			'page_header_color',
			array(
				'priority' => 30,
				'label'    => esc_html__( 'Page Header title color', 'shaka-pt' ),
				'section'  => 'shaka_section_page_header',
			)
		) );
		$this->wp_customize->add_control( new WP_Customize_Color_Control(
			$this->wp_customize,
			'page_header_subtitle_color',
			array(
				'priority' => 31,
				'label'    => esc_html__( 'Page Header subtitle color', 'shaka-pt' ),
				'section'  => 'shaka_section_page_header',
			)
		) );
		$this->wp_customize->add_control( 'show_page_title_area', array(
			'type'        => 'select',
			'priority'    => 35,
			'label'       => esc_html__( 'Show page title area', 'shaka-pt' ),
			'description' => esc_html__( 'This will hide the page title area on all pages. You can also hide individual page headers in page settings. To remove breadcrumbs from all pages, please deactivate the Breadcrumb NavXT plugin.', 'shaka-pt' ),
			'section'     => 'shaka_section_page_header',
			'choices'     => array(
				'yes' => esc_html__( 'Show', 'shaka-pt' ),
				'no'  => esc_html__( 'Hide', 'shaka-pt' ),
			),
		) );

		// Section: shaka_section_breadcrumbs.
		$this->wp_customize->add_control( new WP_Customize_Color_Control(
			$this->wp_customize,
			'breadcrumbs_color',
			array(
				'priority' => 45,
				'label'    => esc_html__( 'Breadcrumbs text color', 'shaka-pt' ),
				'section'  => 'shaka_section_breadcrumbs',
			)
		) );
		$this->wp_customize->add_control( new WP_Customize_Color_Control(
			$this->wp_customize,
			'breadcrumbs_color_hover',
			array(
				'priority' => 50,
				'label'    => esc_html__( 'Breadcrumbs hover text color', 'shaka-pt' ),
				'section'  => 'shaka_section_breadcrumbs',
			)
		) );
		$this->wp_customize->add_control( new WP_Customize_Color_Control(
			$this->wp_customize,
			'breadcrumbs_color_active',
			array(
				'priority' => 50,
				'label'    => esc_html__( 'Breadcrumbs active text color', 'shaka-pt' ),
				'section'  => 'shaka_section_breadcrumbs',
			)
		) );

		// Section: shaka_section_theme_colors.
		$this->wp_customize->add_control( 'layout_mode', array(
			'type'     => 'select',
			'priority' => 10,
			'label'    => esc_html__( 'Layout', 'shaka-pt' ),
			'section'  => 'shaka_section_theme_colors',
			'choices'  => array(
				'wide'  => esc_html__( 'Wide', 'shaka-pt' ),
				'boxed' => esc_html__( 'Boxed', 'shaka-pt' ),
			),
		) );
		$this->wp_customize->add_control( 'blog_columns', array(
			'type'        => 'select',
			'priority'    => 20,
			'label'       => esc_html__( 'Blog columns', 'shaka-pt' ),
			'description' => esc_html__( 'Number of columns on blog pages (blog, archives and search) on desktop.', 'shaka-pt' ),
			'section'     => 'shaka_section_theme_colors',
			'choices'     => array(
				12 => 1,
				6 => 2,
				4 => 3,
				3 => 4,
			),
		) );
		$this->wp_customize->add_control( new WP_Customize_Color_Control(
			$this->wp_customize,
			'text_color_content_area',
			array(
				'priority' => 30,
				'label'    => esc_html__( 'Text color', 'shaka-pt' ),
				'section'  => 'shaka_section_theme_colors',
			)
		) );
		$this->wp_customize->add_control( new WP_Customize_Color_Control(
			$this->wp_customize,
			'link_color',
			array(
				'priority' => 35,
				'label'    => esc_html__( 'Link color', 'shaka-pt' ),
				'section'  => 'shaka_section_theme_colors',
			)
		) );
		$this->wp_customize->add_control( new WP_Customize_Color_Control(
			$this->wp_customize,
			'headings_color',
			array(
				'priority' => 33,
				'label'    => esc_html__( 'Headings color', 'shaka-pt' ),
				'section'  => 'shaka_section_theme_colors',
			)
		) );
		$this->wp_customize->add_control( new WP_Customize_Color_Control(
			$this->wp_customize,
			'primary_color',
			array(
				'priority' => 34,
				'label'    => esc_html__( 'Primary color', 'shaka-pt' ),
				'section'  => 'shaka_section_theme_colors',
			)
		) );
		$this->wp_customize->add_control( new WP_Customize_Color_Control(
			$this->wp_customize,
			'dark_button_color',
			array(
				'priority' => 36,
				'label'    => esc_html__( 'Dark button background color', 'shaka-pt' ),
				'section'  => 'shaka_section_theme_colors',
			)
		) );
		$this->wp_customize->add_control( new WP_Customize_Color_Control(
			$this->wp_customize,
			'light_button_color',
			array(
				'priority' => 37,
				'label'    => esc_html__( 'Light button background color', 'shaka-pt' ),
				'section'  => 'shaka_section_theme_colors',
			)
		) );
		$this->wp_customize->add_control( new WP_Customize_Image_Control(
			$this->wp_customize,
			'body_bg_img',
			array(
				'priority' => 40,
				'label'    => esc_html__( 'Body background image', 'shaka-pt' ),
				'section'  => 'shaka_section_theme_colors',
			)
		) );
		$this->wp_customize->add_control( 'body_bg_img_repeat', array(
			'priority'        => 41,
			'label'           => esc_html__( 'Body background repeat', 'shaka-pt' ),
			'section'         => 'shaka_section_theme_colors',
			'type'            => 'radio',
			'active_callback' => function() {
				return WpUtilsHelpers::is_theme_mod_not_empty( 'body_bg_img' );
			},
			'choices'         => array(
				'no-repeat' => esc_html__( 'No Repeat', 'shaka-pt' ),
				'repeat'    => esc_html__( 'Tile', 'shaka-pt' ),
				'repeat-x'  => esc_html__( 'Tile Horizontally', 'shaka-pt' ),
				'repeat-y'  => esc_html__( 'Tile Vertically', 'shaka-pt' ),
			),
		) );
		$this->wp_customize->add_control( 'body_bg_img_position_x', array(
			'priority'        => 42,
			'label'           => esc_html__( 'Body background position', 'shaka-pt' ),
			'section'         => 'shaka_section_theme_colors',
			'type'            => 'radio',
			'active_callback' => function() {
				return WpUtilsHelpers::is_theme_mod_not_empty( 'body_bg_img' );
			},
			'choices'         => array(
				'left'   => esc_html__( 'Left', 'shaka-pt' ),
				'center' => esc_html__( 'Center', 'shaka-pt' ),
				'right'  => esc_html__( 'Right', 'shaka-pt' ),
			),
		) );
		$this->wp_customize->add_control( 'body_bg_img_attachment', array(
			'priority'        => 43,
			'label'           => esc_html__( 'Body background attachment', 'shaka-pt' ),
			'section'         => 'shaka_section_theme_colors',
			'type'            => 'radio',
			'active_callback' => function() {
				return WpUtilsHelpers::is_theme_mod_not_empty( 'body_bg_img' );
			},
			'choices'         => array(
				'scroll' => esc_html__( 'Scroll', 'shaka-pt' ),
				'fixed'  => esc_html__( 'Fixed', 'shaka-pt' ),
			),
		) );
		$this->wp_customize->add_control( new WP_Customize_Color_Control(
			$this->wp_customize,
			'body_bg',
			array(
				'priority' => 45,
				'label'    => esc_html__( 'Body background color', 'shaka-pt' ),
				'section'  => 'shaka_section_theme_colors',
			)
		) );

		// Section: shaka_section_shop.
		if ( ShakaHelpers::is_woocommerce_active() ) {
			$this->wp_customize->add_control( 'products_per_page', array(
					'label'   => esc_html__( 'Number of products per page', 'shaka-pt' ),
					'section' => 'shaka_section_shop',
				)
			);
			$this->wp_customize->add_control( 'single_product_sidebar', array(
					'label'   => esc_html__( 'Sidebar on single product page', 'shaka-pt' ),
					'section' => 'shaka_section_shop',
					'type'    => 'select',
					'choices' => array(
						'none'  => esc_html__( 'No sidebar', 'shaka-pt' ),
						'left'  => esc_html__( 'Left', 'shaka-pt' ),
						'right' => esc_html__( 'Right', 'shaka-pt' ),
					),
				)
			);
		}

		// Section: shaka_section_portfolio.
		if ( ShakaHelpers::is_portfolio_plugin_active() ) {
			$this->wp_customize->add_control( 'portfolio_parent_page', array(
					'label'       => esc_html__( 'Portfolio parent page', 'shaka-pt' ),
					'description' => esc_html__( 'Page with all the portfolio items.', 'shaka-pt' ),
					'section'     => 'shaka_section_portfolio',
					'type'        => 'dropdown-pages',
				)
			);
			$this->wp_customize->add_control( 'portfolio_slug', array(
					'label'       => esc_html__( 'Portfolio slug', 'shaka-pt' ),
					'description' => esc_html__( 'This is used in the URL part. After changing this, you must save the permalink settings again in Settings &rarr; Permalinks.', 'shaka-pt' ),
					'section'     => 'shaka_section_portfolio',
				)
			);
			$this->wp_customize->add_control( 'portfolio_gallery_columns', array(
					'label'   => esc_html__( 'Number of columns in gallery', 'shaka-pt' ),
					'section' => 'shaka_section_portfolio',
					'type'    => 'select',
					'choices' => array(
						'1' => '1',
						'2' => '2',
						'3' => '3',
					),
				)
			);
		}

		// Section: section_footer.
		$this->wp_customize->add_control( new Control\LayoutBuilder(
			$this->wp_customize,
			'footer_widgets_layout',
			array(
				'priority'    => 1,
				'label'       => esc_html__( 'Footer widgets layout', 'shaka-pt' ),
				'description' => esc_html__( 'Select number of widget you want in the footer and then with the slider rearrange the layout', 'shaka-pt' ),
				'section'     => 'section_footer',
				'input_attrs' => array(
					'min'     => 0,
					'max'     => 12,
					'step'    => 1,
					'maxCols' => 6,
				),
			)
		) );
		$this->wp_customize->add_control( new WP_Customize_Color_Control(
			$this->wp_customize,
			'footer_bg_color',
			array(
				'priority' => 10,
				'label'    => esc_html__( 'Footer background color', 'shaka-pt' ),
				'section'  => 'section_footer',
			)
		) );
		$this->wp_customize->add_control( new WP_Customize_Color_Control(
			$this->wp_customize,
			'footer_title_color',
			array(
				'priority' => 30,
				'label'    => esc_html__( 'Footer widget title color', 'shaka-pt' ),
				'section'  => 'section_footer',
			)
		) );

		$this->wp_customize->add_control( new WP_Customize_Color_Control(
			$this->wp_customize,
			'footer_text_color',
			array(
				'priority' => 31,
				'label'    => esc_html__( 'Footer text color', 'shaka-pt' ),
				'section'  => 'section_footer',
			)
		) );

		$this->wp_customize->add_control( new WP_Customize_Color_Control(
			$this->wp_customize,
			'footer_link_color',
			array(
				'priority' => 32,
				'label'    => esc_html__( 'Footer link color', 'shaka-pt' ),
				'section'  => 'section_footer',
			)
		) );

		$this->wp_customize->add_control( new WP_Customize_Color_Control(
			$this->wp_customize,
			'footer_bottom_bg_color',
			array(
				'priority' => 35,
				'label'    => esc_html__( 'Footer bottom background color', 'shaka-pt' ),
				'section'  => 'section_footer',
			)
		) );

		$this->wp_customize->add_control( new WP_Customize_Color_Control(
			$this->wp_customize,
			'footer_bottom_text_color',
			array(
				'priority' => 36,
				'label'    => esc_html__( 'Footer bottom text color', 'shaka-pt' ),
				'section'  => 'section_footer',
			)
		) );

		$this->wp_customize->add_control( new WP_Customize_Color_Control(
			$this->wp_customize,
			'footer_bottom_link_color',
			array(
				'priority' => 37,
				'label'    => esc_html__( 'Footer bottom link color', 'shaka-pt' ),
				'section'  => 'section_footer',
			)
		) );

		$this->wp_customize->add_control( 'footer_bottom_left_txt', array(
			'type'        => 'text',
			'priority'    => 110,
			'label'       => esc_html__( 'Footer bottom text on the left', 'shaka-pt' ),
			'description' => esc_html__( 'You can use HTML: a, span, i, em, strong, img.', 'shaka-pt' ),
			'section'     => 'section_footer',
		) );

		$this->wp_customize->add_control( 'footer_bottom_center_txt', array(
			'type'        => 'text',
			'priority'    => 115,
			'label'       => esc_html__( 'Footer bottom text on the center', 'shaka-pt' ),
			'description' => esc_html__( 'You can use HTML: a, span, i, em, strong, img.', 'shaka-pt' ),
			'section'     => 'section_footer',
		) );

		$this->wp_customize->add_control( 'footer_bottom_right_txt', array(
			'type'        => 'text',
			'priority'    => 120,
			'label'       => esc_html__( 'Footer bottom text on the right', 'shaka-pt' ),
			'description' => esc_html__( 'You can use HTML: a, span, i, em, strong, img.', 'shaka-pt' ),
			'section'     => 'section_footer',
		) );

		// Section: section_custom_code.
		if ( function_exists( 'wp_update_custom_css_post' ) ) {
			// Show the notice of custom CSS setting migration.
			$this->wp_customize->add_control( 'pt_custom_css', array(
				'type'        => 'hidden',
				'label'       => esc_html__( 'Custom CSS', 'shaka-pt' ),
				'description' => esc_html__( 'This field is obsolete. The existing code was migrated to the "Additional CSS" field, that can be found in the root of the customizer. This new "Additional CSS" field is a WordPress core field and was introduced in WP version 4.7.', 'shaka-pt' ),
				'section'     => 'section_custom_code',
			) );
		}
		else {
			$this->wp_customize->add_control( 'custom_css', array(
				'type'        => 'textarea',
				'label'       => esc_html__( 'Custom CSS', 'shaka-pt' ),
				'description' => sprintf( esc_html__( '%s How to find CSS classes %s in the theme.', 'shaka-pt' ), '<a href="https://www.youtube.com/watch?v=V2aAEzlvyDc" target="_blank">', '</a>' ),
				'section'     => 'section_custom_code',
			) );
		}

		$this->wp_customize->add_control( 'custom_js_head', array(
			'type'        => 'textarea',
			'label'       => esc_html__( 'Custom JavaScript (head)', 'shaka-pt' ),
			'description' => esc_html__( 'You have to include the &lt;script&gt;&lt;/script&gt; tags as well. Paste your Google Analytics tracking code here.', 'shaka-pt' ),
			'section'     => 'section_custom_code',
		) );

		$this->wp_customize->add_control( 'custom_js_footer', array(
			'type'        => 'textarea',
			'label'       => esc_html__( 'Custom JavaScript (footer)', 'shaka-pt' ),
			'description' => esc_html__( 'You have to include the &lt;script&gt;&lt;/script&gt; tags as well.', 'shaka-pt' ),
			'section'     => 'section_custom_code',
		) );

		// Section: section_other.
		$this->wp_customize->add_control( 'show_acf', array(
			'type'        => 'select',
			'label'       => esc_html__( 'Show ACF admin panel?', 'shaka-pt' ),
			'description' => esc_html__( 'If you want to use ACF and need the ACF admin panel set this to <strong>Yes</strong>. Do not change if you do not know what you are doing.', 'shaka-pt' ),
			'section'     => 'section_other',
			'choices'     => array(
				'no'  => esc_html__( 'No', 'shaka-pt' ),
				'yes' => esc_html__( 'Yes', 'shaka-pt' ),
			),
		) );
		$this->wp_customize->add_control( 'use_minified_css', array(
			'type'    => 'select',
			'label'   => esc_html__( 'Use minified theme CSS', 'shaka-pt' ),
			'section' => 'section_other',
			'choices' => array(
				'no'  => esc_html__( 'No', 'shaka-pt' ),
				'yes' => esc_html__( 'Yes', 'shaka-pt' ),
			),
		) );
		$this->wp_customize->add_control( 'charset_setting', array(
			'type'     => 'select',
			'label'    => esc_html__( 'Character set for Google Fonts', 'shaka-pt' ),
			'section'  => 'section_other',
			'choices'  => array(
				'latin'        => 'Latin',
				'latin-ext'    => 'Latin Extended',
				'cyrillic'     => 'Cyrillic',
				'cyrillic-ext' => 'Cyrillic Extended',
				'vietnamese'   => 'Vietnamese',
				'greek'        => 'Greek',
				'greek-ext'    => 'Greek Extended',
			),
		) );
	}
}

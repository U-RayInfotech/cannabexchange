<?php
/**
 * Register automatic updates for this theme.
 */

use ProteusThemes\ThemeRegistration\ThemeRegistration;

class ShakaThemeRegistration {
	function __construct() {
		$this->enable_theme_registration();
	}

	/**
	 * Load theme registration and automatic updates.
	 */
	private function enable_theme_registration() {
		$config = array(
			'item_name'        => 'Shaka',
			'theme_slug'       => 'shaka-pt',
			'item_id'          => 2260,
			'tf_item_id'       => 16965165,
			'customizer_panel' => 'panel_shaka',
			'build'            => 'tf',
		);
		$pt_theme_registration = ThemeRegistration::get_instance( $config );
	}
}

if ( ! SHAKA_DEVELOPMENT && ! defined( 'ENVATO_HOSTED_SITE' ) ) {
	$shaka_theme_registration = new ShakaThemeRegistration();
}

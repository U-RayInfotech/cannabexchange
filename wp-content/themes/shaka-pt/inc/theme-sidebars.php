<?php
/**
 * Register sidebars for Shaka
 *
 * @package shaka-pt
 */

function shaka_sidebars() {
	// Blog Sidebar.
	register_sidebar(
		array(
			'name'          => esc_html__( 'Blog Sidebar', 'shaka-pt' ),
			'id'            => 'blog-sidebar',
			'description'   => esc_html__( 'Sidebar on the blog layout.', 'shaka-pt' ),
			'class'         => 'blog  sidebar',
			'before_widget' => '<div class="widget  %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h4 class="sidebar__headings">',
			'after_title'   => '</h4>',
		)
	);

	// Regular Page Sidebar.
	register_sidebar(
		array(
			'name'          => esc_html__( 'Regular Page Sidebar', 'shaka-pt' ),
			'id'            => 'regular-page-sidebar',
			'description'   => esc_html__( 'Sidebar on the regular page.', 'shaka-pt' ),
			'class'         => 'sidebar',
			'before_widget' => '<div class="widget  %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h4 class="sidebar__headings">',
			'after_title'   => '</h4>',
		)
	);

	// Register top sidebars only if the setting 'top_bar_visibility' in customizer is set to be visible.
	if ( 'no' !== get_theme_mod( 'top_bar_visibility', 'yes' )  ) {
		// Top Left Widgets.
		register_sidebar(
			array(
				'name'          => esc_html__( 'Top Left', 'shaka-pt' ),
				'id'            => 'top-left-widgets',
				'description'   => esc_html__( 'Top left widget area for Icon Box, Social Icons, Custom Menu or Text widget.', 'shaka-pt' ),
				'before_widget' => '<div class="widget  %2$s">',
				'after_widget'  => '</div>',
			)
		);

		// Top Right Widgets.
		register_sidebar(
			array(
				'name'          => esc_html__( 'Top Right', 'shaka-pt' ),
				'id'            => 'top-right-widgets',
				'description'   => esc_html__( 'Top right widget area for Icon Box, Social Icons, Custom Menu or Text widget.', 'shaka-pt' ),
				'before_widget' => '<div class="widget  %2$s">',
				'after_widget'  => '</div>',
			)
		);
	}

	// Header Left Widgets.
	register_sidebar(
		array(
			'name'          => esc_html__( 'Header Left', 'shaka-pt' ),
			'id'            => 'header-left-widgets',
			'description'   => esc_html__( 'Header left widget area for Icon Box, Social Icons, Search, or Text.', 'shaka-pt' ),
			'before_widget' => '<div class="widget  %2$s">',
			'after_widget'  => '</div>',
		)
	);

	// Header Right Widgets.
	register_sidebar(
		array(
			'name'          => esc_html__( 'Header Right', 'shaka-pt' ),
			'id'            => 'header-right-widgets',
			'description'   => esc_html__( 'Header right widget area for Icon Box, Social Icons, Search, or Text.', 'shaka-pt' ),
			'before_widget' => '<div class="widget  %2$s">',
			'after_widget'  => '</div>',
		)
	);

	// WooCommerce shop sidebar.
	if ( ShakaHelpers::is_woocommerce_active() ) {
		register_sidebar(
			array(
				'name'          => esc_html__( 'Shop Sidebar', 'shaka-pt' ),
				'id'            => 'shop-sidebar',
				'description'   => esc_html__( 'Sidebar for the shop page', 'shaka-pt' ),
				'class'         => 'sidebar',
				'before_widget' => '<div class="widget  %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<h4 class="sidebar__headings">',
				'after_title'   => '</h4>',
			)
		);
	}

	// Footer.
	$footer_widgets_num = count( ShakaHelpers::footer_widgets_layout_array() );

	// Only register if not 0.
	if ( $footer_widgets_num > 0 ) {
		register_sidebar(
			array(
				'name'          => esc_html__( 'Footer', 'shaka-pt' ),
				'id'            => 'footer-widgets',
				'description'   => sprintf( esc_html__( 'Footer area works best with %d widgets. This number can be changed in the Appearance &rarr; Customize &rarr; Theme Options &rarr; Footer.', 'shaka-pt' ), $footer_widgets_num ),
				'before_widget' => '<div class="col-xs-12  col-lg-__col-num__"><div class="widget  %2$s">', // __col-num__ is replaced dynamically in filter 'dynamic_sidebar_params'
				'after_widget'  => '</div></div>',
				'before_title'  => '<h6 class="footer-top__heading">',
				'after_title'   => '</h6>',
			)
		);
	}
}
add_action( 'widgets_init', 'shaka_sidebars' );

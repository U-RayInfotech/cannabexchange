<?php
/**
 * Single portfolio page template
 *
 * @package shaka-pt
 */

get_header();

get_template_part( 'template-parts/page-header' );

?>

	<!-- Featured Image -->
	<?php if ( has_post_thumbnail() ) : ?>
		<div class="portfolio__featured-image">
			<a href="<?php echo esc_url( wp_get_attachment_url( get_post_thumbnail_id() ) ); ?>">
				<?php the_post_thumbnail( 'post-thumbnail', array( 'class' => 'img-fluid' ) ); ?>
			</a>
		</div>
	<?php endif; ?>

	<!-- Content Area -->
	<div id="primary" class="content-area  container">
		<main role="main" class="row">
			<div class="col-xs-12  col-lg-6">
				<div class="portfolio__basic-info">
					<!-- Price -->
					<?php if ( ( $price = get_field( 'price' ) ) && get_field( 'show_price' ) ) : ?>
						<p class="portfolio__price"><?php echo esc_html( $price ); ?></p>
					<?php endif; ?>
					<!-- Title -->
					<h2 class="portfolio__title"><?php echo get_the_title(); ?></h2>
					<div class="portfolio__text">
						<?php echo wp_kses_post( get_field( 'short_description' ) ); ?>
					</div>
					<!-- Specifications -->
					<div class="portfolio__specification">
						<?php
						while ( have_rows( 'specification' ) ) :
							the_row();
						?>
							<div class="portfolio__specification-item">
								<i class="fa  <?php echo esc_attr( get_sub_field( 'icon' ) ); ?>" aria-hidden="true"></i>
								<?php echo esc_html( get_sub_field( 'text' ) ); ?>
							</div>
						<?php endwhile; ?>
					</div>
					<!-- Features -->
					<div class="portfolio__feature">
						<?php
						while ( have_rows( 'features' ) ) :
							the_row();
						?>
							<div class="portfolio__feature-item">
								<i class="fa  <?php echo esc_attr( get_sub_field( 'icon' ) ); ?>" aria-hidden="true"></i>
								<?php echo esc_html( get_sub_field( 'text' ) ); ?>
							</div>
						<?php endwhile; ?>
					</div>
					<!-- CTA button -->
					<?php
					$cta_type       = get_field( 'cta_button_type' );
					$cta_text       = get_field( 'cta_button_text' );
					$cta_new_tab    = get_field( 'open_in_new_tab' );
					$cta_url        = '';

					// Setup correct URL.
					if ( 'custom-url' === $cta_type ) {
						$cta_url = get_field( 'cta_button_custom_url' );
					}
					elseif ( 'wc-product' === $cta_type && ShakaHelpers::is_woocommerce_active() ) {
						$product_obj = get_field( 'cta_button_product_link' );
						$product_id  = ! empty( $product_obj ) ? absint( $product_obj->ID ) : '';
						if ( ! empty( $product_id ) ) {
							$cta_url = add_query_arg( 'add-to-cart', esc_attr( $product_id ), ShakaHelpers::get_woocommerce_cart_url() );
						}
					}

					if ( ! empty( $cta_url ) ) :
					?>
						<a class="btn  btn-primary  portfolio__cta" href="<?php echo esc_url( $cta_url ); ?>" target="<?php echo ! empty( $cta_new_tab ) ? '_blank' : '_self'; ?>"><?php echo ! empty( $cta_text ) ? esc_html( $cta_text ) : esc_html__( 'Book Now', 'shaka-pt' ); ?></a>
					<?php endif; ?>
				</div>
			</div>
			<div class="col-xs-12  col-lg-6">
				<div class="portfolio__gallery-container">
					<!-- Gallery -->
					<?php $portfolio_gallery_columns = get_theme_mod( 'portfolio_gallery_columns', 2 ); ?>
					<div class="portfolio__gallery  portfolio__gallery--col-<?php echo absint( $portfolio_gallery_columns ); ?>" data-featherlight-gallery data-featherlight-filter="a">
						<?php
						while ( have_rows( 'project_gallery' ) ) {
							the_row();

							$image = get_sub_field( 'project_image' );
							?>

							<figure class="portfolio__gallery-item  gallery-item">

								<?php
								printf(
									'<a class="portfolio__gallery-link" href="%1$s"><img class="img-fluid  portfolio__gallery-image" src="%2$s" alt="%3$s" width="%4$d" height="%5$d" /></a> ',
									esc_url( $image['url'] ),
									esc_attr( $image['sizes']['shaka-portfolio-gallery'] ),
									esc_attr( $image['alt'] ),
									absint( $image['sizes']['shaka-portfolio-gallery-width'] ),
									absint( $image['sizes']['shaka-portfolio-gallery-height'] )
								);
								?>

								<?php if ( ! empty( $image['caption'] ) ) : ?>
									<figcaption class="portfolio__gallery-item-caption  wp-caption-text gallery-caption">
										<?php echo esc_html( $image['caption'] ); ?>
									</figcaption>
								<?php endif; ?>

							</figure>

							<?php
						}

						?>
					</div>
				</div>
			</div>
			<div class="col-xs-12">
				<!-- Content -->
				<div class="portfolio__content">
					<?php the_content(); ?>
				</div>
			</div>
		</main>
	</div>

<?php get_footer(); ?>
